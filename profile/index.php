<?php

error_reporting(-1);
ini_set('display_errors', 1);

$level = 2;
require_once dirname(__FILE__, $level) . '/main/epiqworx/logic/sample.php';
require_once dirname(__FILE__, $level) . '/main/epiqworx/db/handler.php';
require_once dirname(__FILE__, $level) . '/main/model.php';
require_once 'model.php';
$uname  = $uid = null;
$ICON = PATH . '/main/usr/img/sys/logo.png';
$UAVATAR = PATH .'/main/usr/img/sys/user_1.png';
$rm_avatar = 'display-none';
$session = 'offline';
$pstate = 'old';
$input_err = '';
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if(!isset($_SESSION['uname']) && filter_input(INPUT_GET,'hash') == null){  header("Location:".PATH);}  //------ return to home path if under no session
else{
    $uname = $_SESSION['uname'];
    $uid = $_SESSION['id'];
    $session = 'online';
    if(empty(Account::get_password($uname))){
        $pstate = 'new';
        $input_err = 'Account vulnerable, please create password!';
    }
    if(!empty($avtr = Account::get_icon($uid))){
        $ICON = $UAVATAR = PATH . "/main/usr/img/user/avatar/$uid.$avtr";
        $rm_avatar = null;
    }
}
$connected = dbAccess::test('epiqworx');
$sexion = array(null,null,null,null);
$flash = $nav = null;   //------------------------------------------------------------- Color Fill Special Effect on Page Load (inactive)
$passwd_length = 8;
$err = array();

// Menu Navigation Array
$current = array(
    'home' => array(null, 'href="."'),
    'services' => array(null, 'href="?action=services"'),
    'work' => array(null, 'href="../work?action=work-item1"'),
    'notifications' => array(null, 'href="?action=notifications"'),
    'about-product' => array(null, 'href="?action=about-product"'),
    'about-team' => array(null, 'href="?action=about-team"'),
    'about-tnc' => array(null, 'href="?action=about-tnc"'),
    'profile-edit' => array(null, 'href="?action=profile-edit_one"'),
    'profile-conn' => array(null, 'href="?action=profile-conn"'),
    'profile-signout' => array(null, 'href="?action=profile-signout"'),
    'help-purge' => array(null, 'href="?action=help-purge"'),
    'help-reset' => array(null, 'href="?action=help-reset"'),
    'help-man' => array(null, 'href="?action=help-man"'),
    'help-faq' => array(null, 'href="?action=help-faq"')
);
$active = array('about' => null, 'help' => null, 'profile' => null, 'work' => null);

// Section Navigation Array
$section = array(
    'one' => array(null,'href="?action=profile-edit_one"'),
    'acc' => array(null,'href="?action=profile-edit_acc"'),
    'grp' => array(null,'href="?action=profile-edit_grp"'),
    'pass' => array(null,'href="?action=profile-edit_pass"')
);

/*
 * start controller
 */
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'profile-edit_one';
    }
}
if ($pstate === 'new' && $action !== 'profile-edit_pass') {
    header('location:?action=profile-edit_pass');
}
if(explode('_',$action)[0]=== 'profile-edit'){
$section[explode('_',$action)[1]][0] = 'active';
$section[explode('_',$action)[1]][1] = null;
}
$current[explode('_',$action)[0]][0] = $active[explode('-', $action)[0]] = 'current';
$current[explode('_',$action)[0]][1] = null;

switch ($action) {
    case 'profile-edit_one':
        $title = "Profile :: Personal";
        $script = PATH . '/main/usr/php/ajax/profile.php';
        $sexion[0] = '<span class="float-right big"><i class="fa fa-4x fa-angle-right bounce-x"></i><i class="fa fa-4x fa-angle-right bounce-x"></i></span>';
        require_once 'view/one.php';
        break;
    case 'profile-edit_grp':
        $title = "Profile :: Groups";
        $script = PATH . '/main/usr/php/ajax/profile.php';
        $sexion[1] = '<span class="float-right big"><i class="fa fa-4x fa-angle-right bounce-x"></i><i class="fa fa-4x fa-angle-right bounce-x"></i></span>';
        require_once 'view/group.php';
        break;
    case 'profile-edit_acc':
        $title = "Profile :: Account";
        $script = PATH . '/main/usr/php/ajax/profile.php';
        $sexion[2] = '<span class="float-right big"><i class="fa fa-4x fa-angle-right bounce-x"></i><i class="fa fa-4x fa-angle-right bounce-x"></i></span>';
        require_once 'view/acc.php';
        break;
    case 'profile-edit_pass':
        $title = "Profile :: Password";
        $script = PATH . '/main/usr/php/ajax/profile.php';
        $sexion[3] = '<span class="float-right big"><i class="fa fa-4x fa-angle-right bounce-x"></i><i class="fa fa-4x fa-angle-right bounce-x"></i></span>';
        require_once 'view/passwd.php';
        break;
    case 'notifications':
        $title = 'Notifications';
        require_once 'view/notification.php';
        break;
    case 'profile-conn':
        $title = 'Connections';
        require_once 'view/connections.php';
        break;
    case 'profile-signout':
        session_destroy();
        header('location: '.PATH);
        break;
    default :
        header("location:" . PATH . "?action=$action");
        break;
}