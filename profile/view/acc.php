<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
    <body id="profile_acc">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="#" title="home"><img id="sys_avatar" src="<?=$ICON;?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once 'nav.php'; ?>
            <div class="main-content shadow">
                <form action="." method="post" class="input-form js">
                    <input type="hidden" name="action" value="update-acc-info"/>
                    <ul class="form-input">
                        <div class="group">
                            <span class="label">1</span><span class="label-info float-right">Account Types</span>
                            <li>
                                <label class="clearfix">Lecturer<input type="checkbox" value="lecturer" class="float-right" /></label>
                                <label class="clearfix">Student<input type="checkbox" value="student" class="float-right" /></label>
                                <label class="clearfix">PrivAcc<input type="checkbox" value="privacc" class="float-right" /></label>
                            </li>
                            <li>
                            <button class="button_1" type="button" id="btn_save">save</button>
                            <span id="update_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
                            <div class="success-tick display-none" id="update_success">✔</div>
                            </li>
                        </div>
                        <div class="group">
                            <span class="label">2</span><span class="label-info float-right">Deactivate Account</span>
                            <li>
                                <label class="prompt float-left">Reason</label>
                                <textarea name="group-desc" id="ta_input_1" ></textarea>
                                <span id="cmc_1" title="state reason for deactivating account" class="cmc"></span>
                            </li>
                            <li>
                            <button class="button_1" type="button" id="btn_deactivate">deactivate</button>
                            <span id="update_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
                            <div class="success-tick display-none" id="update_success">✔</div>
                            </li>
                        </div>
                        <li>
                            <hr/>
                            <span id="input_error" class="message display-none"></span>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</section>
<?php require_once dirname(__FILE__, 3) . "/main/view/default/footer.php";
?>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/profile/acc.js"></script>
</body>
</html>