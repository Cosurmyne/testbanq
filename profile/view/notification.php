<!DOCTYPE html>
<html lang="en">
<?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
<body id='notification_page'>
  <section id="page_wrap" class="page-wrap main">
    <header class="main">
      <div class="container">
        <div id="branding" class="float-left">
          <a href="." title="home"><img id="sys_avatar" src="<?=$ICON;?>" alt="Logo" /></a>
        </div>
        <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
      </div>
    </header>
  </section>
  <?php require_once dirname(__FILE__, 3) . "/main/view/default/footer.php";?>
</body>
</html>
