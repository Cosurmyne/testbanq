<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
    <body id="profile_group">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img id="sys_avatar" src="<?= $ICON; ?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once 'nav.php'; ?>
            <div class="main-content shadow">
                <form id="group_form" action="<?= $script; ?>" method="post" nctype="multipart/form-data"  class="input-form js">
                    <input type="hidden" name="action" value="group-add" id="group_script_action"/>
                    <input type="hidden" name="group-id" id="group_id"/>
                    <input type="hidden" name="page" value="<?= $_SERVER['QUERY_STRING']; ?>"/>
                    <ul class="form-input">
                        <div class="group">
                            <span class="label">1</span><span class="label-info float-right">Group Info</span>
                            <li>
                                <label class="prompt float-left">Name</label>
                                <label class="required float-right">*</label>
                                <input id="txt_input_0" name="group-name" minlength="2" maxlength="20" required="" type="text"/>
                                <span id="cmc_0" title="means to identify group" class="cmc"></span>
                            </li>
                            <li>
                                <label class="prompt float-left">Description</label>
                                <textarea name="group-desc" id="ta_input_1" ></textarea>
                                <span id="cmc_1" title="substantial info on the group" class="cmc"></span>
                            </li>
                            <li>
                                <label class="prompt float-left">Access</label>
                                <label class="required float-right">*</label>
                                <label class="radio">Public<input id="rbn_1_group_privacy_2" name="group-access" required value="1" checked type="radio"/></label>
                                <label class="radio">Private<input  id="rbn_0_group_privacy_2"  name="group-access" required value="0" type="radio"/></label>
                                <span id="cmc_2" class="cmc" title="who can access group content"></span>
                            </li>
                            <li class="exception">
                                <label class="prompt float-left">Icon</label>
                                <input type="hidden" name="group-icon" id="group_icon_base64"/>
                                <input type="file" accept="image/*" id="file_input_2" name="img-group-icon" />
                                <span id="cmc_3" title="think interms of a group banner, or emblem" class="cmc"></span
                            </li>
                        </div>
                        <div class="group">
                            <span class="label" title="Current Groups">2</span><span class="label-info float-right">Current Groups</span>
                            <li class="exception list">
                                <table id='tbl_group_ls' class="fancy"></table>
                            </li>
                        </div>
                        <li>
                            <hr class="big"/>
                            <span id="input_error" class="message display-none"></span>

                            <button class="button_1" type="reset" id="clr_btn">clear</button>
                            <button class="button_1" type="submit" id="sbm_btn">save</button>
                            <span id="update_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
                            <div class="success-tick display-none" id="update_success">✔</div>
                        </li>
                    </ul>
                </form>
                <form id="group_content" method="post" class="input-form display-none none-form">
                    <input type="hidden" id="focus_group_id" name="group-id" />
                    <ul class="breadcrumb" id="nav_crumb">
                    </ul>
                    <div class="panel clearfix">
                        <label id="group_content_msg" class="float-right">:</label>
                        <span class="label button" id="btnback_form" title="back to form"><i class="fa fa-arrow-left fa-2x "></i></span>
                        <span class="label right display-none" id="admin_flag" >ADMIN</span>
                    </div>
                    <div id="group_default">
                        <table class="brief" cellspacing="0">
                            <tr id="gmemb_tr"><td colspan="2" class="subject" title="click to see group members">MEMBERS</td><td rowspan="3" id="gmemb_count" title="click to see group members"></td></tr>
                            <tr class="info"><td rowspan="2"><em>Latest</em></td><td id="latest_member_time">:</td></tr>
                            <tr><td id="latest_member_name" class="sub"></td></tr>
                            <tr id="grepo_tr"><td colspan="2" class="subject" title="click to view group repos" >REPOSITORIES</td><td rowspan="3" id="grepo_count" title="click to view group repos" ></td></tr>
                            <tr class="info"><td rowspan="2"><em>Latest</em></td><td id="latest_repo_time">:</td></tr>
                            <tr><td id="latest_repo_name" class="sub"></td></tr>
                        </table>
                    </div>
                    <div id="member_ls" class="display-none">
                        <ul class="form-input">
                            <li class="functionality" id="li_user_add">
                                <input type="text" maxlength="20" id="txt_user_add_4" placeholder="username" />
                                <div class="ctrl">
                                    <button type="button" class="button_1" id="btn_add_member">add</button>
                                    <span id="add_member_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
                                </div>
                                <span id="cmc_4" class="cmc" title="please specify user to add to group"></span>
                            </li>
                            <li class="exception list">
                                <table id='tbl_group_mbers' class="fancy"></table>
                            </li>
                        </ul>
                    </div>
                    <div id="repo_ls" class="display-none">
                        <ul class="form-input">
                            <li class="functionality" id="li_repo_add">
                                <select id="input_repo_select_5"></select>
                                <div class="ctrl">
                                    <button type="button" class="button_1" id="btn_add_repo">add</button>
                                    <span id="add_repo_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
                                </div>
                                <span id="cmc_5" class="cmc" title="please select repo to add to group"></span>
                            </li>
                            <li class="exception list">
                                <table id='tbl_group_repos' class="fancy"></table>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php require_once dirname(__FILE__, 3) . "/main/view/default/footer.php";
?>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/profile/groups.js"></script>
</body>
</html>