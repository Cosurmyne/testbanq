<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
    <body id="profile_one">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img id="sys_avatar" src="<?=$ICON;?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once 'nav.php'; ?>

            <div class="main-content shadow">
                <form action="." method="post" class="input-form js">
                    <input type="hidden" name="action" value="update-personal-info"/>
                    <ul class="form-input">
                        <div class="group">
                            <span class="label">1</span><span class="label-info float-right">Vital Info</span>
                            <li>
                                <label class="prompt float-left">Username</label>
                                <label class="required float-right">*</label>
                                <input id="txt_input_0" name="txt-uname" minlength="2" maxlength="20" required="" type="text"/>
                                <span id="cmc_0" class="cmc" title="<?=PATH,'/main/usr/home/'.$_SESSION['uname'];?>"></span>
                            </li>
                            <li>
                                <label class="prompt float-left">Email</label>
                                <label class="required float-right">*</label>
                                <input id="txt_input_1" name="txt-umail" minlength="2" maxlength="45" required="" type="email"/>
                                <span id='cmc_1' class="cmc" title="means of communication with application"></span>
                            </li>
                            <li class="exception">
                                <label class="prompt float-left">Phone #</label>
                                <input id="txt_input_2" name="txt-uphone" minlength="10" maxlength="10"  type="text"/>
                                <span id='cmc_2' class="cmc" title="complementary contact detail"></span>
                            </li>
                        </div>
                        <div class="group">
                            <span class="label">2</span><span class="label-info float-right">Formalities</span>
                            <li>
                                <label class="prompt float-left">First Name</label>
                                <input id="txt_input_3" name="txt-fname" type="text" maxlength="20" />
                                <span id='cmc_3' class="cmc" title="example : William"></span>
                            </li>
                            <li class="exception">
                                <label class="prompt float-left">Last Name</label>
                                <input id="txt_input_4" name="txt-fname" type="text" maxlength="20" />
                                <span id='cmc_4' class="cmc" title="example : Cooper"></span>
                            </li>
                        </div>
                        <li>
                            <hr class="big"/>
                            <span id="input_error" class="message display-none"></span>
                            <button class="button_1" type="button" id="sbm_btn">save</button>
                            <span id="update_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
                            <div class="success-tick display-none" id="update_success">✔</div>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</section>
<?php require_once dirname(__FILE__, 3) . "/main/view/default/footer.php"; ?>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/profile/one.js"></script>
</body>
</html>
