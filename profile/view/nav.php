<?php
require_once dirname(__FILE__, 3) . '/main/epiqworx/template/noscript.html';
require_once dirname(__FILE__, 3) . '/main/epiqworx/template/modal-img.php';
?>
<div id="ud_wrap" class="profile-wrap js display-flex">
    <div class="page-container">
        <div class="profile-dash">
            <div class="profile-card">
                <div class="div-avatar">
                    <div id="panel_icon_ctrl" class="display-none">
                        <button id="btn_rm_icon" title="remove icon" class="float-right" type="button"><i class="fa fa-close"></i></button>
                    </div>
                    <img id="org_avatar" /></div>
                <div class="div-HUD">
                    <div>
                        <div class="grouping">
                            <i class="fa fa-close right <?= $rm_avatar; ?>" id="rm_avatar" title="remove avatar"></i>
                            <input type="file" id="browse_avatar" accept="image/*" title="update avatar" />
                            <img id="user_avatar" src="<?= $UAVATAR; ?>"  alt="avatar" height="90"/>
                        </div>
                    </div>
                    <div class="user-online">
                        <span id="uname_online"><?= $_SESSION['uname']; ?></span>
                        <span id="umail_online"><?= $_SESSION['umail']; ?></span>
                    </div>
                </div>
            </div>
            <div class="nav shadow">
                <ul>
                    <li class="<?= $section['one'][0]; ?>"><a <?= $section['one'][1]; ?>>Personal <span id="uid"><?= $_SESSION['id']; ?></span><?= $sexion[0]; ?></a></li>
                    <li class="<?= $section['grp'][0]; ?>"><a <?= $section['grp'][1]; ?>>Groups <?= $sexion[1]; ?></a></li>
                    <li class="<?= $section['acc'][0]; ?>" ><a <?= $section['acc'][1]; ?>>Account <?= $sexion[2]; ?></a></li>
                    <li class="<?= $section['pass'][0]; ?>"><a <?= $section['pass'][1]; ?>>Password <?= $sexion[3]; ?></a></li>
                </ul>
            </div>
        </div>
        <script>
            function fetch_canvas(ev) {
                c.result('canvas').then(function (canvas) {
                    img_base64.value = canvas;
                    modal_btn_crop.disabled = false;
                }).catch(function (ex) {
                    window.alert(ex.message);
                });
            }
            function avatar_crop() {
                c.result('canvas').then(function (canvas) {
                    switch_panels(img_vss, modal_btn_crop);
                    img_base64.value = canvas;
                }).catch(function (ex) {
                    window.alert(ex.message);
                });
            }
            function reset_img_back() {
                let parent = org_avatar.parentNode;
                parent.removeChild(org_avatar);
                let img = document.createElement('img');
                img.id = 'org_avatar';
                parent.appendChild(img);
            }
            browse_avatar.addEventListener('change', function (e) {
                modal_img.classList.remove('display-none');
                img_crop_action.value = 'avatar';
                modal_btn_crop.type = 'submit';
                modal_btn_crop.onclick = avatar_crop;

                c = upload_img(this, modal_crop_space, null, 320, 320, false, 'img-rotate', 'Avatar', fetch_canvas);
            });
            modal_btn_close_crop.addEventListener('click', function (e) {
                modal_img.classList.add('display-none');
                browse_avatar.value = null;
                discard_croppie(c);
            });
            modal_crop_space.addEventListener('update', fetch_canvas);
            modal_crop_space.addEventListener('change', function (e) {
                modal_btn_crop.disabled = true;
            });
            rm_avatar.addEventListener('click', function (e) {
                get_ajax(window.localStorage.profile + '?action=rm-avatar', function (data) {
                    console.log(data);
                    window.location = '.';
                });
            });
        </script>
