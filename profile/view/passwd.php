<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
    <body id="page_wrap" class="page-wrap main">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="#" title="home"><img id="sys_avatar" src="<?=$ICON;?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once 'nav.php'; ?>
            <div class="main-content shadow">
                <form action="." method="post" class="input-form js">
                    <input type="hidden" name="action" value="update-passwd"/>
                    <ul class="form-input">
                        <?php require_once "passwd-$pstate.html"; ?>
                        <li>
                            <hr class="big"/>
                            <span id="input_error" class="message"><?=$input_err;?></span>
                            <button class="button_1" type="button" id="sbm_btn" title="save info">save</button>
                            <span id="update_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
                            <div class="success-tick display-none" id="update_success">✔</div>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</section>
<?php require_once dirname(__FILE__, 3) . "/main/view/default/footer.php";
?>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/profile/passwd.js"></script>
</body>
</html>