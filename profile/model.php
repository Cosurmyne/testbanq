<?php
abstract class Account{
    public static function add($uname,$umail,$ajax=false){
        $params = array(':uname'=>$uname,':umail'=>$umail);
        
        $records = dbHandler::dql('SELECT COUNT(USER_ID) RECORDS FROM user WHERE USERNAME = :uname OR USERMAIL = :umail',$params,$ajax)['RECORDS'];
        if(intval($records)>0){            return 'CONFLICT';}
        
        return dbHandler::execute("call sp_add_user(:uname,:umail)", $params,$ajax);
    }
    public static function promote($type,$id){
        return dbHandler::execute("INSERT INTO $type(USER_ID) VALUES(:id)",array(':id'=>$id));
    }

    public static function update($uname,$phone,$fname,$lname,$id,$ajax=false){
        $param = array(':uname'=>$uname,':phone'=>$phone,':fname'=>$fname,':lname'=>$lname,':id'=>$id);
        return dbHandler::execute('UPDATE user SET USERNAME = :uname, USERPHONE = :phone, FIRST_NAME = :fname, LAST_NAME = :lname WHERE USER_ID = :id', $param,$ajax);
    }
    public static function get_id($uname,$ajax=false){
        return dbHandler::dql('SELECT USER_ID FROM user WHERE USERNAME = :uname', array(':uname'=>$uname),$ajax)['USER_ID'];
    }
    public static function get_uname($id){
        return dbHandler::dql('SELECT USERNAME FROM user WHERE USER_ID = :id', array(':id'=>$id))['USERNAME'];
    }
    public static function get_password($uname){
        return dbHandler::dql('SELECT HASH FROM user WHERE USERNAME = :uname', array(':uname'=>$uname))['HASH'];
    }
    public static function set_umail($umail,$id,$ajax=false){
        return dbHandler::execute('UPDATE user SET USERMAIL = :umail WHERE USER_ID = :id', array(':umail'=>$umail,':id'=>$id),$ajax);
    }
    public static function get_umail($id,$ajax=false){
        return dbHandler::dql('SELECT USERMAIL FROM user WHERE USER_ID = :id', array(':id'=>$id),$ajax)['USERMAIL'];
    }
    public static function set_password($passwd,$id,$key,$ajax=false){
        $passwd = Text::encrypt($passwd,$key);
        return dbHandler::execute('UPDATE user SET HASH = :passwd WHERE USER_ID = :id', array(':id'=>$id,':passwd'=>$passwd),$ajax);
    }
    public static function flag($status,$id){
        dbHandler::execute('UPDATE user SET ACTIVE = :status WHERE USER_ID = :id', array(':status'=>$status,':id'=>$id));
    }
    public static function signin($uname,$passwd,$key,$ajax){
        $state = dbHandler::dql('SELECT sf_signin(:uname) STATE',array(':uname'=>$uname),true)['STATE'];
        if(($report = intval(explode(';',$state)[0])) === 1){
            if(!self::is_valid_login($uname,$passwd,$key,$ajax)){  return '-4;password error';}
        }
        return $state;
    }
    public static function is_valid_login($uname, $passwd,$key,$ajax=false) {
        $uname = strtolower(trim($uname));
        $user = dbHandler::dql('SELECT HASH FROM user WHERE USERNAME = :uname', array(':uname'=>$uname),$ajax,PDO::FETCH_OBJ);
        if (empty($user)) { return false;}
        
        if (empty($user->HASH) && empty(trim($passwd))) { return true;}
        if (empty($user->HASH) && !empty(trim($passwd))) { return false;}
        
        $decrypt = Text::decrypt($user->HASH,$key);
        if($decrypt === $passwd){            return true;}
        else {    return false;}
    }
    public static function get_icon($id,$ajax=false){
        return dbHandler::dql('SELECT USERAVATAR FROM user WHERE USER_ID = :id', array(':id'=>$id),$ajax)['USERAVATAR'];
    }
    public static function set_icon($id,$ext='png',$ajax=false){
        return dbHandler::execute('UPDATE user SET USERAVATAR = :ext WHERE USER_ID = :id', array(':id'=>$id,':ext'=>$ext),$ajax);
    }
    public static function purge($uname,$ajax=false){
        return dbHandler::dql('SELECT sf_purge_acc(:uname) REPORT', array(':uname'=>trim($uname)),$ajax)['REPORT'];
    }
}
abstract class Group{
    public static function add($name,$desc,$access,$uid,$ajax=false) {
        $param = array(':name'=>$name,':desc'=>$desc,':access'=>$access,':uid'=>$uid);
        return dbHandler::dql('SELECT add_group(:name,:desc,:access,:uid) ID', $param, $ajax)['ID'];
    }
    public static function add_member($uname,$gid,$client,$ajax=false){
        $param = array(':uname'=>$uname,':gid'=>$gid,':client'=>$client);
        return dbHandler::dql('SELECT add_g_member(:uname,:gid,:client) STATUS', $param, $ajax)['STATUS'];
    }
    public static function add_repo($gid,$rid,$uid,$ajax=false){
        $param = array(':gid'=>$gid,':rid'=>$rid,':uid'=>$uid);
        return dbHandler::dql('SELECT add_g_repo(:gid,:rid,:uid) STATE', $param, $ajax)['STATE'];
    }
    
    public static function ls($uid,$ajax=false){
        return dbHandler::dql('SELECT g.*,(SELECT COUNT(REPO_ID) FROM group_repo WHERE GROUP_ID = g.GROUP_ID) REPOS,ug.USER_ID,ug.ADMIN FROM grouping g, user_group ug WHERE g.GROUP_ID = ug.GROUP_ID AND USER_ID = :uid', array(':uid'=>$uid), $ajax);
    }
    public static function ls_member($gid,$ajax=false){
        $param = array(':gid'=>$gid,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT *, :client CLIENT FROM GROUP_MEMBERS WHERE GROUP_ID = :gid', $param, $ajax);
    }
    public static function ls_repo($gid,$ajax=false){
        $param = array(':gid'=>$gid,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT *, :client CLIENT FROM GROUP_REPOS WHERE GROUP_ID = :gid', $param, $ajax);
    }
    public static function ls_repo_ready(){
        return dbHandler::dql('SELECT ', $param, $ajax);
    }

    public static function get_repo($rid,$gid,$ajax=false){
        return dbHandler::dql('SELECT *, :client CLIENT FROM GROUP_REPOS WHERE GROUP_ID = :gid && REPO_ID = :rid', array(':gid'=>$gid,':rid'=>$rid,':client'=>$_SESSION['id']), $ajax);
    }
    public static function get_member($gid,$uid,$ajax=false){
        return dbHandler::dql('SELECT * FROM GROUP_MEMBERS WHERE GROUP_ID = :gid && USER_ID = :uid', array(':gid'=>$gid,':uid'=>$uid), $ajax);
    }
    
    public static function get_icon($id,$ajax=false){
        return dbHandler::dql('SELECT GROUP_BANNER FROM grouping WHERE GROUP_ID = :id', array(':id'=>$id),$ajax)['GROUP_BANNER'];
    }
    public static function set_icon($id,$ext='png',$ajax=false){
        return dbHandler::execute('UPDATE grouping SET GROUP_BANNER = :ext WHERE GROUP_ID = :id', array(':id'=>$id,':ext'=>$ext),$ajax);
    }
    
    public static function mv_member($uid,$gid,$client,$ajax=false){
        $param  = array(':uid'=>$uid,':gid'=>$gid,':client'=>$client);
        return dbHandler::dql('SELECT mv_g_member(:uid,:gid,:client) STATUS', $param, $ajax)['STATUS'];
    }
    public static function rm_member($uid,$gid,$client,$ajax=false){
        $param = array(':uid'=>$uid,':gid'=>$gid,':client'=>$client);
        return dbHandler::dql('SELECT rm_g_member(:uid,:gid,:client) STATUS', $param, $ajax)['STATUS'];
    }
    public static function count($relation,$gid,$ajax = false){
        return dbHandler::dql("SELECT COUNT(*) Q FROM $relation WHERE GROUP_ID = :gid", array(':gid'=>$gid), $ajax)['Q'];
    }
    public static function is_admin($gid,$uid,$ajax=false){
        return dbHandler::dql('SELECT ADMIN FROM user_group WHERE GROUP_ID = :gid AND USER_ID = :uid', array(':gid'=>$gid,':uid'=>$uid), $ajax)['ADMIN'];
    }
    public static function latest_member($gid,$ajax = false){
        return dbHandler::dql("SELECT (SELECT USERNAME FROM user u WHERE u.USER_ID = this.USER_ID) USER, ADDED FROM user_group this WHERE this.ADDED = (SELECT MAX(ADDED) FROM user_group WHERE GROUP_ID = :gid) LIMIT 1", array(':gid'=>$gid), $ajax);
    }
    public static function latest_repo($gid,$ajax = false){
        return dbHandler::dql("SELECT (SELECT REPO_NAME FROM repo r WHERE r.REPO_ID = this.REPO_ID) REPO, ADDED FROM group_repo this WHERE this.ADDED = (SELECT MAX(ADDED) FROM group_repo WHERE GROUP_ID = :gid) LIMIT 1", array(':gid'=>$gid), $ajax);
    }
    public static function repo_select($uid,$gid,$ajax=false){
        $param = array(':uid'=>$uid,':gid'=>$gid);
        return dbHandler::dql('SELECT REPO_ID, REPO_NAME FROM repo WHERE USER_ID = :uid AND REPO_ID NOT IN (SELECT REPO_ID FROM group_repo WHERE GROUP_ID = :gid)', $param, $ajax);
    }
    
    
    public static function rm_repo($rid,$gid,$client,$ajax=false){
        $param = array(':rid'=>$rid,':gid'=>$gid,':client'=>$client);
        return dbHandler::dql('SELECT rm_g_repo(:rid,:gid,:client) STATUS', $param, $ajax)['STATUS'];
    }
    
    public static function update($name,$desc,$public,$id,$ajax=false) {
        $param = array(':id'=>$id,':name'=>$name,':desc'=>$desc,':public'=>$public,);
        return dbHandler::execute('UPDATE grouping SET GROUP_NAME = :name, DESCRIPTION = :desc, PUBLIC = :public WHERE GROUP_ID = :id',$param,$ajax);
    }
    public static function leave($gid,$ajax=false){
        $param = array(':uid'=>$_SESSION['id'],':gid'=>$gid,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT rm_g_member(:uid,:gid,:client) STATUS', $param, $ajax)['STATUS'];
    }
    
}