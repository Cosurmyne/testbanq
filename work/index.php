<?php

error_reporting(-1);
ini_set('display_errors', 1);

$level = 2;
require_once dirname(__FILE__, $level) . '/main/epiqworx/logic/sample.php';
require_once dirname(__FILE__, $level) . '/main/epiqworx/db/handler.php';
require_once dirname(__FILE__, $level) . '/main/model.php';
require_once dirname(__FILE__, $level) . '/profile/model.php';
require_once 'model.php';
$uname = $uid = null;
$ICON = PATH . '/main/usr/img/sys/logo.png';
$session = 'offline';
$haspass = true;
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if(!isset($_SESSION['uname'])){  header("Location:".PATH);}  //------ return to home path if under no session
else{
    $uname = $_SESSION['uname'];
    $uid = $_SESSION['id'];
    $session = 'online';
    if ($haspass = empty(Account::get_password($uname))) {
        header('location:../profile?action=profile-edit_pass');
    }
    if(!empty($avtr = Account::get_icon($uid))){
        $ICON = PATH . "/main/usr/img/user/avatar/$uid.$avtr";
    }
}
$connected = dbAccess::test('epiqworx');
$nav = $flash = null;   //------------------------------------------------------------- Color Fill Special Effect on Page Load (inactive)
$err = array();

// Menu Navigation Array
$current = array(
    'home' => array(null, 'href="."'),
    'services' => array(null, 'href="?action=services"'),
    'work' => array(null, 'href="?action=work-item1"'),
    'notifications' => array(null, 'href="?action=notifications"'),
    'about-product' => array(null, 'href="?action=about-product"'),
    'notifications' => array(null, 'href="../profile?action=notifications"'),
    'about-team' => array(null, 'href="?action=about-team"'),
    'about-tnc' => array(null, 'href="?action=about-tnc"'),
    'profile-edit' => array(null, 'href="../profile?action=profile-edit_one"'),
    'profile-conn' => array(null, 'href="../profile?action=profile-conn"'),
    'profile-signout' => array(null, 'href="../profile?action=profile-signout"'),
    'help-purge' => array(null, 'href="?action=help-purge"'),
    'help-reset' => array(null, 'href="?action=help-reset"'),
    'help-man' => array(null, 'href="?action=help-man"'),
    'help-faq' => array(null, 'href="?action=help-faq"')
);
$active = array('about' => null, 'help' => null, 'profile' => null, 'work' => null);
// Section Navigation Array
$section = array(
    'item1' => array(null,'href="?action=work-item1"'),
    'item2' => array(null,'href="?action=work-item2"'),
    'item3' => array(null,'href="?action=work-item3"'),
    'item4' => array(null,'href="?action=work-item4"')
);
/*
 * start controller
 */
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'work-item1';
    }
}
if(explode('-',$action)[0]=== 'work'){
$section[explode('-',$action)[1]][0] = 'active';
$section[explode('-',$action)[1]][1] = null;
}
$current[explode('_',$action)[0]][0] = $active[explode('-', $action)[0]] = 'current';
$current[explode('_',$action)[0]][1] = null;
switch ($action) {
    case 'work-item1':
        $title = 'Databank';
        $script = PATH . '/main/usr/php/ajax/item1.php';
        require_once 'view/page-item1.php';
        break;
    case 'work-item2':
        $title = 'Topics';
        $script = PATH . '/main/usr/php/ajax/item2.php';
        require_once 'view/page-item2.php';
        break;
    case 'work-item3':
        $title = 'Scripts';
        $script = PATH . '/main/usr/php/ajax/item3.php';
        require_once 'view/page-item3.php';
        break;
    case 'work-item4':
        $title = 'Stats';
        $script = PATH . '/main/usr/php/ajax/item4.php';
        require_once 'view/page-item4.php';
        break;
    case 'test':
        echo 'hello';
        break;
    default :
        header("location:" . PATH . "?action=$action");
        break;
}