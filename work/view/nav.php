<?php require_once dirname(__FILE__, 3) . '/main/epiqworx/template/noscript.html'; ?>
<div class="dash-wrap shadow js display-flex clearfix">
    <aside id="sideline" class="dml">
        <i id="m_btn_dml" class="fa fa-database content-key" title="item operations"></i>
        <div id="m_panel_dml">
            <ul>
                <li class="group-I off">
                        <button id="btn_sl_mk" type="button" class="sideline-btn" title="add"><i class="fa fa-plus"></i></button>
                    <div class="btn-shield display-block"></div>
                </li>
                <li class="group-I off">
                        <button id="btn_sl_mv" type="button" class="sideline-btn" title="update"><i class="fa fa-edit"></i></button>
                    <div class="btn-shield display-block"></div>
                </li>
                <li class="group-I off">
                        <button id="btn_sl_rm" type="button" class="sideline-btn" title="remove"><i class="fa fa-remove"></i></button>
                        <button id="vss_sl_rm" type="button" class="display-none" ><i class="fa fa-spinner fa-pulse"></i></button>
                    <div class="btn-shield display-block"></div>
                </li>
            </ul>
            <hr>
            <ul>
                <li class="group-II">
                        <input name="action" value="" type="hidden">
                        <button id="btn_sl_home" type="submit" title="home"><i class="fa fa-home"></i></button>
                    </li>
                <li class="group-II small-1280 big">
                    <button class="display-none" id="_btnback" title="back one level"><i class="fa fa-arrow-left fa-2x "></i></button>
                </li>
            </ul>
        </div>
        <input type="hidden" id="sideline_status" value="000"/>
    </aside>
    <article>
        <div class="small m-dex base-dotted-dark">
            <span class="icon-wrap display-none small" id="m_btn_back_to_ls" ><a href="javascript:m_crumb_nav()"><i class="fa fa-chevron-left"></i></a></span>
            <i id="m_nav_key" class="fa fa-ellipsis-h right content-key" title="item operations"></i>
            <ul class="breadcrumb" id="m_nav_crumb">
                <li class="crumb+" id="l0_slice"><a><?= $title;?></a></li>
            </ul>
        </div>
        <main class="dash-content table">
            <header id="menu_nav" class="menu-nav big-block">
                <ul class="streight-nav">
                    <li class="<?= $section['item1'][0]; ?>"><a <?= $section['item1'][1]; ?>>Question Databank</a></li>
                    <li class="<?= $section['item2'][0]; ?>"><a <?= $section['item2'][1]; ?>>Topics</a></li>
                    <li class="<?= $section['item3'][0]; ?>"><a <?= $section['item3'][1]; ?>>Scripts</a></li>
                    <li class="<?= $section['item4'][0]; ?>"><a <?= $section['item4'][1]; ?>>Statistics</a></li>
                </ul>
            </header>