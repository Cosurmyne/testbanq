<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
    <body id="workspace2" class="workspace">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img src="<?= $ICON; ?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once 'nav.php'; ?>
            <div class="main table">
                <div id="list_panel" class="table-cell list-panel">
                    <div id="instance_ls_shield" class="display-none"></div>
                    <ul id="instance_list"></ul>
                </div>
                <div id="panel_content" class="table-cell x768 content">
                    <div id="panel_default" class="text-pane">
                        <div id="instance_info">
                            <h1 id="instance_h1">No Topic Selected</h1>
                            <p id="instance_p1">Please select Topic to your left.</p>
                            <p id="instance_p2">Or use large cross to add one.</p>
                            <p id="instance_err"></p>
                        </div>
                    </div>
                    <div id="panel_dml" class="display-none">
                        <div class="input-form">
                            <ul class="form-input">
                                <li>
                                    <label class="prompt float-left">Category</label>
                                    <label class="required float-right">*</label>
                                    <select id="input_topic_00" ></select>
                                    <span class="cmc" id="cmc_00" title="what topic is this?"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Title</label>
                                    <label class="required float-right">*</label>
                                    <input type="text" id="input_topic_01" maxlength="35"/>
                                    <span class="cmc" id="cmc_01" title="label your topic"></span>
                                </li>
                                <li>
                                    <hr/>
                                    <span id="input_error_instance" class="message  err-msg"></span>
                                    <button id="btn_save_instance" class="button_1" title="update data" type="button">save</button>
                                    <button id="vss_save_instance" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                    <div class="instance_success-tick display-none" id="update_success">✔</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="panel_assign_quest" class="display-none">
                        <div class="input-form">
                            <ul class="form-input">
                                <li>
                                    <label class="prompt float-left">Source</label>
                                    <select id="input_topic_02" ></select>
                                    <span class="cmc" id="cmc_02" title="leave to 'local' for your own"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Repository</label>
                                    <select id="input_topic_03" ></select>
                                    <span class="cmc" id="cmc_03" title="source of question"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Question</label>
                                    <select id="input_topic_04" ></select>
                                    <span class="cmc" id="cmc_04" title="status quo"></span>
                                </li>
                                <li>
                                    <hr/>
                                    <span id="input_error_tquest" class="message  err-msg"></span>
                                    <button id="btn_save_tquest" class="button_1" title="update data" type="button">save</button>
                                    <button id="vss_save_tquest" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                    <div class="instance_success-tick display-none" id="update_success">✔</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <span id="content_err" class="err-report"></span>
                </div>
                <div id="panel_dash_aside" class="table-cell x768 sidebar">
                    <div id="panel_topic_quest" class="ls display-none">
                        <div class="ls-wrap">
                            <table id="tbl_ls_quest" class="dull normal"></table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer big-1280">
                <div class="sidebar">:</div>
                <div id="panel_bottombar_ctrl" class="content">
                    <div id="wrap_btn_back" class="icon-wrap display-none">:
                        <a id="btn_back" href="javascript:" title="back one level" >
                            <span><i class="fa fa-chevron-left"></i></span>
                        </a>
                    </div>
                    <ul id="bottom_nav_crumb">
                        <li id="bn_l0"><a><?= $title; ?></a></li>
                    </ul>
                </div>
                <div class="content r">
                    <ul class="streight-nav">
                        <li><div>Questions ::&nbsp;&nbsp;<span class="value" id="osd_3">null</span><span id="link_addx1" title="add Question" class="link_addx display-none"><i class="fa fa-plus"></i></span></div></li>
                        <li><div>Topics ::&nbsp;&nbsp;<span class="value" id="osd_1">null</span></div></li>
                    </ul>
                </div>
            </div>
        </main>
    </article>
</div>
<div class="small-1280 m-osd">
    <i id="m_tbl_osd_toggle" class="fa fa-info" title="toggle OSD visibility"></i>
    <table id="m_tbl_osd" class="osd display-none">
        <tr class="item">
            <td rowspan="2" class="ctrl-l ctrl l"><button id="btn_addx1" class="link_addx display-none"><i class="fa fa-plus"></i></button></td>
            <td class="index">?s</td><td class="gap" rowspan="2"></td>
            <td class="gap" rowspan="2"></td>
            <td class="index">Topics</td>
        </tr>
        <tr class="values">
            <td id="m_osd_3">null</td><td id="m_osd_1">null</td>
        </tr>
    </table>
</div>
</section>
<?php require_once dirname(__FILE__, 3) . "/main/view/default/footer.php"; ?>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/dash/media.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/dash/item2.js"></script>

</body>
</html>
