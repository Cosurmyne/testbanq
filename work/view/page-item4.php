<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
    <body id="workspace4" class="workspace">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img src="<?= $ICON; ?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once 'nav.php'; ?>
            <div class="main table">
                <div id="list_panel" class="table-cell list-panel">
                    <div id="instance_ls_shield" class="display-none"></div>
                    <ul id="instance_list"></ul>
                </div>
                <div id="panel_content" class="table-cell x768 content">
                    <div id="panel_default" class="text-pane">
                        <div id="instance_info">
                            <h1 id="instance_h1">No Stat Selected</h1>
                            <p id="instance_p1">Please select Stat to your left.</p>
                            <p id="instance_p2">Then use <b>controls</b> tweak to your needs.</p>
                            <p id="instance_err"></p>
                        </div>
                    </div>
                    <div id="panel_report" class="display-none">
                        <div id='chart_info' class="display-none"></div>
                        <div class="ct-chart ct-perfect-fourth"></div>
                    </div>
                </div>
                <div id="panel_dash_aside" class="big-cell sidebar">
                    <div id="panel_controls_pc" class="display-none">
                        <div id="panel_sec_preview_p1" class="">
                            <table id="tbl_sec_preview_p1" class="quest" >
                                <tr><td></td><td colspan="2" class="align-right" >INTERACTIVE REPORT</td></tr>
                                <tr><td colspan="3"><img alt="LOGO" src="<?= PATH; ?>/main/usr/img/sys/qb-ink.png" /></td></tr>
                                <tr><td colspan="3">date range section</td></tr>
                                <tr>
                                    <td class="symbol">report</td><td colspan="2" class="answer">
                                        <select id="cbx_foucus_action"><option value="V">Views</option><option value="D">Downloads</option></select>
                                    </td>
                                </tr>
                                <tr><td class="symbol">start</td><td colspan="2" class="answer"><input id="plc_start_date" min="<?= Dates::date_add(date('Y-m-d'), -30); ?>" max="<?= date('Y-m-d'); ?>" type="date" value="<?= Dates::week()['start']; ?>" /></td></tr>
                                <tr><td class="symbol">end</td><td colspan="2" class="answer"><input id="plc_end_date" value="<?= date('Y-m-d'); ?>" min="<?= Dates::date_add(date('Y-m-d'), -29); ?>"  max="<?= date('Y-m-d'); ?>" type="date" /></td></tr>
                            </table>
                        </div>
                        <div id="panel_sec_preview_p2" class=" display-none">
                            <table id="tbl_sec_preview_p2" class="quest" >
                                <tr><td></td><td colspan="2" class="align-right" >INTERACTIVE REPORT</td></tr>
                                <tr><td colspan="3" class="height-200px"><table id="tbl_ls_quest" class="dull"></table></td></tr>
                                <tr><td colspan="3">question scope selection</td></tr>
                                <tr><td class="symbol">source</td><td colspan="2" class="answer"><select id="select_group_sec_preview_p2"></select></td></tr>
                                <tr><td class="symbol">repo</td><td colspan="2" class="answer"><select id="select_repo_sec_preview_p2"></select></td></tr>
                            </table>
                        </div>
                        <footer class="menu-nav">
                            <ul id="prev_switch_bar" class="streight-nav">
                                <li id="sec_preview_p1" class="section-preview-nav active"><span>Legend</span></li>
                                <li id="sec_preview_p2" class="section-preview-nav"><span>Selection</span></li>
                            </ul>
                        </footer>
                        <br/>
                    </div>
                    <div id="panel_controls_ri" class="display-none">
                        <table id="tbl_sec_preview_ri" class="quest" >
                            <tr><td></td><td colspan="2" class="align-right" >INTERACTIVE REPORT</td></tr>
                            <tr><td colspan="3"><img alt="LOGO" src="<?= PATH; ?>/main/usr/img/sys/qb-ink.png" /></td></tr>
                            <tr><td colspan="3">data selection section</td></tr>
                            <tr><td class="symbol">source</td><td colspan="2" class="answer"><select id="select_group_sec_preview_ri"></select></td></tr>
                        </table>
                        <br/>
                    </div>
                </div>
            </div>
            <div class="footer big-1280">
                <div class="sidebar">:</div>
                <div id="panel_bottombar_ctrl" class="content">
                    <div  id="btn_back" class="icon-wrap display-none">:
                        <a href="#" title="back to list">
                            <span><i class="fa fa-chevron-left"></i></span>
                        </a>
                    </div>
                    <ul id="bottom_nav_crumb">
                        <li id="bn_l0"><a><?= $title; ?></a></li>
                    </ul>
                </div>
                <div class="content r">
                    <ul class="streight-nav">
                        <li><div>Stats ::&nbsp;&nbsp;<span class="value" id="osd_1">null</span></div></li>
                    </ul>
                </div>
            </div>
        </main>
    </article>
</div>
<div class="small-1280 m-osd">
    <i id="m_tbl_osd_toggle" class="fa fa-info" title="toggle OSD visibility"></i>
    <table id="m_tbl_osd" class="osd display-none">
        <tr class="item">
            <td class="index">Stats</td>
        </tr>
        <tr class="values">
            <td id="m_osd_1">null</td>
        </tr>
    </table>
</div>
</section>
<?php require_once dirname(__FILE__, 3) . "/main/view/default/footer.php"; ?>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/library/chartist/chartist.min.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/dash/item4.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/dash/media.js"></script>
</body>
</html>
