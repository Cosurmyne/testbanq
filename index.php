<?php

error_reporting(-1);
ini_set('display_errors', 1);

$level = 1;
require_once('main/epiqworx/logic/sample.php');
require_once('main/epiqworx/db/handler.php');
require_once('main/model.php');
require_once('profile/model.php');

$nav = $flash = null;
$ICON = PATH . '/main/usr/img/sys/logo.png';
$err = array();
$uname = $uid = null;
$session = 'offline';
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if(isset($_SESSION['uname'])){
    $uname = $_SESSION['uname'];
    $uid = $_SESSION['id'];
    $session = 'online';
    if (empty(Account::get_password($uname))) {
        header('location:profile?action=profile-edit_pass');
    }
    if(!empty($avtr = Account::get_icon($uid))){
        $ICON = PATH . "/main/usr/img/user/avatar/$uid.$avtr";
    }
}
// Menu Navigation Array

$connected = dbAccess::test('testbank'); //  ------------------------------------ test database connection
$current = array(
    'home' => array(null, 'href="."'),
    'services' => array(null, 'href="?action=services"'),
    'work' => array(null, 'href="work?action=work-item1"'),
    'notifications' => array(null, 'href="profile?action=notifications"'),
    'about-product' => array(null, 'href="?action=about-product"'),
    'about-team' => array(null, 'href="?action=about-team"'),
    'about-tnc' => array(null, 'href="?action=about-tnc"'),
    'profile-edit' => array(null, 'href="profile?action=profile-edit_one"'),
    'profile-conn' => array(null, 'href="profile?action=profile-conn"'),
    'profile-signout' => array(null, 'href="profile?action=profile-signout"'),
    'help-purge' => array(null, 'href="?action=help-purge"'),
    'help-reset' => array(null, 'href="?action=help-reset"'),
    'help-man' => array(null, 'href="?action=help-man"'),
    'help-faq' => array(null, 'href="?action=help-faq"')
);
$active = array('about' => null, 'help' => null, 'profile' => null, 'work' => null);
/*
 * start controller
 */
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'home';
    }
}
$current[explode('_',$action)[0]][0] = $active[explode('-', $action)[0]] = 'current';
$current[explode('_',$action)[0]][1] = null;
switch ($action) {
    case 'home':
        $title = "Testanq :: Home";    //-------------------------------------- Page Title
        $flash = 'flash';   //-------------------------------------------------- Color Fill Special Effect on Page Load (active)
        $current[0] = 'current';
        //==========================================================================
        require_once ('main/view/default/home.php');
        break;
    case 'services':
        $title = "Testbanq :: Services";    //---------------------------------- Page Title
        //==========================================================================
        require_once ('main/view/page/services.php');
        break;
    case 'help-man':
        $title = "Help :: Manual";    //---------------------------------- Page Title
        //==========================================================================
        require_once ('main/view/page/help-man.php');
        break;
    case 'help-faq':
        $title = "Help :: FAQ";    //---------------------------------- Page Title
        //==========================================================================
        require_once ('main/view/page/help-faq.php');
        break;
    case 'help-purge':
        $title = "Help :: Email Reset";    //---------------------------------- Page Title
        //==========================================================================
        $field = filter_input(INPUT_POST, 'field');
        require_once ('main/view/page/help-purge.php');
        break;
    case 'help-reset':
        $title = "Help :: Login Reset";    //---------------------------------- Page Title
        //==========================================================================
        $field = filter_input(INPUT_POST, 'field');
        require_once ('main/view/page/help-reset.php');
        break;
    case 'about-product':
        $title = "About :: Product";    //---------------------------------- Page Title
        //==========================================================================
        require_once ('main/view/page/about-product.php');
        break;
    case 'about-team':
        $title = "About :: Maintainance";    //---------------------------------- Page Title
        //==========================================================================
        require_once ('main/view/page/about-team.php');
        break;
    case 'about-tnc':
        $title = "About :: Terms of Use";    //---------------------------------- Page Title
        //==========================================================================
        require_once ('main/view/page/about-tnc.php');
        break;
    case 'error':
        $error_message = filter_input(INPUT_POST, 'msg');
        if ($error_message == NULL) {
            $error_message = filter_input(INPUT_GET, 'msg');
        }
        if(file_exists('main/view/error/generic.php')){$title = "Error";require_once 'main/view/error/generic.php';}
        else {echo $error_message;}
        break;
    case 'test':
        echo explode('_','hello-eminem_ij')[0];
        break;
    case 'phpinfo':
        phpinfo();
        break;
    default:
        $error_message = "case not handled for action '<strong>$action</strong>'";
        $err_page = 'main/view/error/generic.php';
        if (file_exists($err_page)) {
            $title = "Error";
            require_once $err_page;
        } else {
            echo $error_message;
        }
        break;
}