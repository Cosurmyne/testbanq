<?php
require_once dirname(__FILE__,1).'/epiqworx/db/reuse.php';
abstract class Generic{
    public static function add_query($fname,$lname,$email,$subject,$query){
        $param = array(':fname'=>$fname,':lname'=>$lname,':email'=>$email,':subject'=>$subject,':query'=>$query);
        return dbHandler::execute('INSERT INTO query(FIRST_NAME,LAST_NAME,EMAIL_ADDRESS,QUERY_SUBJECT,QUERY_TEXT) VALUES(:fname,:lname,:email,:subject,:query)',$param);
    }
}
