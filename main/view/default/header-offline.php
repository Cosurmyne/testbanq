<nav class="float-right">
    <div id="mkey" class="menu-key small">
        <div class="bar1 bar"></div>
        <div class="bar2 bar"></div>
        <div class="bar3 bar"></div>
    </div>
    <ul class="small">
        <li class="<?=$current['home'][0];?>"><a <?=$current['home'][1]?>>Home</a></li>
        <li class="<?=$current['services'][0];?>"><a <?=$current['services'][1];?>>Services</a></li>
        <li>
            <div class="dropdown">
                <button class="dropbtn <?= $active['help']; ?>">Help</button>
                <div class="dropdown-content">
                    <a <?=$current['help-faq'][1];?> class="submenu top-edge <?=$current['help-faq'][0];?>">F&SmallCircle;A&SmallCircle;Q</a>
                    <a <?=$current['help-purge'][1];?> class="submenu isolate <?=$current['help-purge'][0];?>">mistyped email</a>
                    <a <?=$current['help-reset'][1];?> class="submenu <?=$current['help-reset'][0];?>">forgot login</a>
                </div>
            </div>
        </li>
        <li>
            <div class="dropdown">
                <button class="dropbtn <?= $active['about']; ?>">About</button>
                <div class="dropdown-content">
                    <a <?=$current['about-product'][1];?> class="submenu top-edge <?=$current['about-product'][0];?>">this product</a>
                    <a <?=$current['about-team'][1];?> class="submenu <?=$current['about-team'][0];?>">maintenance</a>
                    <a <?=$current['about-tnc'][1];?> class="submenu  isolate <?=$current['about-tnc'][0];?>">terms of service</a>
                </div>
            </div>
        </li>
    </ul>
    <div class="big keys">
        <div class="dropdown float-right <?=$flash;?> <?=$active['about'];?>">
            <button class="dropbtn <?= $active['about']; ?>">About</button>
            <div class="dropdown-content">
                <a <?=$current['about-product'][1];?> class="isolate <?=$current['about-product'][0];?>">this product</a>
                <a <?=$current['about-team'][1];?> class="<?=$current['about-team'][0];?>">maintenance</a>
                <a <?=$current['about-tnc'][1];?> class="isolate <?=$current['about-tnc'][0];?>">terms of service</a>
            </div>
        </div>
        <div class="dropdown float-right <?=$flash;?>">
            <button class="dropbtn <?= $active['help'] ?>">Help</button>
            <div class="dropdown-content">
                <a <?=$current['help-faq'][1];?> class="isolate <?=$current['help-faq'][0];?>">F&SmallCircle;A&SmallCircle;Q</a>
                <a <?=$current['help-purge'][1];?> class="isolate <?=$current['help-purge'][0];?>">mistype email</a>
                <a <?=$current['help-reset'][1];?> class="<?=$current['help-reset'][0];?>">forgot login</a>
            </div>
        </div>
        <a <?=$current['services'][1];?> class="float-right <?=$current['services'][0];?>">Services<//a>
        <a <?=$current['home'][1];?> class="float-right <?=$current['home'][0];?>">Home</a>
    </div>
</nav>