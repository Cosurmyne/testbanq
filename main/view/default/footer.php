<?php require_once dirname(__FILE__, 3) . '/epiqworx/template/assistantpane.html'; ?>
<footer class="main">
    <p>Testbanq :: <span id="foot_year"><?=date('Y');?></span>  &copy <span id="user_online"><?= htmlspecialchars($uname);?></span></p>
    <input type="hidden" id="PATH" value="<?=PATH;?>"/>
    <input type="hidden" id="experimental" value="<?= isset($_GET['experimental']);?>" />
</footer>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/ajax.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/string.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/template.js"></script>