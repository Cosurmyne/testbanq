<section id="home_acc_sec">
  <noscript>
    <div style="text-align:center">please use a JavaScript enabled browser to continue</div>
  </noscript>
  <span id="acc_err"></span>
  <div class="container js clearfix">
    <a id="accswitch"></a><span class="focus-right"><i class="fa fa-4x fa-angle-left bounce-x"></i><i class="fa fa-4x fa-angle-left bounce-x"></i></span>
    <div class="std">
      <input id="acc_sec_action" type="hidden" name="action" value="login"/>
      <span class="acc-intro-cmpnt popup">
        <span id="popupfield1" class="popuptext error"></span>
        <input type="text" name="input-1" id="tf_acc_sec_1" title="username" maxlength="20" required="" minlength="2"/>
      </span>
      <span class="acc-intro-cmpnt popup">
        <span id="popupfield2" class="popuptext error"></span>
        <input type="password" name="input-2" id="tf_acc_sec_2" maxlength="45"/>
      </span>
      <span class="spacer">
        <button type="button" id="btn_acc_sec" class="button_1 acc-intro-cmpnt">Sign In</button>
        <button  id="signup_vss" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
      </span>
    </div>
  </div>
</section>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/home.js"></script>