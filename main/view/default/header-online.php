<nav class="float-right">
    <div id="mkey" class="menu-key small">
        <div class="bar1 bar"></div>
        <div class="bar2 bar"></div>
        <div class="bar3 bar"></div>
    </div>
    <ul class="small">
        <li class="<?=$current['work'][0];?>"><a class="<?=$active['work'];?>" <?=$current['work'][1]?>>Workspace</a></li>
        <li class="<?=$current['notifications'][0];?>"><a <?=$current['notifications'][1];?>>Notifications</a></li>
        <li>
            <div class="dropdown">
                <button class="dropbtn <?= $active['profile']; ?>">Profile</button>
                <div class="dropdown-content">
                    <a <?=$current['profile-edit'][1];?> class="submenu top-edge <?=$current['profile-edit'][0];?>">User Details</a>
                    <a <?=$current['profile-conn'][1];?> class="submenu isolate <?=$current['profile-conn'][0];?>">Connections</a>
                    <a <?=$current['profile-signout'][1];?> class="submenu isolate">Sign Out</a>
                </div>
            </div>
        </li>
        <li>
            <div class="dropdown">
                <button class="dropbtn <?= $active['help']; ?>">Help</button>
                <div class="dropdown-content">
                    <a <?=$current['help-man'][1];?> class="submenu top-edge <?=$current['help-man'][0];?>">Manual</a>
                    <a <?=$current['help-faq'][1];?> class="submenu  <?=$current['help-faq'][0];?>">F&SmallCircle;A&SmallCircle;Q</a>
                </div>
            </div>
        </li>
    </ul>
    <div class="big keys">
        <div class="dropdown float-right <?=$flash;?>">
            <button class="dropbtn <?= $active['help']; ?>">Help</button>
            <div id="big_menu_help" class="dropdown-content">
                <a id="big_menu_man" <?=$current['help-man'][1];?> class="isolate <?=$current['help-man'][0];?>">Manual</a>
                <a id="big_menu_faq" <?=$current['help-faq'][1];?> class=" <?=$current['help-faq'][0];?>">F&SmallCircle;A&SmallCircle;Q</a>
            </div>
        </div>
        <div class="dropdown float-right <?=$flash;?>">
            <button class="dropbtn <?= $active['profile'] ?>">Profile</button>
            <div class="dropdown-content">
                <a <?=$current['profile-edit'][1];?> class="isolate <?=$current['profile-edit'][0];?>">User Details</a>
                <a <?=$current['profile-conn'][1];?> class="isolate <?=$current['profile-conn'][0];?>">Connections</a>
                <a <?=$current['profile-signout'][1];?> class="isolate">Sign Out</a>
            </div>
        </div>
        <a <?=$current['notifications'][1];?> class="float-right <?=$current['notifications'][0];?>">Notifications</a>
        <a <?=$current['work'][1];?> class="float-right <?=$active['work'];?>">Workspace</a>
    </div>
</nav>