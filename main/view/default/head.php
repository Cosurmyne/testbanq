<head>
    <meta charset="utf-8"/>
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?= PATH; ?>/main/usr/img/sys/logo.png">
    <link rel="stylesheet" type="text/css" href="<?= PATH; ?>/main/usr/library/fontawesome/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= PATH; ?>/main/usr/library/croppie/croppie.css" />
    <link rel="stylesheet" href="<?= PATH; ?>/main/usr/library/chartist/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="<?= PATH; ?>/main/usr/css/main.css">
</head>