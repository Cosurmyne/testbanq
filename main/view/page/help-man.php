<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 2) . "/default/head.php";
    require_once dirname(__FILE__, 3) . '/usr/library/parsedown/Parsedown.php';
    require_once dirname(__FILE__, 3) . '/usr/library/parsedown/ParsedownExtra.php';
    $Parsedown = new Parsedown();
    $ParsedownExtra = new ParsedownExtra();
    ?>
    <body id="help_man" class="help-page">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img src="<?=$ICON;?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 2) . "/default/header-$session.php"; ?>
                </div>
            </header>
            <div id="help_wrap" class="help-wrap display-flex">
                <div class="container">
                    <article id="manual_article">
                        <?php
                            $file = dirname(__FILE__, 3) . "/usr/md/help/main-faq.md";
                            if (file_exists($file) && ($handle = fopen($file, "r"))) {
                                $message = "";
                                if(filesize($file)>0){$message = fread($handle, filesize($file));}
                                fclose($handle);
                            } else {
                                header("location: ?action=error&msg=Loading files failed!$$ Couldn't locate directory: $file");
                            }
                            echo $ParsedownExtra->text($message);
                            ?>
                    </article>
                    <aside>
                        <div id="manual_aside" class="dark">
                            <?php
                                    $file = dirname(__FILE__, 3) . "/usr/md/help/aside-faq.md";
                                    if (file_exists($file) && ($handle = fopen($file, "r"))) {
                                        $message = "";
                                        if(filesize($file)>0){$message = fread($handle, filesize($file));}
                                        fclose($handle);
                                    } else {
                                        header("location: ?action=error&msg=Loading files failed!$$ Couldn't locate directory: $file");
                                    }
                                    echo $ParsedownExtra->text($message);
                                    ?>
                        </div>
                    </aside>
                </div>
            </div>
        </section>
        <?php require_once dirname(__FILE__, 2) . '/default/footer.php'; ?>
    </body>
</html>