<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 2) . "/default/head.php";
    require_once dirname(__FILE__, 3) . '/usr/library/parsedown/Parsedown.php';
    require_once dirname(__FILE__, 3) . '/usr/library/parsedown/ParsedownExtra.php';
    $Parsedown = new Parsedown();
    $ParsedownExtra = new ParsedownExtra();
?>
    <body id="services_page">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img src="<?= $ICON;?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 2) . "/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once dirname(__FILE__, 3) . '/epiqworx/template/noscript.html'; ?>
            <div id="desc_wrap" class="desc-wrap js display-flex">
                <div class="container">
                    <article id="services_article">
                        <?php 
                            $file = dirname(__FILE__, 3) . "/usr/md/about/main-services.md";
                            if (file_exists($file) && ($handle = fopen($file, "r"))) {
                                $message = "";
                                if(filesize($file)>0){$message = fread($handle, filesize($file));}
                                fclose($handle);
                            } else {
                                header("location: ?action=error&msg=Loading files failed!$$ Couldn't locate file: $file");
                            }
                            echo $Parsedown->text($message);
                            ?>
                    </article>
                    <aside>
                        <?php require_once dirname(__FILE__, 3) . '/epiqworx/template/queries.php'; ?>
                    </aside>
                </div>
            </div>
        </section>
        <?php require_once dirname(__FILE__, 2) . '/default/footer.php'; ?>
	<script type="text/javascript" src="<?= PATH;?>/main/usr/js/desc.js"></script>
	<script src="<?= PATH;?>/main/usr/library/highlight/highlight.pack.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>
    </body>
</html>
