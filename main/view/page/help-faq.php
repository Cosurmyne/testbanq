<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 2) . "/default/head.php"; ?>
    <body id="help_faq"  class="help-page">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img src="<?= $ICON; ?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 2) . "/default/header-$session.php"; ?>
                </div>
            </header>
            <div id="help_wrap" class="help-wrap display-flex">
                <div class="container">
                    <div id="panel_faq_empty" class="shadow feature">
                        <div>
                            <h1>Frequently Asked Questions</h1>
                            <p>This section outlines questions frequent with the <b>Testbanq</b> user base.</p>
                            <p>However, questions must have been addressed by staff and management of the <b>Testbanq</b> system</p>
                            <p>As we stand, you're seeing this message because the list is currently empty.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php require_once dirname(__FILE__, 2) . '/default/footer.php'; ?>
    </body>
</html>