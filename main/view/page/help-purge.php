<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 2) . "/default/head.php"; ?>
    <body id="help_purge" class="help-page">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img src="<?=$ICON;?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 2) . "/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once dirname(__FILE__, 3) . '/epiqworx/template/noscript.html'; ?>
            <div id="help_wrap" class="help-wrap shadow js display-flex">
                <div class="container">
                    <article class="content">
                        <div id="workspace">
                            <h1 id="h1_feedback"> Purge Account</h1>
                            <p class="process-desc">please provide your <b>username</b>  on text field below, and we'll reverse the process of creating this account from our server<br><br>This, undoubtedly, will allow you to start over with a correct email address.</p>
                            <form action="#" method="post" class="std">
                                <input name="action" value="help-purge" type="hidden">
                                <input maxlength="20" id="tf_subject" name="field" required="" type="text" value="<?=$field;?>">
                                <div class="grouping">
                                    <button type="button" class="button_1" id="btn_submit">purge</button>
                                    <div class="success-tick display-none" id="update_vss">✔</div>
                                    <span id="sending_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
                                </div>
                            </form>
                            <p class="heads-up">process will:</p><ul><li>Remove Database <b>Record</b></li><li>Delete <b>Home Directory</b> and User <b>Files</b></li></ul><p></p>
                            <p class="err" id="reset_stumble"></p>
                        </div>
                    </article>
                    <aside class="banner">
                        <div class="screen flash">
                            <img src="<?=PATH;?>/main/usr/img/sys/qb-ink.png" alt="banner">
                            <div id="brand">testbank</div>
                            <div id="motto">the lecturer's best friend</div>
                        </div>
                    </aside>
                </div>
            </div>
        </section>
        <?php require_once dirname(__FILE__, 2) . '/default/footer.php'; ?>
        <script type="text/javascript" src="<?=PATH;?>/main/usr/js/help/support.js"></script>
    </body>
</html>