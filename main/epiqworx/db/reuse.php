<?php
abstract class dbAccess{
    public static function test($value,$ajax=false){
        $test = dbHandler::dql('SELECT DATABASE() DB',null,$ajax)['DB'];
        if ("$test" === "$value") {
            return true;
        }
        return false;
    }
    public static function get_all($relation){
        return dbHandler::dql("SELECT * FROM $relation",null,true);
    }

    public static function get_data($relation,$id_field,$id,$ajax=false){
        return dbHandler::dql("SELECT * FROM $relation WHERE $id_field = :id", array(':id'=>$id),$ajax);
    }
    public static function count($relation,$key=null,$value=null,$field='*'){
        if(!isset($key) || (empty($key)) || (!isset($value) || empty($value))) {return dbHandler::dql("SELECT COUNT($field) RECORDS FROM $relation",null,true)['RECORDS'];}
        return dbHandler::dql("SELECT COUNT($field) RECORDS FROM $relation WHERE $key = :value",array(':value'=>$value),true)['RECORDS'];
    }

    public static function record_exists($relation, $field, $value,$ajax=false) {
        $sql = "SELECT ((SELECT COUNT($field) FROM $relation WHERE $field = :value) > 0) FOUND";
        $params = array(':value'=>$value);
        return dbHandler::dql($sql,$params,$ajax)['FOUND'];
    }
    public static function delete($relation, $id, $value,$ajax=false) {
        $sql = "DELETE FROM $relation WHERE $id = :value";
        $params = array(':value'=>$value);
        return dbHandler::execute($sql,$params,$ajax);
    }
    public static function clear_field($relation, array $fields, $default, $key, $value,$ajax=false) {
         $length = count($fields);
            $x = 0;
            $subjects = "";
            foreach ($fields as $field) {
                $x++;
                if ($x < $length) {
                    $subjects .= "$field = :empty" . ' , ';
                } else {
                    $subjects .= "$field = :empty";
                }
            }
            $sql = "UPDATE $relation SET $subjects WHERE $key = :value";
            $params = array(':value'=>$value,':empty'=>$default);
            return dbHandler::execute($sql,$params,$ajax);
    }
    public static function enum_dropdown($relation, $enum,$ajax=false) {
        $sql = "SHOW COLUMNS FROM $relation WHERE FIELD = :enum";
        $params = array(':enum' => $enum);
        $record = dbHandler::dql($sql, $params,$ajax);
        $type = substr($record["Type"], 6, (strlen($record["Type"]) - 8));
        return explode("','", $type);
    }

}
