"use strict";
function get_ajax(url, callback){
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('GET',url);
    xhr.onreadystatechange = function (){if(xhr.readyState>3 && xhr.status==200) callback(xhr.responseText);};
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.send();
    return xhr;
}
function post_ajax(url,data, callback){
    var params = typeof data == 'string' ? data : Object.keys(data).map(function(k){return encodeURIComponent(k)+'='+encodeURIComponent(data[k])}).join('&');
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST',url);
    xhr.onreadystatechange = function (){if(xhr.readyState>3 && xhr.status==200) callback(xhr.responseText);};
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(params);
    return xhr;
}
function url_exists(url){
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('HEAD',url,false);
    try{
        xhr.send();
    }catch(ex)
    {
        return false;
    }
    return xhr.status != 404;
}
function read_json(url,callback){
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    xhr.overrideMimeType('application/json');
    xhr.open('GET',url,true);
    xhr.onreadystatechange = function (){   if(xhr.readyState === 4 && xhr.status==200) callback(xhr.responseText);};
    xhr.send(null);
}
function read_md(url,json,destination){
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('GET', url, true);
    xhr.onreadystatechange = function(){if(xhr.readyState == 4 && xhr.status == 200){
        var md = new Remarkable('full',JSON.parse(json));
        destination.innerHTML = md.render(this.responseText);
    }};
    xhr.send();
}