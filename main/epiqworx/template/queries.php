<div id="query_pane" class="dark">
    <h3>queries</h3>
    <div class="form">
        <div><input type="text" id="tf_query_fname" placeholder="First Name" class="half" name="fname" /><input type="text" id="tf_query_lname" placeholder="Last Name" class="half push" name="lname" /></div>
        <div><input type="email" id="tf_query_email" title="Email Address" placeholder="Email Address" required name="email" class="" /></div>
        <div><textarea id="ta_query_text" required title="type query here..." placeholder="type query here..." style="min-height: 128px" name="query" class=""></textarea></div>
        <div style="position:relative">
            <button type="button" class="button_1" id="query_save">send</button>
            <span id="sending_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
            <div class="success-tick display-none" id="query_vss">&#10004;</div>
        </div>
        <input type="hidden" name="action" value="query"/>
        <input type="hidden" id="target" name="target" value="<?= $action; ?>"/>
    </div>
</div>
<style>
    #query_pane .form div { margin: 0 0 .3em 0;}
    #query_pane .form input.half {width: 49%;}
    #query_pane .form input.push {margin-left: 2%;}
    #query_pane .form input, #query_pane textarea {width: 100%;padding: 5px 0;border: 0;text-indent: 7pt;background-color: var(--main-bg-color);color: var(--yang);}
    #query_pane .form input:hover,  #query_pane .form textArea:hover{background-color: #e9e9e9}
    #query_pane .button_1[disabled],#query_pane .button_1[disabled]:hover{background-color:  var(--darker-color);opacity: initial;}
</style>
<script>
    // Error TextField Formatting
    function err_field(field, txt) {
        //  reseting elements
        field.classList.remove("err");
        field.title = field.placeholder;
        // check for error text
        if (typeof txt === "string" && txt.length > 0) {
            field.classList.add("err");
            field.title = txt;
        }
    }

    query_save.addEventListener('click', function (e) {
        // -------------------------------- validation
        if (tf_query_email.value.trim().length < 1) {
            err_field(tf_query_email, 'filed required!');
            return;
        }
        if (!is_email(tf_query_email.value)) {
            err_field(tf_query_email, 'email invalid');
            return;
        }

        if (ta_query_text.value.trim().length < 1) {
            err_field(ta_query_text, 'filed required!');
            return;
        }
        // ----------------------------------- button function
        switch_panels(sending_vss, query_save);
        
        post_ajax(window.localStorage.profile,
        'action=query&target='+target.value+'&fname='+tf_query_fname.value+'&lname='+tf_query_lname.value+'&email='+tf_query_email.value+'&querytext='+ta_query_text.value,
        function(data){console.log(data);
            switch(data){
                case 'success':
                    switch_panels(query_save, sending_vss);
                    tf_query_fname.value = tf_query_lname.value = tf_query_email.value = ta_query_text.value = null;
                    query_save.innerHTML = 'sent!';
                    query_save.disabled = true;
                    query_vss.classList.remove('display-none');
                    break;
                default:
                    
                    break;
            }
        });
    });
    tf_query_email.addEventListener('input', function (e) {
        err_field(this);
    });
    ta_query_text.addEventListener('input', function (e) {
        err_field(this);
    });

</script>