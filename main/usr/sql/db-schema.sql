-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: testbank
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `testbank`
--

/*!40000 DROP DATABASE IF EXISTS `testbank`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `testbank` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `testbank`;

--
-- Temporary table structure for view `GROUP_MEMBERS`
--

DROP TABLE IF EXISTS `GROUP_MEMBERS`;
/*!50001 DROP VIEW IF EXISTS `GROUP_MEMBERS`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `GROUP_MEMBERS` AS SELECT 
 1 AS `USER_ID`,
 1 AS `USERNAME`,
 1 AS `USERMAIL`,
 1 AS `FIRST_NAME`,
 1 AS `LAST_NAME`,
 1 AS `USERAVATAR`,
 1 AS `GROUP_ID`,
 1 AS `GROUP_NAME`,
 1 AS `GROUP_BANNER`,
 1 AS `ADMIN`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `GROUP_REPOS`
--

DROP TABLE IF EXISTS `GROUP_REPOS`;
/*!50001 DROP VIEW IF EXISTS `GROUP_REPOS`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `GROUP_REPOS` AS SELECT 
 1 AS `REPO_ID`,
 1 AS `REPO_NAME`,
 1 AS `USER_ID`,
 1 AS `DESCRIPTION`,
 1 AS `REPO_ICON`,
 1 AS `USERNAME`,
 1 AS `GROUP_ID`,
 1 AS `GROUP_NAME`,
 1 AS `GROUP_BANNER`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `QUEST_LIST`
--

DROP TABLE IF EXISTS `QUEST_LIST`;
/*!50001 DROP VIEW IF EXISTS `QUEST_LIST`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `QUEST_LIST` AS SELECT 
 1 AS `ID`,
 1 AS `REPO`,
 1 AS `CREATED`,
 1 AS `READY`,
 1 AS `QTYPE_DESC`,
 1 AS `QUESTION`,
 1 AS `USERNAME`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `REPO_LIST`
--

DROP TABLE IF EXISTS `REPO_LIST`;
/*!50001 DROP VIEW IF EXISTS `REPO_LIST`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `REPO_LIST` AS SELECT 
 1 AS `REPO_ID`,
 1 AS `REPO_NAME`,
 1 AS `USER_ID`,
 1 AS `DESCRIPTION`,
 1 AS `REPO_ICON`,
 1 AS `QUESTS`,
 1 AS `USERNAME`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `ANS_ID` varchar(23) CHARACTER SET utf8 NOT NULL,
  `CORRECT` tinyint(1) NOT NULL,
  `QUEST_ID` bigint(20) unsigned NOT NULL,
  `TEXT` varchar(45) NOT NULL,
  `MISCONCEPTION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ANS_ID`),
  UNIQUE KEY `TEXT` (`TEXT`,`QUEST_ID`),
  KEY `QUEST_ID` (`QUEST_ID`),
  CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`QUEST_ID`) REFERENCES `question` (`QUEST_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `associate`
--

DROP TABLE IF EXISTS `associate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `associate` (
  `USER_A` char(13) CHARACTER SET utf8 NOT NULL,
  `USER_B` char(13) CHARACTER SET utf8 NOT NULL,
  `REQUEST` char(13) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`USER_A`,`USER_B`),
  KEY `USER_B` (`USER_B`),
  CONSTRAINT `associate_ibfk_1` FOREIGN KEY (`USER_A`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `associate_ibfk_2` FOREIGN KEY (`USER_B`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `figure`
--

DROP TABLE IF EXISTS `figure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `figure` (
  `FIGURE_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FIGURE_LABEL` varchar(20) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FIGURE_TYPE` varchar(20) DEFAULT NULL,
  `REPO_ID` char(25) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`FIGURE_ID`),
  KEY `REPO_ID` (`REPO_ID`),
  CONSTRAINT `figure_ibfk_1` FOREIGN KEY (`REPO_ID`) REFERENCES `repo` (`REPO_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `focus_quest`
--

DROP TABLE IF EXISTS `focus_quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `focus_quest` (
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `QUEST_ID` bigint(20) unsigned NOT NULL,
  `ADVENT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Q_ACTION` enum('D','V') NOT NULL,
  PRIMARY KEY (`USER_ID`,`QUEST_ID`,`ADVENT`),
  KEY `QUEST_ID` (`QUEST_ID`),
  CONSTRAINT `focus_quest_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `focus_quest_ibfk_2` FOREIGN KEY (`QUEST_ID`) REFERENCES `question` (`QUEST_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_repo`
--

DROP TABLE IF EXISTS `group_repo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_repo` (
  `GROUP_ID` char(17) CHARACTER SET utf8 NOT NULL,
  `REPO_ID` char(25) CHARACTER SET utf8 NOT NULL,
  `ADDED` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`GROUP_ID`,`REPO_ID`),
  KEY `REPO_ID` (`REPO_ID`),
  CONSTRAINT `group_repo_ibfk_1` FOREIGN KEY (`GROUP_ID`) REFERENCES `grouping` (`GROUP_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `group_repo_ibfk_2` FOREIGN KEY (`REPO_ID`) REFERENCES `repo` (`REPO_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grouping`
--

DROP TABLE IF EXISTS `grouping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grouping` (
  `GROUP_ID` char(17) CHARACTER SET utf8 NOT NULL,
  `GROUP_NAME` varchar(20) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `GROUP_BANNER` char(3) DEFAULT NULL,
  `PUBLIC` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `LANG_ID` char(2) CHARACTER SET utf8 NOT NULL,
  `LANG_DESC` varchar(20) NOT NULL,
  PRIMARY KEY (`LANG_ID`),
  UNIQUE KEY `LANG_DESC` (`LANG_DESC`),
  KEY `DESCRIPTION` (`LANG_DESC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lecturer`
--

DROP TABLE IF EXISTS `lecturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lecturer` (
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`USER_ID`),
  CONSTRAINT `lecturer_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `NOTE_ID` char(12) CHARACTER SET utf8 NOT NULL,
  `HEADING` varchar(64) NOT NULL,
  `HAVE_READ` tinyint(1) NOT NULL DEFAULT '0',
  `RECEIVED` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`NOTE_ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paper`
--

DROP TABLE IF EXISTS `paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper` (
  `PAPER_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MODULE_CODE` char(5) CHARACTER SET utf8 DEFAULT NULL,
  `PAPER_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `PAPER_DESC` text,
  `VENUE` varchar(45) DEFAULT NULL,
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`PAPER_ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `paper_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `lecturer` (`USER_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `privacc`
--

DROP TABLE IF EXISTS `privacc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privacc` (
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`USER_ID`),
  CONSTRAINT `privacc_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `query`
--

DROP TABLE IF EXISTS `query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `query` (
  `QUERY_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(20) DEFAULT NULL,
  `LAST_NAME` varchar(20) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(45) NOT NULL,
  `QUERY_SUBJECT` varchar(20) DEFAULT NULL,
  `QUERY_TEXT` text NOT NULL,
  `QUERY_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`QUERY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `query_response`
--

DROP TABLE IF EXISTS `query_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `query_response` (
  `QUERY_ID` bigint(20) unsigned NOT NULL,
  `RESPONSE_ID` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`QUERY_ID`,`RESPONSE_ID`),
  KEY `RESPONSE_ID` (`RESPONSE_ID`),
  CONSTRAINT `query_response_ibfk_1` FOREIGN KEY (`QUERY_ID`) REFERENCES `query` (`QUERY_ID`) ON DELETE CASCADE,
  CONSTRAINT `query_response_ibfk_2` FOREIGN KEY (`RESPONSE_ID`) REFERENCES `response` (`RESPONSE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quest_figure`
--

DROP TABLE IF EXISTS `quest_figure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_figure` (
  `QUEST_ID` bigint(20) unsigned NOT NULL,
  `FIGURE_ID` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`QUEST_ID`,`FIGURE_ID`),
  KEY `FIGURE_ID` (`FIGURE_ID`),
  CONSTRAINT `quest_figure_ibfk_2` FOREIGN KEY (`FIGURE_ID`) REFERENCES `figure` (`FIGURE_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `quest_figure_ibfk_3` FOREIGN KEY (`QUEST_ID`) REFERENCES `question` (`QUEST_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quest_paper`
--

DROP TABLE IF EXISTS `quest_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_paper` (
  `QUEST_ID` bigint(20) unsigned NOT NULL,
  `PAPER_ID` bigint(20) unsigned NOT NULL,
  `WEIGHTING` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `NEGETIVE_MARK` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`QUEST_ID`,`PAPER_ID`),
  KEY `PAPER_ID` (`PAPER_ID`),
  CONSTRAINT `quest_paper_ibfk_2` FOREIGN KEY (`PAPER_ID`) REFERENCES `paper` (`PAPER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `quest_paper_ibfk_3` FOREIGN KEY (`QUEST_ID`) REFERENCES `question` (`QUEST_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quest_text`
--

DROP TABLE IF EXISTS `quest_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_text` (
  `QUEST_ID` bigint(20) unsigned NOT NULL,
  `LANG_ID` char(6) CHARACTER SET utf8 NOT NULL,
  `QUESTION_TEXT` text NOT NULL,
  PRIMARY KEY (`QUEST_ID`,`LANG_ID`),
  KEY `QUEST_ID` (`QUEST_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `quest_text_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `language` (`LANG_ID`),
  CONSTRAINT `quest_text_ibfk_3` FOREIGN KEY (`QUEST_ID`) REFERENCES `question` (`QUEST_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quest_topic`
--

DROP TABLE IF EXISTS `quest_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_topic` (
  `QUEST_ID` bigint(20) unsigned NOT NULL,
  `TOPIC_ID` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`QUEST_ID`,`TOPIC_ID`),
  KEY `TOPIC_ID` (`TOPIC_ID`),
  CONSTRAINT `quest_topic_ibfk_2` FOREIGN KEY (`TOPIC_ID`) REFERENCES `topic` (`TOPIC_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `quest_topic_ibfk_3` FOREIGN KEY (`QUEST_ID`) REFERENCES `question` (`QUEST_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quest_type`
--

DROP TABLE IF EXISTS `quest_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_type` (
  `QTYPE_ID` char(3) CHARACTER SET utf8 NOT NULL,
  `QTYPE_DESC` varchar(35) NOT NULL,
  `MULT_ANS` tinyint(1) NOT NULL,
  PRIMARY KEY (`QTYPE_ID`),
  UNIQUE KEY `DESCRIPTION` (`QTYPE_DESC`,`MULT_ANS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `QUEST_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CREATED` datetime DEFAULT CURRENT_TIMESTAMP,
  `QTYPE_ID` char(3) CHARACTER SET utf8 NOT NULL,
  `REPO_ID` char(25) CHARACTER SET utf8 NOT NULL,
  `READY` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`QUEST_ID`),
  KEY `QTYPE_ID` (`QTYPE_ID`),
  KEY `REPO_ID` (`REPO_ID`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`QTYPE_ID`) REFERENCES `quest_type` (`QTYPE_ID`) ON UPDATE CASCADE,
  CONSTRAINT `question_ibfk_2` FOREIGN KEY (`REPO_ID`) REFERENCES `repo` (`REPO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repo`
--

DROP TABLE IF EXISTS `repo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repo` (
  `REPO_ID` char(25) CHARACTER SET utf8 NOT NULL,
  `REPO_NAME` varchar(20) NOT NULL,
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `REPO_ICON` char(3) DEFAULT NULL,
  PRIMARY KEY (`REPO_ID`),
  UNIQUE KEY `REPO_NAME` (`REPO_NAME`,`USER_ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `repo_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `lecturer` (`USER_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `RESPONSE_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `RESPONSE_TEXT` text NOT NULL,
  PRIMARY KEY (`RESPONSE_ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `response_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `privacc` (`USER_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats` (
  `STAT_ID` char(3) DEFAULT NULL,
  `STAT_NAME` varchar(20) NOT NULL,
  `STAT_DESC` varchar(255) DEFAULT NULL,
  UNIQUE KEY `STAT_NAME` (`STAT_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `STUDENT_NO` char(9) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic` (
  `TOPIC_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TOPIC_TITLE` varchar(35) NOT NULL,
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `TTYPE_ID` char(2) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`TOPIC_ID`),
  UNIQUE KEY `TOPIC_TITLE` (`TOPIC_TITLE`),
  UNIQUE KEY `TOPIC_TITLE_2` (`TOPIC_TITLE`),
  KEY `USER_ID` (`USER_ID`),
  KEY `TTYPE_ID` (`TTYPE_ID`),
  CONSTRAINT `topic_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `lecturer` (`USER_ID`) ON UPDATE CASCADE,
  CONSTRAINT `topic_ibfk_2` FOREIGN KEY (`TTYPE_ID`) REFERENCES `topic_type` (`TTYPE_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `topic_type`
--

DROP TABLE IF EXISTS `topic_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_type` (
  `TTYPE_ID` char(2) CHARACTER SET utf8 NOT NULL,
  `TTYPE_DESC` varchar(20) NOT NULL,
  PRIMARY KEY (`TTYPE_ID`),
  UNIQUE KEY `DESCRIPTION` (`TTYPE_DESC`),
  UNIQUE KEY `TTYPE_DESC` (`TTYPE_DESC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `USERNAME` varchar(20) NOT NULL,
  `USERMAIL` varchar(45) NOT NULL,
  `HASH` text,
  `FIRST_NAME` varchar(20) DEFAULT NULL,
  `LAST_NAME` varchar(20) DEFAULT NULL,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  `USERAVATAR` char(10) DEFAULT NULL,
  `USERPHONE` char(10) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `USERNAME` (`USERNAME`),
  UNIQUE KEY `USERMAIL` (`USERMAIL`),
  UNIQUE KEY `USERPHONE` (`USERPHONE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `USER_ID` char(13) CHARACTER SET utf8 NOT NULL,
  `GROUP_ID` char(17) CHARACTER SET utf8 NOT NULL,
  `ADMIN` tinyint(1) NOT NULL DEFAULT '0',
  `ADDED` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`USER_ID`,`GROUP_ID`),
  KEY `GROUP_ID` (`GROUP_ID`),
  CONSTRAINT `user_group_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_group_ibfk_2` FOREIGN KEY (`GROUP_ID`) REFERENCES `grouping` (`GROUP_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'testbank'
--

--
-- Dumping routines for database 'testbank'
--
/*!50003 DROP FUNCTION IF EXISTS `add_figure` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `add_figure`(label VARCHAR(20),fdesc VARCHAR(255),ftype VARCHAR(20),rid CHAR(25),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE REPO_ID = rid AND USER_ID = client);		
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;

	IF TRIM(label) = '' THEN RETURN 'figure label required';END IF;
	INSERT INTO figure(FIGURE_LABEL,DESCRIPTION,FIGURE_TYPE,REPO_ID) VALUES (label,fdesc,ftype,rid);
	RETURN LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `add_group` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `add_group`(name CHAR(20),gdesc VARCHAR(255),access BOOLEAN,uid CHAR(13)) RETURNS char(17) CHARSET latin1
BEGIN
	DECLARE ID CHAR(17);
	INSERT INTO grouping(GROUP_NAME,DESCRIPTION,PUBLIC) VALUES(name,'iding',access);
	SELECT GROUP_ID FROM grouping WHERE DESCRIPTION = 'iding' INTO ID; 
	UPDATE grouping SET DESCRIPTION = gdesc WHERE DESCRIPTION = 'iding';
	
	INSERT INTO user_group(USER_ID,GROUP_ID,ADMIN) VALUES(uid,ID,TRUE);
	RETURN ID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `add_g_member` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `add_g_member`(name CHAR(20),gid CHAR(17),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE ID CHAR(13) DEFAULT (SELECT USER_ID FROM user WHERE USERNAME = name);					
	DECLARE ADMIN BOOLEAN DEFAULT (SELECT ADMIN FROM user_group WHERE GROUP_ID = gid && USER_ID = client);		
	DECLARE MEMBER BOOLEAN DEFAULT (SELECT client IN (SELECT USER_ID FROM user_group WHERE GROUP_ID = gid));	

	IF NOT MEMBER THEN RETURN 'you arn\'t part of group';	END IF;	
	IF NOT ADMIN THEN RETURN 'you\'re not admin'; END IF;
	IF ID IS \N THEN RETURN 'user not found'; END IF;
	IF (SELECT COUNT(*) FROM user_group WHERE USER_ID = ID && GROUP_ID = gid) = 1 THEN RETURN 'already a member'; END IF;
	
	INSERT INTO user_group(USER_ID,GROUP_ID) VALUES(ID,gid);
	RETURN CONCAT(ID,':',gid);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `add_g_repo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `add_g_repo`(gid CHAR(17),rid CHAR(25),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE MEMBER BOOLEAN DEFAULT (SELECT client IN (SELECT USER_ID FROM user_group WHERE GROUP_ID = gid));	
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE REPO_ID = rid AND USER_ID = client);		

	IF NOT MEMBER THEN RETURN 'you arn\'t part of group';	END IF;
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	
	INSERT INTO group_repo(GROUP_ID,REPO_ID) VALUES(gid,rid);
	RETURN CONCAT(gid,':',rid);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `add_quest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `add_quest`(rid CHAR(25),qtype CHAR(3),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE REPO_ID = rid AND USER_ID = client);		
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;

	INSERT INTO question(REPO_ID,QTYPE_ID) VALUES(rid,qtype);
	RETURN LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `add_quest_ansr` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `add_quest_ansr`(qid BIGINT UNSIGNED,atext TEXT,sound BOOLEAN,misnomer VARCHAR(255),client CHAR(13)) RETURNS varchar(75) CHARSET latin1
BEGIN
	DECLARE qtype CHAR(3) DEFAULT (SELECT QTYPE_ID FROM question WHERE QUEST_ID = qid);			
	DECLARE found BOOLEAN;
	DECLARE OWNER BOOLEAN;

	SELECT ((SELECT USER_ID FROM repo WHERE REPO_ID = (SELECT REPO_ID FROM question WHERE QUEST_ID = qid)) = client) INTO OWNER;
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;

	IF LENGTH(TRIM(atext)) = 0 THEN RETURN 'text required';	END IF;		





	CASE qtype
	WHEN 'T1S' THEN
	BEGIN
		IF UPPER(atext) <> 'T' && UPPER(atext) <> 'F' THEN
			RETURN 'True/False Questions only accept \'T\' or \'F\' as answers';
		END IF;
		SELECT (SELECT COUNT(ANS_ID) FROM answer WHERE QUEST_ID = qid) > 0 INTO found;	
		IF found THEN	DELETE FROM answer WHERE QUEST_ID = qid;	END IF;

		CASE atext
		WHEN 'T' THEN
		BEGIN
			INSERT INTO answer VALUES(\N,TRUE,qid,'T',\N);
			INSERT INTO answer VALUES(\N,FALSE,qid,'F',misnomer);
		END;
		WHEN 'F' THEN
		BEGIN
			INSERT INTO answer VALUES(\N,TRUE,qid,'F',\N);
			INSERT INTO answer VALUES(\N,FALSE,qid,'T',misnomer);
		END;
		END CASE;

		UPDATE question SET READY = TRUE WHERE QUEST_ID = qid;
	END;
	WHEN 'M1M' THEN
	BEGIN
		IF sound && LENGTH(TRIM(misnomer)) > 0 THEN					
		RETURN 'misconceptions apply only on incorrect answers';
		END IF;	
		INSERT INTO answer VALUES(\N,sound,qid,atext,misnomer);
		IF ((SELECT COUNT(*) FROM answer WHERE QUEST_ID = qid AND CORRECT) > 0 && (SELECT COUNT(*) FROM answer WHERE QUEST_ID = qid AND NOT CORRECT) > 0) THEN			
			UPDATE question SET READY = TRUE WHERE QUEST_ID = qid;
		ELSE
			UPDATE question SET READY = FALSE WHERE QUEST_ID = qid;
		END IF;
	END;
	END CASE;

	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `add_quest_text` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `add_quest_text`(qid BIGINT UNSIGNED,lid CHAR(6),qtext TEXT,client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE OWNER BOOLEAN;

	SELECT ((SELECT USER_ID FROM repo WHERE REPO_ID = (SELECT REPO_ID FROM question WHERE QUEST_ID = qid)) = client) INTO OWNER;
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;

	IF LENGTH(TRIM(qtext)) = 0 THEN RETURN 'text required';	END IF;

	INSERT INTO quest_text VALUES(qid,lid,qtext);
	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `assign_quest_figure` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `assign_quest_figure`(qid BIGINT UNSIGNED,fid BIGINT UNSIGNED,action CHAR(2),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE rid CHAR(25) DEFAULT (SELECT REPO_ID FROM question WHERE QUEST_ID = qid);
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE REPO_ID = rid AND USER_ID = client);		
	DECLARE records TINYINT UNSIGNED DEFAULT (SELECT COUNT(*) FROM quest_figure WHERE QUEST_ID = qid);
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	
	CASE action
	WHEN 'rm' THEN
	BEGIN
		DELETE FROM quest_figure WHERE QUEST_ID = qid AND FIGURE_ID = fid;
		RETURN 'ereased';
	END;
	WHEN 'mk' THEN
	BEGIN	
		IF records > 0 THEN 	DELETE FROM quest_figure WHERE QUEST_ID = qid;	END IF;
		INSERT INTO quest_figure VALUES (qid,fid);
		RETURN 'assigned';
	END;
	END CASE;
	RETURN 'action not understood';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `mv_figure` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `mv_figure`(label VARCHAR(20),fdesc VARCHAR(255),ftype VARCHAR(20),fid BIGINT UNSIGNED,client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE rid CHAR(25) DEFAULT (SELECT REPO_ID FROM figure WHERE FIGURE_ID = fid);
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE REPO_ID = rid AND USER_ID = client);		
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	
	IF TRIM(label) = '' THEN RETURN 'figure label required';END IF;
	UPDATE figure SET FIGURE_LABEL = label, DESCRIPTION = fdesc, FIGURE_TYPE = ftype WHERE FIGURE_ID = fid;
	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `mv_g_member` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `mv_g_member`(uid CHAR(13),gid CHAR(17),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE FOUND BOOLEAN DEFAULT (SELECT COUNT(*) FROM user_group WHERE GROUP_ID = gid && USER_ID = uid);		
	DECLARE ADMINS SMALLINT UNSIGNED DEFAULT (SELECT COUNT(*) FROM user_group WHERE GROUP_ID = gid && ADMIN);	
	DECLARE UROLE BOOLEAN DEFAULT (SELECT ADMIN FROM user_group WHERE GROUP_ID = gid && USER_ID = uid);		
	DECLARE ADMIN BOOLEAN DEFAULT (SELECT ADMIN FROM user_group WHERE GROUP_ID = gid && USER_ID = client);		
	DECLARE MEMBER BOOLEAN DEFAULT (SELECT client IN (SELECT USER_ID FROM user_group WHERE GROUP_ID = gid));	

	DECLARE NEWADMIN BOOLEAN DEFAULT (NOT UROLE);									

	IF NOT MEMBER THEN RETURN 'you arn\'t part of group';	END IF;	
	IF NOT ADMIN THEN RETURN 'you\'re not admin';	END IF;
	IF NOT FOUND THEN RETURN 'not found';	END IF;
	IF NOT NEWADMIN && ADMINS = 1 THEN RETURN 'group can\'t lack admin'; END IF;
	
	UPDATE user_group SET ADMIN = NEWADMIN WHERE USER_ID = uid AND GROUP_ID = gid;
	RETURN NEWADMIN;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `mv_repo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `mv_repo`(label VARCHAR(20),rdesc VARCHAR(255),rid CHAR(25),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE USER_ID = client);		
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	
	IF TRIM(label) = '' THEN RETURN 'repo name required';END IF;
	UPDATE repo SET REPO_NAME = label, DESCRIPTION = rdesc WHERE REPO_ID = rid;
	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `repo_icon` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `repo_icon`(icon CHAR(3),rid CHAR(25),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE USER_ID = client);		
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	
	UPDATE repo SET REPO_ICON = icon WHERE REPO_ID = rid;
	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `rm_figure` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `rm_figure`(fid BIGINT UNSIGNED,client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE rid CHAR(25) DEFAULT (SELECT REPO_ID FROM figure WHERE FIGURE_ID = fid);
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE REPO_ID = rid AND USER_ID = client);		
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	DELETE FROM figure WHERE FIGURE_ID = fid;
	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `rm_g_member` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `rm_g_member`(uid CHAR(13),gid CHAR(17),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE FOUND BOOLEAN DEFAULT (SELECT COUNT(*) FROM user_group WHERE GROUP_ID = gid && USER_ID = uid);		
	DECLARE GMEMBERS SMALLINT UNSIGNED DEFAULT (SELECT COUNT(*) FROM user_group WHERE GROUP_ID = gid);		
	DECLARE ADMINS SMALLINT UNSIGNED DEFAULT (SELECT COUNT(*) FROM user_group WHERE GROUP_ID = gid && ADMIN);	
	DECLARE UROLE BOOLEAN DEFAULT (SELECT ADMIN FROM user_group WHERE GROUP_ID = gid && USER_ID = uid);		
	DECLARE ADMIN BOOLEAN DEFAULT (SELECT ADMIN FROM user_group WHERE GROUP_ID = gid && USER_ID = client);		
	DECLARE MEMBER BOOLEAN DEFAULT (SELECT client IN (SELECT USER_ID FROM user_group WHERE GROUP_ID = gid));	
	DECLARE SELF BOOLEAN DEFAULT (SELECT uid = client);								

	IF NOT FOUND THEN RETURN 'not found';	END IF;
	IF NOT MEMBER THEN RETURN 'you arn\'t part of group';	END IF;
	IF NOT SELF && NOT ADMIN THEN RETURN 'you\'re not admin';	END IF;
	
	IF GMEMBERS = 1 THEN DELETE FROM grouping WHERE GROUP_ID = gid; RETURN 'done'; END IF;
	IF UROLE && ADMINS = 1 THEN RETURN 'group can\'t lack admin';	END IF;
	
	DELETE FROM user_group WHERE USER_ID = uid AND GROUP_ID = gid;
	CREATE TEMPORARY TABLE t AS SELECT gr.REPO_ID FROM group_repo gr, repo r WHERE r.REPO_ID = gr.REPO_ID && USER_ID = uid;	
	DELETE FROM group_repo WHERE REPO_ID IN (SELECT REPO_ID FROM t);
	IF uid = client THEN RETURN 'done'; END IF;
	RETURN 'gone';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `rm_g_repo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `rm_g_repo`(rid CHAR(25),gid CHAR(17),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE FOUND BOOLEAN DEFAULT (SELECT COUNT(*) FROM group_repo WHERE GROUP_ID = gid && REPO_ID = rid);		
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE REPO_ID = rid AND USER_ID = client);		
	DECLARE ADMIN BOOLEAN DEFAULT (SELECT ADMIN FROM user_group WHERE GROUP_ID = gid && USER_ID = client);		
	DECLARE MEMBER BOOLEAN DEFAULT (SELECT client IN (SELECT USER_ID FROM user_group WHERE GROUP_ID = gid));	
	DECLARE NAME VARCHAR(20) DEFAULT (SELECT REPO_NAME FROM repo WHERE REPO_ID = rid);

	IF NOT MEMBER THEN RETURN 'you arn\'t part of group';	END IF;
	IF NOT FOUND THEN RETURN 'not found';	END IF;
	IF NOT OWNER && NOT ADMIN THEN RETURN 'you don\'t own repo';	END IF;
	
	DELETE FROM group_repo WHERE REPO_ID = rid AND GROUP_ID = gid;
	IF (SELECT COUNT(*) FROM group_repo WHERE GROUP_ID = gid) = 0 THEN RETURN CONCAT(NAME,'|empty'); END IF;
	RETURN CONCAT(NAME,'|gone');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `rm_quest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `rm_quest`(qid BIGINT UNSIGNED,client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE rid CHAR(25) DEFAULT (SELECT REPO_ID FROM question WHERE QUEST_ID = qid);
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE REPO_ID = rid AND USER_ID = client);		
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	DELETE FROM question WHERE QUEST_ID = qid;
	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `rm_quest_cmp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `rm_quest_cmp`(id TEXT,subject CHAR(4),lang CHAR(2),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE qid BIGINT UNSIGNED;
	DECLARE OWNER BOOLEAN;

	CASE subject
	WHEN 'text' THEN
	BEGIN
	 SELECT ((SELECT USER_ID FROM repo WHERE REPO_ID = (SELECT REPO_ID FROM question WHERE QUEST_ID = id)) = client) INTO OWNER;		
	 IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	 DELETE FROM quest_text WHERE QUEST_ID = id AND LANG_ID = lang;	
	END;
	WHEN 'ansr' THEN
	BEGIN
	 SELECT QUEST_ID FROM answer WHERE ANS_ID = id INTO qid;
	 SELECT ((SELECT USER_ID FROM repo WHERE REPO_ID = (SELECT REPO_ID FROM question WHERE QUEST_ID = qid)) = client) INTO OWNER;		
	 IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	 DELETE FROM answer WHERE ANS_ID = id;
	 IF ((SELECT COUNT(*) FROM answer WHERE QUEST_ID = qid AND CORRECT) = 0 || (SELECT COUNT(*) FROM answer WHERE QUEST_ID = qid AND NOT CORRECT) = 0) THEN			
			UPDATE question SET READY = FALSE WHERE QUEST_ID = qid;
	 END IF;
	END;	
	END CASE;
	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `rm_repo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `rm_repo`(rid CHAR(25),client CHAR(13)) RETURNS varchar(45) CHARSET latin1
BEGIN
	DECLARE OWNER BOOLEAN DEFAULT (SELECT COUNT(*) FROM repo WHERE USER_ID = client);		
	IF NOT OWNER THEN RETURN 'you don\'t own repo';	END IF;
	DELETE FROM repo WHERE REPO_ID = rid;
	RETURN 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `select_quest_text` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `select_quest_text`(qid BIGINT UNSIGNED,lid CHAR(6)) RETURNS text CHARSET latin1
BEGIN
	DECLARE qtext TEXT;
	DECLARE found BOOLEAN;

	CASE 
	WHEN LENGTH(TRIM(lid)) = 0 OR LENGTH(TRIM(lid)) IS \N  THEN
	BEGIN
		SELECT (SELECT COUNT(LANG_ID) FROM quest_text WHERE LANG_ID = 'EN' AND QUEST_ID = qid) > 0 INTO found;
		IF found THEN 
			SET qtext = (SELECT QUESTION_TEXT FROM quest_text WHERE QUEST_ID = qid AND LANG_ID = 'EN');
		ELSE
			SET qtext = (SELECT QUESTION_TEXT FROM quest_text WHERE QUEST_ID = qid LIMIT 1);
		END IF;
	END;
	ELSE
		SET qtext = (SELECT QUESTION_TEXT FROM quest_text WHERE QUEST_ID = qid AND LANG_ID = lid);
	END CASE;

	RETURN qtext;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_focus_hits` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` FUNCTION `sf_focus_hits`(qid BIGINT UNSIGNED,action ENUM('V','D'),a DATE) RETURNS bigint(20) unsigned
BEGIN
	DECLARE b DATE DEFAULT(DATE_ADD(a,INTERVAL 1 DAY));

	RETURN (SELECT l.HITS FROM (SELECT QUEST_ID ID, COUNT(*) HITS FROM focus_quest fq WHERE ADVENT BETWEEN a AND  b AND Q_ACTION = action AND QUEST_ID  GROUP BY QUEST_ID) l WHERE l.ID = qid);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_purge_acc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_purge_acc`(uname VARCHAR(20)) RETURNS text CHARSET latin1
BEGIN
	DECLARE FOUND BOOLEAN;
	SELECT ((SELECT COUNT(USER_ID) FROM user WHERE USERNAME = uname) > 0) INTO FOUND;

	IF FOUND THEN
		SELECT ((SELECT ACTIVE FROM user WHERE USERNAME = uname) = -1) INTO FOUND;
		IF FOUND THEN 

			RETURN 'success';
		ELSE
			RETURN 'account active';
		END IF;
	ELSE
		RETURN 'username not found';
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `sf_signin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `sf_signin`(uname VARCHAR(20)) RETURNS text CHARSET latin1
BEGIN
	DECLARE state TINYINT;
	IF TRIM(uname) = '' THEN RETURN '-3;uname empty'; END IF;
	IF (SELECT ((SELECT COUNT(USER_ID) FROM user WHERE USERNAME = uname) = 0)) THEN
		RETURN '-2;username not found';
	END IF;
	CASE (SELECT ACTIVE FROM user WHERE USERNAME = uname)
		WHEN	-1	THEN
		BEGIN
			RETURN CONCAT('-1;activation email sent to : ',(SELECT USERMAIL FROM user WHERE USERNAME = uname));
		END;
		WHEN	0	THEN
		BEGIN
			RETURN '0;account deactivated';
		END;
		WHEN	1	THEN
		BEGIN
			
		END;
	END CASE;

	IF ((SELECT HASH FROM user WHERE USERNAME = uname) IS \N) THEN
		RETURN '2;NO PASSWORD';
	END IF;
	RETURN '1;success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_repo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` PROCEDURE `sp_add_repo`(IN name VARCHAR(20), IN userid CHAR(13))
BEGIN
	DECLARE CHECKPOINT  TINYINT UNSIGNED DEFAULT 0;
	DECLARE ID CHAR(25);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN 
		GET DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
		ROLLBACK;
        	SELECT CONCAT('C',LPAD(CHECKPOINT,2,'0'))  AS 'SQL EXCEPTION',@p1 AS CODE, @p2 AS DESCRIPTION;
	END;
	DECLARE CONTINUE HANDLER FOR 1452
	BEGIN
		INSERT INTO lecturer(USER_ID) VALUES(userid);
		INSERT INTO repo(REPO_NAME,USER_ID) VALUES('iding',userid);
	END;
	START TRANSACTION;
		INSERT INTO repo(REPO_NAME,USER_ID) VALUES('iding',userid);
		SELECT REPO_ID FROM repo WHERE REPO_NAME = 'iding' INTO ID;
		UPDATE repo SET REPO_NAME = name WHERE REPO_ID = ID;
	COMMIT;
	SELECT r.*, USERNAME FROM repo r, user u WHERE r.USER_ID = u.USER_ID AND r.REPO_ID = ID;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_user`(IN uname VARCHAR(20), IN umail VARCHAR(45))
BEGIN
	DECLARE CHECKPOINT  TINYINT UNSIGNED DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN 
		GET DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
		ROLLBACK;
        	SELECT CONCAT('C',LPAD(CHECKPOINT,2,'0'))  AS 'SQL EXCEPTION',@p1 AS CODE, @p2 AS DESCRIPTION;
	END;
	START TRANSACTION;
	
	SET uname = LOWER(REPLACE(uname,' ',''));
	SET umail = LOWER(REPLACE(umail,' ',''));
	
	IF uname = '' THEN	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'username cannot be left blank';	END IF;	SET CHECKPOINT = 1;
	IF umail = '' THEN	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'email cannot be left blank';	END IF;	SET CHECKPOINT = 2;
	
 	INSERT INTO user(USERNAME,USERMAIL,ACTIVE) VALUES(uname,umail,-1);	SET CHECKPOINT = 3;
 	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_assign_lang` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_assign_lang`(IN lang CHAR(30),IN code CHAR(3),IN content BIGINT UNSIGNED)
BEGIN
	DECLARE FOUND BOOLEAN DEFAULT FALSE;
	DECLARE LANG CHAR(6);
	DECLARE CHECKPOINT  TINYINT UNSIGNED DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN 
		GET DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
		ROLLBACK;
        	SELECT CONCAT('C',LPAD(CHECKPOINT,2,'0'))  AS 'POINT',@p1 AS CODE, @p2 AS DESCRIPTION;
	END;
	START TRANSACTION;
	SET lang = trim(lang);


	SET FOUND = ((SELECT COUNT(LANG_ID) FROM language WHERE DESCRIPTION = lang && COUNTRY_CODE = code) > 0);
	IF NOT FOUND THEN	INSERT INTO language VALUES(\N,lang,code);	END IF;			SET CHECKPOINT = 1;
	

	SET LANG = (SELECT LANG_ID FROM language WHERE DESCRIPTION = lang && COUNTRY_CODE = code);
	UPDATE text SET LANG_ID = LANG WHERE TEXT_ID = content;

 	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_focus_hits` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sanhedrin`@`%` PROCEDURE `sp_focus_hits`(IN action ENUM('V','D'),IN a DATE)
BEGIN
	DECLARE b DATE DEFAULT(DATE_ADD(a,INTERVAL 1 DAY));
	
	SELECT QUEST_ID ID, COUNT(*) HITS FROM focus_quest WHERE ADVENT BETWEEN a AND b AND Q_ACTION = action AND QUEST_ID GROUP BY QUEST_ID;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `testbank`
--

USE `testbank`;

--
-- Final view structure for view `GROUP_MEMBERS`
--

/*!50001 DROP VIEW IF EXISTS `GROUP_MEMBERS`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`sanhedrin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `GROUP_MEMBERS` AS select `u`.`USER_ID` AS `USER_ID`,`u`.`USERNAME` AS `USERNAME`,`u`.`USERMAIL` AS `USERMAIL`,`u`.`FIRST_NAME` AS `FIRST_NAME`,`u`.`LAST_NAME` AS `LAST_NAME`,`u`.`USERAVATAR` AS `USERAVATAR`,`ug`.`GROUP_ID` AS `GROUP_ID`,`g`.`GROUP_NAME` AS `GROUP_NAME`,`g`.`GROUP_BANNER` AS `GROUP_BANNER`,`ug`.`ADMIN` AS `ADMIN` from ((`user` `u` join `user_group` `ug`) join `grouping` `g`) where ((`u`.`USER_ID` = `ug`.`USER_ID`) and (`g`.`GROUP_ID` = `ug`.`GROUP_ID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `GROUP_REPOS`
--

/*!50001 DROP VIEW IF EXISTS `GROUP_REPOS`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`sanhedrin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `GROUP_REPOS` AS select `r`.`REPO_ID` AS `REPO_ID`,`r`.`REPO_NAME` AS `REPO_NAME`,`r`.`USER_ID` AS `USER_ID`,`r`.`DESCRIPTION` AS `DESCRIPTION`,`r`.`REPO_ICON` AS `REPO_ICON`,(select `u`.`USERNAME` from `user` `u` where (`u`.`USER_ID` = `r`.`USER_ID`)) AS `USERNAME`,`g`.`GROUP_ID` AS `GROUP_ID`,`g`.`GROUP_NAME` AS `GROUP_NAME`,`g`.`GROUP_BANNER` AS `GROUP_BANNER` from ((`repo` `r` join `grouping` `g`) join `group_repo` `gr`) where ((`r`.`REPO_ID` = `gr`.`REPO_ID`) and (`gr`.`GROUP_ID` = `g`.`GROUP_ID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `QUEST_LIST`
--

/*!50001 DROP VIEW IF EXISTS `QUEST_LIST`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`sanhedrin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `QUEST_LIST` AS select `q`.`QUEST_ID` AS `ID`,`q`.`REPO_ID` AS `REPO`,`q`.`CREATED` AS `CREATED`,`q`.`READY` AS `READY`,`qt`.`QTYPE_DESC` AS `QTYPE_DESC`,`select_quest_text`(`q`.`QUEST_ID`,NULL) AS `QUESTION`,`u`.`USERNAME` AS `USERNAME` from (((`question` `q` join `quest_type` `qt`) join `repo` `r`) join `user` `u`) where ((`q`.`QTYPE_ID` = `qt`.`QTYPE_ID`) and (`q`.`REPO_ID` = `r`.`REPO_ID`) and (`r`.`USER_ID` = `u`.`USER_ID`)) order by `q`.`READY` desc,`q`.`CREATED` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `REPO_LIST`
--

/*!50001 DROP VIEW IF EXISTS `REPO_LIST`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`sanhedrin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `REPO_LIST` AS select `r`.`REPO_ID` AS `REPO_ID`,`r`.`REPO_NAME` AS `REPO_NAME`,`r`.`USER_ID` AS `USER_ID`,`r`.`DESCRIPTION` AS `DESCRIPTION`,`r`.`REPO_ICON` AS `REPO_ICON`,count(0) AS `QUESTS`,`u`.`USERNAME` AS `USERNAME` from ((`repo` `r` join `user` `u`) join `question` `q`) where ((`r`.`USER_ID` = `u`.`USER_ID`) and (`q`.`REPO_ID` = `r`.`REPO_ID`) and (`q`.`READY` = TRUE)) group by `r`.`REPO_ID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-01 15:51:13
