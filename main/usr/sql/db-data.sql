-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: testbank
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES ('A11030',0,103,'Mathew','Subject confuses tax collectors for thieves'),('A11031',1,103,'Judas Is\'caliot',NULL),('A11050',0,105,'Lion','Subject watched too much Disney'),('A11051',1,105,'Cyrus',NULL),('A11070',0,107,'T','subject confuses kings'),('A11071',1,107,'F',NULL),('A11080',0,108,'Right Ventricle','subjects confuses question text with right lung'),('A11081',1,108,'Aorta',NULL),('A11110',0,111,'T','subject takes language out of context'),('A11111',1,111,'F',NULL),('A11130',0,113,'Love','subject mistakes life for a fairy tale'),('A11131',1,113,'War',NULL),('A11140',0,114,'F','subject doesn\'t understand the concept of investment'),('A11141',1,114,'T',NULL),('A11150',0,115,'T','subject is dreaming'),('A11151',1,115,'F',NULL),('A11160',0,116,'F','subject underestimates the power of volts'),('A11161',1,116,'T',NULL),('A11170',0,117,'raining','subject looks at this from a water perspective'),('A11171',1,117,'Lighting',NULL),('A11180',0,118,'T','subject is color blind'),('A11181',1,118,'F',NULL),('A11190',0,119,'0 - 30',NULL),('A11191',1,119,'50 - 65',NULL),('A11220',0,122,'F','subject confuses housing for a luxury'),('A11221',1,122,'T',NULL),('A11230',0,123,'F','subject isn\'t conscious of the expense pollution have to the environment'),('A11231',1,123,'T',NULL),('A11240',0,124,'T','subject is mislead by the veins'),('A11241',1,124,'F',NULL),('A11250',0,125,'2nd','subject confuses c   with the assembly languages'),('A11251',1,125,'3rd',NULL),('A21030',0,103,'James','Subject underestimates the bond of family'),('A21050',0,105,'Nebudkenezza','Subject confuses kings'),('A21080',0,108,'Left Atrium','subject confuses question context with left lung'),('A21130',0,113,'Boredom','subject is on the right track, but there\'s more to it than that.'),('A21170',0,117,'Windy','subject thinks of the power lines'),('A21190',0,119,'31 - 49',NULL),('A21250',0,125,'4th','the misconception is that these languages go as deep as c  '),('A31050',0,105,'Hezekiah','subjects insn\'t paying attention to timeline'),('A31080',0,108,'Pulmounary Valve','subject is completely clueless'),('A31190',0,119,'more than 60',NULL);
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `associate`
--

LOCK TABLES `associate` WRITE;
/*!40000 ALTER TABLE `associate` DISABLE KEYS */;
/*!40000 ALTER TABLE `associate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `figure`
--

LOCK TABLES `figure` WRITE;
/*!40000 ALTER TABLE `figure` DISABLE KEYS */;
INSERT INTO `figure` VALUES (12,'retnal physics','the study of the eye','inside out','181005053627U218LE00001SN'),(16,'energy','think of power sources','diagram','181001112425U218LE00001SN'),(18,'builing',NULL,'blueprint','181001112425U218LE00001SN'),(21,'cat skeleton',NULL,'diagram','181005053627U218LE00001SN'),(22,'computer networks',NULL,'diagram','181001112425U218LE00001SN'),(24,'fall of babylon','Persian King, Cyrus, leads a military campaign to capture great city, Babylon','illustration','181012121901U218LE00001SN'),(25,'the christ birthday','history records suggest it was summer when the Christ was born','illustration','181012121901U218LE00001SN'),(26,'Daniel with lions',NULL,'illustration','181012121901U218LE00001SN'),(27,'Ethopian Munnach',NULL,'illustration','181012121901U218LE00001SN'),(28,'heart',NULL,'diagram','181005053627U218LE00001SN'),(29,'UML Class',NULL,'diagram','180622045511U218FL00001BU'),(30,'GCC','Code Snippent','illustration','180622045519U218FL00001BU'),(31,'Responsive Design',NULL,'illustration','180622045519U218FL00001BU'),(32,'Computer Network',NULL,'diagram','180622045909U218SN00001PE'),(33,'Modern Computing',NULL,'illustration','180622045909U218SN00001PE'),(34,'Stairs',NULL,'diagram','180622045934U218SN00001PE'),(35,'Top view',NULL,'blueprint','180622045934U218SN00001PE'),(36,'City',NULL,'photograph','180622045934U218SN00001PE'),(37,'the city',NULL,'photograph','181023124007U218FL00001BU');
/*!40000 ALTER TABLE `figure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `focus_quest`
--

LOCK TABLES `focus_quest` WRITE;
/*!40000 ALTER TABLE `focus_quest` DISABLE KEYS */;
INSERT INTO `focus_quest` VALUES ('U218FL00001BU',103,'2018-10-26 00:19:42','D'),('U218FL00001BU',103,'2018-10-26 00:21:22','D'),('U218FL00001BU',103,'2018-10-28 10:30:09','V'),('U218FL00001BU',103,'2018-10-30 17:24:02','V'),('U218FL00001BU',105,'2018-10-19 03:37:25','D'),('U218FL00001BU',105,'2018-10-23 10:30:14','V'),('U218FL00001BU',105,'2018-10-26 00:19:35','V'),('U218FL00001BU',105,'2018-10-30 17:23:58','D'),('U218FL00001BU',107,'2018-10-19 03:37:07','V'),('U218FL00001BU',107,'2018-10-26 00:20:42','D'),('U218FL00001BU',111,'2018-10-23 10:31:17','V'),('U218FL00001BU',111,'2018-11-01 12:40:01','V'),('U218FL00001BU',113,'2018-10-16 10:31:54','D'),('U218FL00001BU',113,'2018-10-23 10:31:34','V'),('U218FL00001BU',113,'2018-10-26 00:15:47','D'),('U218FL00001BU',113,'2018-10-28 19:11:53','V'),('U218FL00001BU',113,'2018-10-28 19:12:56','V'),('U218FL00001BU',113,'2018-10-30 17:24:58','V'),('U218FL00001BU',113,'2018-11-01 12:39:38','V'),('U218FL00001BU',113,'2018-11-01 12:39:42','D'),('U218FL00001BU',114,'2018-10-28 19:12:10','D'),('U218FL00001BU',114,'2018-10-28 19:12:20','V'),('U218FL00001BU',114,'2018-10-30 17:24:11','V'),('U218FL00001BU',114,'2018-11-01 12:40:20','D'),('U218FL00001BU',115,'2018-10-28 19:12:30','D'),('U218FL00001BU',115,'2018-10-28 19:12:36','D'),('U218FL00001BU',115,'2018-10-28 19:12:41','D'),('U218FL00001BU',115,'2018-10-30 17:24:18','D'),('U218FL00001BU',115,'2018-11-01 12:40:29','V'),('U218FL00001BU',118,'2018-10-23 12:49:23','V'),('U218FL00001BU',119,'2018-10-15 12:49:49','D'),('U218FL00001BU',125,'2018-10-28 18:59:15','V'),('U218FL00001BU',125,'2018-11-01 12:40:05','D'),('U218LE00001SN',103,'2018-10-23 14:57:53','D'),('U218LE00001SN',103,'2018-10-23 23:04:51','V'),('U218LE00001SN',103,'2018-10-23 23:09:15','V'),('U218LE00001SN',103,'2018-10-24 04:18:09','V'),('U218LE00001SN',103,'2018-10-24 04:18:39','V'),('U218LE00001SN',103,'2018-10-24 21:40:12','V'),('U218LE00001SN',103,'2018-10-24 21:47:32','V'),('U218LE00001SN',105,'2018-10-23 17:20:53','V'),('U218LE00001SN',105,'2018-10-23 18:15:59','V'),('U218LE00001SN',105,'2018-10-23 19:09:54','V'),('U218LE00001SN',105,'2018-10-23 22:49:04','V'),('U218LE00001SN',105,'2018-10-23 22:50:22','V'),('U218LE00001SN',105,'2018-10-24 21:40:45','V'),('U218LE00001SN',105,'2018-10-24 21:42:36','V'),('U218LE00001SN',105,'2018-10-24 21:44:23','V'),('U218LE00001SN',107,'2018-10-24 04:16:26','V'),('U218LE00001SN',107,'2018-10-24 04:18:16','V'),('U218LE00001SN',108,'2018-10-20 14:07:42','V'),('U218LE00001SN',108,'2018-10-23 11:09:29','D'),('U218LE00001SN',108,'2018-10-23 14:08:04','D'),('U218LE00001SN',108,'2018-10-23 14:55:26','V'),('U218LE00001SN',108,'2018-10-23 17:16:23','V'),('U218LE00001SN',108,'2018-10-23 23:05:55','V'),('U218LE00001SN',108,'2018-10-23 23:09:38','V'),('U218LE00001SN',108,'2018-10-24 04:08:11','V'),('U218LE00001SN',108,'2018-10-24 04:10:33','V'),('U218LE00001SN',108,'2018-10-24 04:16:38','V'),('U218LE00001SN',108,'2018-10-24 10:25:53','D'),('U218LE00001SN',108,'2018-10-24 21:36:31','V'),('U218LE00001SN',111,'2018-10-18 10:55:56','D'),('U218LE00001SN',111,'2018-10-23 11:09:13','V'),('U218LE00001SN',111,'2018-10-29 08:40:55','V'),('U218LE00001SN',114,'2018-10-21 10:51:59','V'),('U218LE00001SN',114,'2018-10-23 10:51:54','D'),('U218LE00001SN',114,'2018-10-23 11:09:10','V'),('U218LE00001SN',114,'2018-10-23 11:09:16','V'),('U218LE00001SN',114,'2018-10-27 16:01:30','V'),('U218LE00001SN',114,'2018-10-27 16:01:36','D'),('U218LE00001SN',115,'2018-10-16 10:55:50','D'),('U218LE00001SN',115,'2018-10-20 14:19:05','V'),('U218LE00001SN',115,'2018-10-23 10:52:08','D'),('U218LE00001SN',115,'2018-10-23 11:09:08','V'),('U218LE00001SN',115,'2018-10-23 11:09:18','V'),('U218LE00001SN',115,'2018-10-23 14:19:19','D'),('U218LE00001SN',115,'2018-10-27 16:00:34','V'),('U218LE00001SN',115,'2018-10-27 16:00:45','D'),('U218LE00001SN',115,'2018-10-27 23:39:01','D'),('U218LE00001SN',116,'2018-10-23 10:56:14','V'),('U218LE00001SN',116,'2018-10-23 11:08:27','V'),('U218LE00001SN',116,'2018-10-23 11:08:33','V'),('U218LE00001SN',116,'2018-10-23 11:08:38','D'),('U218LE00001SN',116,'2018-10-23 11:09:42','V'),('U218LE00001SN',116,'2018-10-23 11:09:47','D'),('U218LE00001SN',116,'2018-10-29 08:40:11','V'),('U218LE00001SN',117,'2018-10-20 11:08:35','V'),('U218LE00001SN',117,'2018-10-21 10:56:12','V'),('U218LE00001SN',117,'2018-10-23 11:09:45','V'),('U218LE00001SN',117,'2018-10-24 10:56:18','D'),('U218LE00001SN',117,'2018-10-29 08:40:33','D'),('U218LE00001SN',122,'2018-10-30 13:04:34','V'),('U218LE00001SN',122,'2018-10-30 16:33:34','V'),('U218LE00001SN',122,'2018-11-01 09:39:02','V'),('U218LE00001SN',123,'2018-10-30 13:01:10','V'),('U218LE00001SN',124,'2018-10-27 16:10:03','V'),('U218SN00001PE',103,'2018-10-21 10:43:13','D'),('U218SN00001PE',103,'2018-10-30 17:12:29','D'),('U218SN00001PE',105,'2018-10-19 10:43:27','V'),('U218SN00001PE',105,'2018-10-21 10:43:19','D'),('U218SN00001PE',105,'2018-10-25 20:35:59','V'),('U218SN00001PE',105,'2018-10-30 17:12:22','V'),('U218SN00001PE',105,'2018-11-01 12:11:47','V'),('U218SN00001PE',107,'2018-10-23 10:43:24','V'),('U218SN00001PE',107,'2018-11-01 12:11:50','D'),('U218SN00001PE',108,'2018-10-30 17:20:44','D'),('U218SN00001PE',108,'2018-10-30 17:22:09','V'),('U218SN00001PE',108,'2018-11-01 12:11:05','D'),('U218SN00001PE',122,'2018-10-25 20:36:10','V'),('U218SN00001PE',123,'2018-11-01 12:11:27','V'),('U218SN00001PE',123,'2018-11-01 12:11:36','D'),('U218SN00001PE',124,'2018-10-30 17:20:41','V'),('U218SN00001PE',124,'2018-11-01 12:11:15','V');
/*!40000 ALTER TABLE `focus_quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `group_repo`
--

LOCK TABLES `group_repo` WRITE;
/*!40000 ALTER TABLE `group_repo` DISABLE KEYS */;
INSERT INTO `group_repo` VALUES ('zaf01180920024839','180622045511U218FL00001BU','2018-10-22 23:56:47'),('zaf01180920024839','180622045519U218FL00001BU','2018-10-22 23:58:07'),('zaf01180920024839','180622045535U218FL00001BU','2018-10-23 12:50:56'),('zaf01180920024839','180622045934U218SN00001PE','2018-09-26 20:04:18'),('zaf01180920024839','181012121901U218LE00001SN','2018-10-22 20:19:43'),('zaf01180920040941','181005053627U218LE00001SN','2018-10-25 20:37:06'),('zaf01180920060532','180622050018U218SN00001PE','2018-09-27 03:28:24'),('zaf01180920060532','180624051215U218SN00001PE','2018-09-27 03:28:57'),('zaf01180920060532','180918022011U218SN00001PE','2018-09-27 03:07:58'),('zaf01180920060532','181001112425U218LE00001SN','2018-10-22 22:19:04'),('zaf01180920060532','181012121901U218LE00001SN','2018-10-22 22:19:12'),('zaf01180922061219','180622045934U218SN00001PE','2018-09-27 03:36:06'),('zaf01180925123429','180622045535U218FL00001BU','2018-10-22 23:57:25'),('zaf01180925123429','180622045909U218SN00001PE','2018-10-22 21:52:29'),('zaf01180925123429','180624051215U218SN00001PE','2018-10-22 21:52:33'),('zaf01180925123429','180925110334U218FL00001BU','2018-10-22 23:57:20'),('zaf01180927123733','180622045511U218FL00001BU','2018-10-22 23:57:57'),('zaf01180927123733','180622045535U218FL00001BU','2018-10-22 23:57:52');
/*!40000 ALTER TABLE `group_repo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `grouping`
--

LOCK TABLES `grouping` WRITE;
/*!40000 ALTER TABLE `grouping` DISABLE KEYS */;
INSERT INTO `grouping` VALUES ('zaf01180920024839','serious men',NULL,'png',1),('zaf01180920040941','legion','It is because we\'re many',NULL,0),('zaf01180920060532','elite','we pride ourselves as acting in the best interest of mankind','png',1),('zaf01180922061219','the free','we openly share ideas',NULL,1),('zaf01180925123429','glorious men','we\'re awesome','png',0),('zaf01180927123733','the profane','we suck!',NULL,1);
/*!40000 ALTER TABLE `grouping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES ('DE','Danish'),('NL','Dutch'),('EN','English');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `lecturer`
--

LOCK TABLES `lecturer` WRITE;
/*!40000 ALTER TABLE `lecturer` DISABLE KEYS */;
INSERT INTO `lecturer` VALUES ('L218ME00001AB'),('U218FL00001BU'),('U218HA00001BU'),('U218LE00001SN'),('U218LF00001LY'),('U218SN00001PE');
/*!40000 ALTER TABLE `lecturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `paper`
--

LOCK TABLES `paper` WRITE;
/*!40000 ALTER TABLE `paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `privacc`
--

LOCK TABLES `privacc` WRITE;
/*!40000 ALTER TABLE `privacc` DISABLE KEYS */;
/*!40000 ALTER TABLE `privacc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `query`
--

LOCK TABLES `query` WRITE;
/*!40000 ALTER TABLE `query` DISABLE KEYS */;
INSERT INTO `query` VALUES (1,NULL,NULL,'s215013395@mmhgga.ac.za','services','What morte canyouu offer?','2018-10-23 12:57:22');
/*!40000 ALTER TABLE `query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `query_response`
--

LOCK TABLES `query_response` WRITE;
/*!40000 ALTER TABLE `query_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `query_response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quest_figure`
--

LOCK TABLES `quest_figure` WRITE;
/*!40000 ALTER TABLE `quest_figure` DISABLE KEYS */;
INSERT INTO `quest_figure` VALUES (122,18),(105,24),(107,25),(108,28),(111,30),(113,32),(115,36),(119,37);
/*!40000 ALTER TABLE `quest_figure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quest_paper`
--

LOCK TABLES `quest_paper` WRITE;
/*!40000 ALTER TABLE `quest_paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quest_text`
--

LOCK TABLES `quest_text` WRITE;
/*!40000 ALTER TABLE `quest_text` DISABLE KEYS */;
INSERT INTO `quest_text` VALUES (103,'DE','Wolk ense val kraam de kristo?'),(103,'EN','Who betrayed the Christ?'),(105,'EN','What\'s the name of the King that captured Babylon?'),(107,'DE','Var krito el var sikur'),(107,'EN','The Christ was born in summer.'),(108,'EN','Which part of the heart connects directly to the body?'),(108,'NL','Wat betir van herte konse die kaar mr?'),(110,'EN','What color are one\'s ribs?'),(111,'DE','C   kiper lungo gtza'),(111,'EN','C   is a scripting anguage.'),(113,'DE','Vuure vul osdery yui kompt er?'),(113,'EN','What most likely lead to the invention of computers?'),(114,'EN','Property value appreciates'),(114,'NL','Die veer van die huis baire'),(115,'EN','City life is designed for country people.'),(116,'EN','Electricity is fatal '),(117,'EN','Which weather is not entertaining to electricity'),(118,'EN','The sky is blue'),(119,'EN','What mark mark will you get for the project?'),(119,'NL','What is blyk onse?'),(122,'EN','Housing is one of the 3 basic needs'),(123,'EN','Burning of fossil fuels sucks.'),(124,'DE','Voonsgat tregto shuk'),(124,'EN','Blood colour is purple'),(125,'EN','What generation of programming languages is the c   language?');
/*!40000 ALTER TABLE `quest_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quest_topic`
--

LOCK TABLES `quest_topic` WRITE;
/*!40000 ALTER TABLE `quest_topic` DISABLE KEYS */;
INSERT INTO `quest_topic` VALUES (123,1),(124,1),(103,2),(107,2),(124,2),(108,4),(122,5),(124,5);
/*!40000 ALTER TABLE `quest_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quest_type`
--

LOCK TABLES `quest_type` WRITE;
/*!40000 ALTER TABLE `quest_type` DISABLE KEYS */;
INSERT INTO `quest_type` VALUES ('C1S','column match',0),('M1M','multichoice',1),('T1S','true/false',0);
/*!40000 ALTER TABLE `quest_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (103,'2018-10-20 12:36:33','M1M','181012121901U218LE00001SN',1),(104,'2018-10-20 16:37:45','M1M','181012121901U218LE00001SN',0),(105,'2018-10-20 16:49:38','M1M','181012121901U218LE00001SN',1),(107,'2018-10-20 23:01:23','T1S','181012121901U218LE00001SN',1),(108,'2018-10-21 22:46:21','M1M','181005053627U218LE00001SN',1),(110,'2018-10-22 00:26:33','M1M','181005053627U218LE00001SN',0),(111,'2018-10-23 01:25:23','T1S','180622045519U218FL00001BU',1),(112,'2018-10-23 03:02:28','M1M','180622045519U218FL00001BU',0),(113,'2018-10-23 06:09:04','M1M','180622045909U218SN00001PE',1),(114,'2018-10-23 06:21:54','T1S','180622045934U218SN00001PE',1),(115,'2018-10-23 10:37:10','T1S','180622045934U218SN00001PE',1),(116,'2018-10-23 10:44:01','T1S','180622050018U218SN00001PE',1),(117,'2018-10-23 10:46:56','M1M','180622050018U218SN00001PE',1),(118,'2018-10-23 12:41:59','T1S','181023124007U218FL00001BU',1),(119,'2018-10-23 12:45:39','M1M','181023124007U218FL00001BU',1),(120,'2018-10-24 09:51:03','T1S','181005053627U218LE00001SN',0),(121,'2018-10-24 09:56:55','M1M','181005053627U218LE00001SN',0),(122,'2018-10-24 22:16:28','T1S','181001112425U218LE00001SN',1),(123,'2018-10-24 22:28:30','T1S','181001112425U218LE00001SN',1),(124,'2018-10-27 16:04:09','T1S','181005053627U218LE00001SN',1),(125,'2018-10-28 18:52:21','M1M','180622045519U218FL00001BU',1);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `repo`
--

LOCK TABLES `repo` WRITE;
/*!40000 ALTER TABLE `repo` DISABLE KEYS */;
INSERT INTO `repo` VALUES ('180622045511U218FL00001BU','Java Programing','U218FL00001BU','Java is a general-purpose computer-programming language that is concurrent, class-based, object-oriented, and specifically designed to have as few implementation dependencies as possible','png'),('180622045519U218FL00001BU','c++','U218FL00001BU','This language adds the concept of abstraction to the C language','png'),('180622045535U218FL00001BU','python','U218FL00001BU',NULL,NULL),('180622045909U218SN00001PE','computers','U218SN00001PE',NULL,'png'),('180622045934U218SN00001PE','properties','U218SN00001PE','personal/commercial,\\xd\\xafor rental/for sale','png'),('180622050018U218SN00001PE','electricity','U218SN00001PE','energy sources and power',NULL),('180624051215U218SN00001PE','mother nature','U218SN00001PE','psalms 37:29','png'),('180625103956U218SN00001PE','food','U218SN00001PE','one of the three basic needs','png'),('180918022011U218SN00001PE','Jets','U218SN00001PE',NULL,NULL),('180925110334U218FL00001BU','php','U218FL00001BU','server-side scripting since 1995','png'),('181001112425U218LE00001SN','engineering','U218LE00001SN','the science of living','png'),('181005053627U218LE00001SN','biology','U218LE00001SN','Biology is the natural science that studies life and living organisms, including their physical structure, chemical processes, molecular interactions, physiological mechanisms, development and evolution','png'),('181012121901U218LE00001SN','history','U218LE00001SN','the study of the universe',NULL),('181023124007U218FL00001BU','project management','U218FL00001BU',NULL,NULL),('181101034647U218HA00001BU','sict 3220','U218HA00001BU',NULL,NULL);
/*!40000 ALTER TABLE `repo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `stats`
--

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;
INSERT INTO `stats` VALUES ('PC0','popularity contest','presents question popularity amongst users, by view and downloads'),('RI0','repo intensity','Measures the weight of repo by content');
/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` VALUES (1,'Database Bible','U218LE00001SN','bk'),(2,'Philosophy','U218LE00001SN','dc'),(4,'Thinking Fast and Slow','U218LE00001SN','bk'),(5,'AppScan','U218LE00001SN','mc');
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `topic_type`
--

LOCK TABLES `topic_type` WRITE;
/*!40000 ALTER TABLE `topic_type` DISABLE KEYS */;
INSERT INTO `topic_type` VALUES ('bk','book'),('bc','book chapter'),('dc','discipline'),('md','module'),('mc','module charpter');
/*!40000 ALTER TABLE `topic_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('U218EV00001CR','evidence','cruzzie.e@gmail.com',NULL,NULL,NULL,-1,NULL,NULL),('U218FL00001BU','flames','s215013395@mandela.ac.za','SDFHek44Y2w0aTBja095R2x6MExXQT09Ojo5zXjLZowJBu36dGyOc0w9','Jonga','Cephus',1,'png',NULL),('U218HA00001BU','hailmary','business52@live.com','Z0R5WFQzZ1cvemdaR3dmMWdFTEZwZz09Ojr6lU8y0fp9XfZodJjYKJdL','John','Doe',1,NULL,NULL),('U218LE00001SN','lee','snakebite@live.co.za','NWswN3MyeExmUDdUd1cxaFkvYVBkQT09Ojo+PrqNaY7HvnUrYnNWrUaP','Jin','Kazama',1,NULL,NULL),('U218LF00001LY','lfutcher','lynn.futcher@gmail.com','KzdLQjFwS1I0eWhWYWNTejl2VDB0UT09Ojqneh00B9V9s7qKvFM5DC10','Lynn','Futcher',1,NULL,NULL),('U218LY00001LY','lynn','lynn.futcher@mandela.ac.za',NULL,NULL,NULL,-1,NULL,NULL),('U218ME00001CH','mentation','chumagantsho@gmail.com','UmoxbGNpYVBBTVV6Q081M3ZrWnY4Zz09OjqdNaqLQ8rQdQbxpYEgDCoh','Chuma','Martin',1,NULL,'0615544465'),('U218SN00001PE','snakebite','peculiar@exclusivemail.co.za','Y2pqbEJrdmVQUUtKQTFxTkcxZXdtdz09OjqaFIHG8PhsXXsEAmoASi+3',NULL,NULL,1,NULL,'0734513142');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES ('U218FL00001BU','zaf01180920024839',0,'2018-09-26 20:22:54'),('U218FL00001BU','zaf01180925123429',1,'2018-09-25 12:34:29'),('U218FL00001BU','zaf01180927123733',1,'2018-09-27 12:37:33'),('U218LE00001SN','zaf01180920024839',1,'2018-09-26 20:22:45'),('U218LE00001SN','zaf01180920040941',1,'2018-09-25 00:15:36'),('U218LE00001SN','zaf01180920060532',1,'2018-09-24 23:26:44'),('U218LE00001SN','zaf01180927123733',0,'2018-10-26 00:16:35'),('U218SN00001PE','zaf01180920040941',0,'2018-10-25 20:37:27'),('U218SN00001PE','zaf01180920060532',0,'2018-09-24 12:23:32'),('U218SN00001PE','zaf01180922061219',1,'2018-09-24 21:01:04'),('U218SN00001PE','zaf01180925123429',0,'2018-10-22 21:50:47');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-01 15:51:19
