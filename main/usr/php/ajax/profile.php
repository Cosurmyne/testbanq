<?php

require_once dirname(__FILE__, 4) . '/epiqworx/logic/sample.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/handler.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/reuse.php';
require_once dirname(__FILE__, 4) . '/model.php';
require_once dirname(__FILE__, 5) . '/work/model.php';
require_once dirname(__FILE__, 5) . '/profile/model.php';

$passwd_length = 8;
$err = array();

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'test';
    }
}
switch ($action) {
    case 'check':
        $relation = filter_input(INPUT_GET, 'relation');    //  --------------- database table to query
        $target = filter_input(INPUT_GET, 'target');    //  ------------------- target attribute to query
        $value = trim(filter_input(INPUT_GET, 'value'));    //  --------------  value check of selected attribute
        echo dbAccess::record_exists($relation, $target, $value, true);
        break;
    case 'count':
        $target = filter_input(INPUT_GET, 'target');
        switch ($target) {
            case 'group-members':
                $gid = filter_input(INPUT_GET, 'gid');
                echo Group::count('user_group',$gid, true);
                break;
            case 'group-repo':
                $gid = filter_input(INPUT_GET, 'gid');
                echo Group::count('group_repo',$gid, true);
                break;
        }
        break;
    case 'signup':
        require_once dirname(__FILE__,4).'/epiqworx/logic/validation.php';
        $uname = strtolower(trim(filter_input(INPUT_POST, 'uname')));
        $umail = strtolower(trim(filter_input(INPUT_POST, 'umail')));
        Account::add($uname,$umail,true);
        $userdir = dirname(__FILE__, 3) . "/home/$uname";   //  --------------- user's home directory
        $token = md5(rand(0, 1000));
        if (File::dump_token($userdir, $token)) {
            File::dump_key($userdir, 32);   //  ------------------------------- password encryption key
            $id = Account::get_id($uname);
            mkdir("$userdir/notifications");
        //------------------------------------- attempt to send activation email
            require_once dirname(__FILE__, 2).'/mailing.php';
            $message = Mailing::read_file('activate-account.php', $id, $umail, $token);
            // --------------------- test if file containig email was read correctly
            if (strlen($message) < 4) {
                dbAccess::delete('user', 'USERNAME', $uname, true);
                File::rmdir_r($userdir);
                echo "$$$message";
                break;
            }
            $mailsent = Mailing::send_mail('Activate New Account', $message, $umail);
            if ($mailsent !== "0") {
                dbAccess::delete('user', 'USERNAME', $uname, true);
                File::rmdir_r($userdir);
                echo "$mailsent$$-4";
                break;
            }
            echo "$$$mailsent";
        } else {
            dbAccess::delete('user', 'USERNAME', $uname, true);
            File::rmdir_r($userdir);
            echo "$$-3";
        }
        break;
    case 'signin':
        $uname = strtolower(trim(filter_input(INPUT_POST, 'uname')));
        $passwd = filter_input(INPUT_POST, 'passwd');
        if (empty($uname)) {break;}
        $userdir = dirname(__FILE__, 3) . "/home/$uname";   //  ----------- user's home directory
        if (!is_dir($userdir)) {echo "-5;<b>user file</b> missing!";break;} //- check if user's home can be found
        if(!file_exists("$userdir/key")){echo "-6;crypto key missing!";break;}//check if file containing user password encryption key can be located

        $key = File::read_token("$userdir/key");    //  ----------------------- get encrytion key from file
        $tate = Account::signin($uname,$passwd,$key,true);
        if (intval(explode(';', $tate)[0]) > 0) {  //  --------------------------- this means that account is worthy of a sign in
            
            $id = Account::get_id($uname);
            $data = dbAccess::get_data('user', 'USER_ID', $id);
            $_SESSION['id'] = $data['USER_ID'];
            $_SESSION['umail'] = $data['USERMAIL'];
            $_SESSION['uname'] = $data['USERNAME'];
        }
        echo "$tate";
        break;
    case 'one':
        echo json_encode(dbAccess::get_data('user', 'USER_ID', $_SESSION['id']));
        break;
    case 'one-update':
        require_once dirname(__FILE__,4).'/epiqworx/logic/validation.php';
        $data = dbAccess::get_data('user', 'USER_ID', $_SESSION['id']);
        
        $uname = strtolower(trim(filter_input(INPUT_POST, 'uname')));
        $umail = strtolower(trim(filter_input(INPUT_POST, 'umail')));
        $uphone = trim(filter_input(INPUT_POST, 'uphone'));
        $fname = trim(filter_input(INPUT_POST, 'fname'));
        $lname = trim(filter_input(INPUT_POST, 'lname'));
        
        if(!Validate::uname($uname,$msg,true,2)){ array_push($err,"username error   : $msg"); unset($msg);}
        if(!Validate::umail($umail,$msg,true)){array_push($err,"email error     : $msg"); unset($msg);array_push($err,$msg); unset($msg);}
        if(!Validate::phone($uphone,$msg)){array_push($err,"phone error      : $msg"); unset($msg);}
        if(!Validate::pname($fname,$msg)){array_push($err,"first name error : $msg"); unset($msg);}
        if(!Validate::pname($lname,$msg)){array_push($err,"last name error  : $msg"); unset($msg);}
        
        if(count($err)>0){
            $_SESSION['error'] = $err;
            echo "validation error$$-1";
            break;
        } else {
            if ($data['USERNAME'] !== $uname) {
                $oldname = dirname(__FILE__, 3) . "/home/" . $_SESSION['uname'];
                $newname = dirname(__FILE__, 3) . "/home/$uname";
                if (!rename($oldname, $newname)) {
                    echo 'couldn\'t rename your home directory$$-2';
                    break;
                }
            }
            $update = '' . Account::update($uname, $uphone, $fname, $lname, $_SESSION['id'], true);
            if (strlen($update) === 0) {
                $_SESSION['uname'] = $uname;
                $_SESSION['umail'] = $umail;
            } else {
                echo "$update$$-3";
                break;
            }
            if ($umail !== $data['USERMAIL']) {
                require_once dirname(__FILE__, 2) . '/mailing.php';
                $userdir = dirname(__FILE__, 3) . "/home/$uname";   //  --------------- user's home directory
                $token = md5(rand(0, 1000));
                if (File::dump_token($userdir, $token)) {
                    $message = Mailing::read_file('reset-email.php', $_SESSION['id'], $umail, $token);
                    // --------------------- test if file containig email was read correctly
                    if (strlen($message) < 4) {
                        dbAccess::delete('user', 'USERNAME', $uname, true);
                        File::rmdir_r($userdir);
                        echo "$$$message";
                        break;
                    }
                    $mailsent = Mailing::send_mail('Email Address Update', $message, $umail);
                    if($mailsent === '0'){
                        echo "$$$mailsent";
                    }else{
                        unlink("$userdir/token.tmp");
                        echo "$mailsent$$-7";
                    }
                    break;
                }else{
                    echo "coudn't write file$$-5";
                    break;
                }
            }
            echo "$$0";
        }
        break;
    case 'groups':
        echo json_encode(Group::ls($_SESSION['id'], TRUE));
        break;
    case 'group-add':
        $name = filter_input(INPUT_POST, 'group-name');
        $desc = Filter::escape(filter_input(INPUT_POST, 'group-desc'));
        $access = filter_input(INPUT_POST, 'group-access');
        $img = filter_input(INPUT_POST, 'group-icon');
        $page = filter_input(INPUT_POST, 'page');
        $id = Group::add($name, $desc, $access,$_SESSION['id'], TRUE);
        define('GROUP_ICON',dirname(__FILE__, 4)."/usr/img/user/group/$id.png");
        if(!empty($img)){
            list($type, $img) = explode(';', $img);
            list(, $img) = explode(',', $img);
            $img = base64_decode($img);
            if(!is_dir($img_dir = dirname(__FILE__, 4).'/usr/img/user/group')){mkdir($img_dir);}
            if(!is_dir($img_dir)){header("location:../../../..?action=error&msg=couldn't write on server document root");}
            if(file_exists(GROUP_ICON)){unlink(GROUP_ICON);}
            file_put_contents(GROUP_ICON, $img);
            $file = explode('/',GROUP_ICON)[count(explode('/',GROUP_ICON))-1];
            Group::set_icon($id, explode('.', $file)[1]);
        }
        header("location:../../../../profile?$page");
        break;
    case 'group-update':
        $id = filter_input(INPUT_POST, 'group-id');
        $name = filter_input(INPUT_POST, 'group-name');
        $desc = Filter::escape(filter_input(INPUT_POST, 'group-desc'));
        $public = filter_input(INPUT_POST, 'group-access');
        $img = filter_input(INPUT_POST, 'group-icon');
        $page = filter_input(INPUT_POST, 'page');
        Group::update($name, $desc, $public, $id);
        define('GROUP_ICON',dirname(__FILE__, 4)."/usr/img/user/group/$id.png");
        if(!empty($img)){
            list($type, $img) = explode(';', $img);
            list(, $img) = explode(',', $img);
            $img = base64_decode($img);
            if(!is_dir($img_dir = dirname(__FILE__, 4).'/usr/img/user/group')){mkdir($img_dir);}
            if(!is_dir($img_dir)){header("location:../../../..?action=error&msg=couldn't write on server document root");}
            if(file_exists(GROUP_ICON)){unlink(GROUP_ICON);}
            file_put_contents(GROUP_ICON, $img);
            $file = explode('/',GROUP_ICON)[count(explode('/',GROUP_ICON))-1];
            Group::set_icon($id, explode('.', $file)[1]);
        }
        header("location:../../../../profile?$page");
        break;
    case 'group-rm-icon':
        $id = filter_input(INPUT_GET, 'id');
        define('GROUP_ICON',dirname(__FILE__, 4)."/usr/img/user/group/$id.png");
        if(file_exists(GROUP_ICON)){unlink(GROUP_ICON);}
        dbAccess::clear_field('grouping', array('GROUP_BANNER'), null, 'GROUP_ID', $id, true);
        echo 'success';
        break;
    case 'group-select':
        $gid = filter_input(INPUT_GET, 'gid');
        $admin = Group::is_admin($gid, $_SESSION['id'], TRUE);
        $members = Group::count('user_group',$gid, true);
        $repos = Group::count('group_repo',$gid, true);
        $memb_time = $repo_time = $memb_name = $repo_name = '';
        $memb_new = Group::latest_member($gid, true);
        $repo_new = Group::latest_repo($gid, true);
        
        if(!empty($memb_new)){$memb_time = $memb_new['ADDED'];$memb_name = $memb_new['USER'];}
        if(!empty($repo_new)){$repo_time = $repo_new['ADDED'];$repo_name = $repo_new['REPO'];}
        $group = array('MEMBERS'=>$members,'REPOS'=>$repos,'ID'=>$gid,'ADMIN'=>$admin,'MEMB_TIME'=>$memb_time,'REPO_TIME'=>$repo_time,'MEMB_NAME'=>$memb_name,'REPO_NAME'=>$repo_name);
        
        echo json_encode($group);
        break;
    case 'group-members':
        $gid = filter_input(INPUT_GET, 'gid');
        echo json_encode(Group::ls_member($gid, TRUE));
        break;
    case 'group-add-member':
        $user = filter_input(INPUT_POST, 'uname');
        $gid = filter_input(INPUT_POST, 'gid');
        $state = Group::add_member($user, $gid,$_SESSION['id'],TRUE);
        if(strpos($state, ':') !== false) {
            echo json_encode(Group::get_member($gid, Account::get_id($user), true));
        }else{
            echo "$state";
        }
        break;
    case 'group-mv-member':
        $user = filter_input(INPUT_POST, 'uid');
        $gid = filter_input(INPUT_POST, 'gid');
        echo Group::mv_member($user, $gid, $_SESSION['id'], TRUE);
        break;
    case 'group-rm-member':
        $user = filter_input(INPUT_POST, 'uid');
        $gid = filter_input(INPUT_POST, 'gid');
        echo Group::rm_member($user, $gid, $_SESSION['id'], TRUE);
        break;
    case 'repo-select':
        $gid = filter_input(INPUT_GET, 'gid');
        echo json_encode(Group::repo_select($_SESSION['id'], $gid, TRUE));
        break;
    case 'group-repos':
        $gid = filter_input(INPUT_GET, 'gid');
        echo json_encode(Group::ls_repo($gid, TRUE));
        break;
    case 'group-add-repo':
        $rid = filter_input(INPUT_POST, 'rid');
        $gid = filter_input(INPUT_POST, 'gid');
        $state = Group::add_repo($gid, $rid, $_SESSION['id'], TRUE);
        if(strpos($state, ':') !== false) {
            echo json_encode(Group::get_repo($rid,$gid, true));
        }else{
            echo "$state";
        }
        break;
    case 'group-rm-repo':
        $rid = filter_input(INPUT_POST, 'rid');
        $gid = filter_input(INPUT_POST, 'gid');
        echo Group::rm_repo($rid, $gid, $_SESSION['id'], TRUE);
        break;
    case 'group-leave':
        $gid = filter_input(INPUT_GET, 'gid');
        define('GROUP_ICON',dirname(__FILE__, 4)."/usr/img/user/group/$gid.png");
        if(file_exists(GROUP_ICON)){unlink(GROUP_ICON);}
        echo Group::leave($gid, TRUE);
        break;
    case 'pass-update':
        require_once dirname(__FILE__,4).'/epiqworx/logic/validation.php';
        $pass0 = filter_input(INPUT_POST, 'pass0');
        $pass1 = filter_input(INPUT_POST, 'pass1');
        $pass2 = filter_input(INPUT_POST, 'pass2');
        $uname = $_SESSION['uname'];
        $hasspass = !empty(Account::get_password($uname));
        $key = File::read_token(dirname(__FILE__, 3) . "/home/$uname/key");
        
        //---------------------- check if user is password protected
        if($hasspass && !Account::is_valid_login($uname,$pass0,$key,true))
        {
            $_SESSION['error'] = array('password error   : incorrect password');
            echo 'password error$$-1';
            break;
        }
        
        if($pass1 !== $pass2){
            echo "password don't match$$-2";
            break;
        }
        //---------------------- check if user is password protected
        if(!Validate::passwd($pass1,$msg)){
            $_SESSION['error'] = "password error   : $msg"; 
            unset($msg);
            echo 'validation error$$-3';
            break;
        }
        $update = '' .  Account::set_password($pass1,$_SESSION['id'],$key,true);
        if (strlen($update) === 0) {
            echo '$$0';
            } else {
                echo "$update$$-4";
                break;
            }
        break;
    case 'avatar':
        $img = $_POST['base64'];
        $page = $_POST['subject'];
        $id = $_SESSION['id'];
         define('USER_AVATAR',dirname(__FILE__, 4)."/usr/img/user/avatar/$id.png");
         if(!empty($img)){
            list($type, $img) = explode(';', $img);
            list(, $img) = explode(',', $img);
            $img = base64_decode($img);
            if(!is_dir($img_dir = dirname(__FILE__, 4).'/usr/img/user/avatar')){mkdir($img_dir);}
            if(!is_dir($img_dir)){header("location:../../../..?action=error&msg=couldn't write on server document root");}
            if(file_exists(USER_AVATAR)){unlink(INSTANCE_ICON);}
            file_put_contents(USER_AVATAR, $img);
            $file = explode('/',USER_AVATAR)[count(explode('/',USER_AVATAR))-1];
        }
        Account::set_icon($id, explode('.', $file)[1]);
        header("location:../../../../profile?$page");
        break;
    case 'rm-avatar':
        $uname = $_SESSION['uname'];
        $uid = $_SESSION['id'];
        dbAccess::clear_field('user', array('USERAVATAR'), NULL, 'USER_ID', $uid, TRUE);
        unlink(dirname(__FILE__, 4)."/usr/img/user/avatar/$uid.png");
        echo 'success';
        break;
    case 'activate':
        $user_id = filter_input(INPUT_GET, 'id');
        $hash = filter_input(INPUT_GET, 'hash');
        $uname = Account::get_uname($user_id);
        //  ------------------------------------------------------ VERIFY TOKEN
        if (file_exists($activation_file = dirname(__FILE__, 3) . "/home/$uname/token.tmp")) {
            $token = File::read_token($activation_file);
            if($hash === $token)
            {
                Account::flag(true,$user_id); //  -------------------------------- flag account as active
                unlink($activation_file);   //  -------------------------------- delete token file
                $data = dbAccess::get_data('user','USER_ID',$user_id);
                //  -------------------------------- LOAD SESSION VARIABLE DATA
                $_SESSION['id'] = $data['USER_ID'];
                $_SESSION['uname'] = $data['USERNAME'];
                $_SESSION['umail'] = $data['USERMAIL'];
                if (isset($_GET['lecturer'])) {
                    Account::promote('lecturer', $user_id);
                }
                if (isset($_GET['student'])) {
                    Account::promote('student', $user_id);
                }
                header("location: ../../../../");
            }header("location: ../../../..?action=error&msg=couldn't match activation token with hash on link");
        }header("location: ../../../..?action=error&msg=testbanq couldn't locate file containing activation token");
        break;
    case 'help_purge':
        $subject = trim(strtolower(filter_input(INPUT_POST,'subject')));
        $status = Account::purge($subject);
        if($status === 'success'){
            dbAccess::delete('user','USERNAME', $subject);
            File::rmdir_r(dirname(__FILE__, 3)."/home/$subject");
        }
        echo "$status";
        break;
    case 'help_reset':
        $subject = trim(filter_input(INPUT_POST,'subject'));
        $data = dbAccess::get_data('user', 'USERMAIL', $subject, TRUE);
        if (count($data) > 0) {
            if(intval($data['ACTIVE']) < 1){
                echo "account not active$$-8";
                break;
            }
                require_once dirname(__FILE__, 2) . '/mailing.php';
                $userdir = dirname(__FILE__, 3) . "/home/".$data['USERNAME'];   //  --------------- user's home directory
                $token = md5(rand(0, 1000));
                if (File::dump_token($userdir, $token)) {
                    $message = Mailing::read_file('reset-passwd.php', $data['USER_ID'], $data['USERMAIL'], $token);
                    // --------------------- test if file containig email was read correctly
                    if (strlen($message) < 4) {
                        echo "$$$message";
                        break;
                    }
                    $mailsent = Mailing::send_mail('Reset Login', $message, $data['USERMAIL']);
                    if($mailsent === '0'){
                        echo "$$$mailsent";
                    }else{
                        unlink("$userdir/token.tmp");
                        echo "$mailsent$$-7";
                    }
                    
                    break;
                }else{
                    echo "coudn't write file$$-5";
                    break;
                }
            }
            echo 'nothing found!$$-6';
        break;
    case 'reset':
        $user_id = filter_input(INPUT_GET, 'id');
        $hash = filter_input(INPUT_GET, 'hash');
        $uname = Account::get_uname($user_id);
        
        if (file_exists($key_file = dirname(__FILE__, 3) . "/home/$uname/token.tmp")) {
            $token = File::read_token($key_file);
            if($hash === $token)
            {
                $data = dbAccess::get_data('user','USER_ID',$user_id);
                //  -------------------------------- LOAD SESSION VARIABLE DATA
                $_SESSION['id'] = $data['USER_ID'];
                $_SESSION['uname'] = $data['USERNAME'];
                
                if (isset($_GET['email'])) {
                    $mail = filter_input(INPUT_GET, 'new');
                    Account::set_umail($mail, $user_id);
                    $_SESSION['umail'] = $mail;
                }
                if (isset($_GET['passwd'])) {
                    dbAccess::clear_field('user',array('HASH'),null,'USER_ID',$user_id);
                    $_SESSION['umail'] = $data['USERMAIL'];
                }
                unlink($key_file);
                header('location:../../../../profile');                echo 'updated!';             break;
            }header("location: ../../../..?action=error&msg=couldn't match activation token with hash on link");            break;
        }header("location: ../../../..?action=error&msg=testbanq couldn't locate file containing activation token");        break;
        break;
    case 'query':
        $subject = filter_input(INPUT_POST, 'target');
        
        $fname = filter_input(INPUT_POST, 'fname');
        $lname = filter_input(INPUT_POST, 'lname');
        $email = filter_input(INPUT_POST, 'email');
        $qtxt = filter_input(INPUT_POST, 'querytext');
        
        Generic::add_query($fname, $lname, $email, $subject, $qtxt);
        echo "success";
        break;
    case 'test':
        echo 'connected!';
        break;
    default :
        echo 'nothing';
        break;
}
