<?php

require_once dirname(__FILE__, 4) . '/epiqworx/logic/sample.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/handler.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/reuse.php';
require_once dirname(__FILE__, 4) . '/model.php';
require_once dirname(__FILE__, 5) . '/work/model.php';
require_once dirname(__FILE__, 5) . '/profile/model.php';

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'test';
    }
}
switch ($action){
    case 'count':
        $subject = filter_input(INPUT_GET, 'subject');
        $id = filter_input(INPUT_GET, 'id');
        switch($subject){
            case 'this':
                echo dbAccess::count('stats');
                break;
        }
        break;
    case 'present':
        $subject = filter_input(INPUT_GET, 'subject');
        switch ($subject){
            case 'popularity-contest':
                $a = filter_input(INPUT_GET, 'a');  //  point A (start date)
                $b = filter_input(INPUT_GET, 'b');  //  point B (end date)
                $target = filter_input(INPUT_GET, 'target');
                $repo = filter_input(INPUT_GET, 'repo');
                
                $quest = Quest::ls_ready($repo);
                $dates = Dates::range($a, $b);
                
                $data = array();
                
                if(!array_key_exists('ID',$quest)){ //  2D Array
                    foreach ($quest as $records) {
                        
                        $temp = array();
                        for($y = 0 ; $y < count($dates) ; $y++){
                            $hits = Item4::focus_hits($records['ID'],$target, $dates[$y]);
                            array_push($temp,array('DAY' => $dates[$y], 'HITS' => 0));
                            if (!empty($hits)) { $temp[$y]['HITS'] = $hits; }
                        }
                        array_push($data, array('QUEST'=>$records,'DATA'=>$temp));
                    }
                }else{  //  1D  Array
                    $temp = array();
                    for($y = 0 ; $y < count($dates) ; $y++){
                        $hits = Item4::focus_hits($quest['ID'],$target, $dates[$y]);
                        array_push($temp,array('DAY' => $dates[$y], 'HITS' => 0));
                        if(!empty($hits)){ $temp[$y]['HITS'] = $hits; }
                    }
                    array_push($data, array('QUEST'=>$quest,'DATA'=>$temp));
                }
                
//                $output = array('PERIOD'=>$dates,'FOCUS'=>$data);
                echo json_encode($data);
                break;
            case 'repo-intensity':
                $gid = filter_input(INPUT_GET, 'gid');
                $repo = Item1::ls_ready();
                if($gid!=='0'){
//                    $repo = 
                }
                echo $gid;
//                echo json_encode(Group::ls_repo($gid, TRUE));
                break;
        }
        break;
    case 'ls':
        $subject = filter_input(INPUT_GET, 'subject');
        $id = filter_input(INPUT_GET, 'id');
        switch ($subject){
            case 'this':
                echo json_encode(dbAccess::get_all('stats'));
                break;
            case 'quest':
                $quest = dbAccess::get_data('QUEST_LIST', 'USERNAME', $_SESSION['uname'], true);
                $repo = Item4::repo_quest();
//                echo json_encode(array('QUEST'=>$quest.'REPO'=>$repo));
                break;
            case 'popularity-contest':
                $groups = Group::ls($_SESSION['id']);   //  get groupd associated with user
                $repos = Item1::ls_ready(); //  get user repos with at east one question that's 'fit for duty'
                $output = array('GROUPS'=>$groups,'REPOS'=>$repos);
                echo json_encode($output);
                break;
        }
        break;
    case 'test':
        $hit0 = array('ID'=>'103','HITS'=>1);
        $hit1 = array(array('ID' => '103', 'HITS' => 1), array('ID' => '104', 'HITS' => 1));
//        echo Text::in_multi_dim($hit1, '103');
//        echo Text::key_in_multi_dim('ID', $hit1);
        if (array_key_exists("ID", $hit1)) {
            echo "Key exists!";
        } else {
            echo "Key does not exist!";
        }
        break;
    default :
        echo 'nothing';
        break;
}