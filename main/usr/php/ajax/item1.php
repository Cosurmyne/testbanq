<?php

require_once dirname(__FILE__, 4) . '/epiqworx/logic/sample.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/handler.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/reuse.php';
require_once dirname(__FILE__, 4) . '/model.php';
require_once dirname(__FILE__, 5) . '/work/model.php';
require_once dirname(__FILE__, 5) . '/profile/model.php';

$err = array();

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'test';
    }
}
switch ($action) {
    case 'count':
        $target = filter_input(INPUT_GET, 'target');
        $id = filter_input(INPUT_GET, 'id');
        switch ($target) {
            case 'this':
                echo dbAccess::count('repo','USER_ID',$_SESSION['id']);
                break;
            case 'databank':
                $figs =  dbAccess::count('figure','REPO_ID',$id);
                $quests = dbAccess::count('question','REPO_ID',$id);
                echo json_encode(array('FIGURES'=>$figs,'QUESTIONS'=>$quests));
                break;
            case 'quest':
                echo dbAccess::count('question','REPO_ID',$id);
                break;
            case 'figure':
                echo dbAccess::count('figure','REPO_ID',$id);
                break;
            case 'groups':
                echo dbAccess::count('user_group', 'USER_ID', $_SESSION['id']);
                break;
        }
        break;
    case 'ls':
        $target = filter_input(INPUT_GET, 'target');
        $id = filter_input(INPUT_GET, 'id');
        switch ($target){
            case 'this':
                echo json_encode(Item1::ls());
                break;
            case 'this-groups':
                $groups = Group::ls($_SESSION['id'], true);
                echo json_encode($groups);
                break;
            case 'this-quest':
                echo json_encode(Quest::ls($id, TRUE));
                break;
            case 'this-figs':
                echo json_encode(dbAccess::get_data('figure', 'REPO_ID', $id, TRUE));
                break;
            case 'index':
                $x1 = Quest::ls($id, TRUE);
                $x2 = Figure::ls($id);
                echo json_encode(array('QUEST' => $x1, 'FIG' => $x2));
                break;
            case 'fig-types':
                echo json_encode(Figure::ls_types(TRUE));
                break;
            case 'quest-types':
                echo json_encode(dbAccess::get_all('quest_type'));
                break;
            case 'quest-lang':
                echo json_encode(Lang::not_in_quest($id));
                break;
            case 'quest-txt':
                echo json_encode(Quest::ls_text($id));
                break;
            case 'quest-ansr':
                echo json_encode(Quest::ls_ansr($id));
                break;
            case 'quest-fig':
                echo json_encode(Quest::ls_fig($id));
                break;
            case 'quest-fig-short':
                $ftype = filter_input(INPUT_GET, 'type');
                if($ftype === '--'){
                    echo json_encode(dbAccess::get_data('figure', 'REPO_ID', $id, TRUE));
                    break;
                }
                echo json_encode(Figure::shortlist($id, $ftype));
                break;
        }
        break;
    case 'select':
        $target = filter_input(INPUT_GET, 'target');
        $id = filter_input(INPUT_GET, 'id');
        switch ($target){
            case 'this':
                echo json_encode(dbAccess::get_data('repo', 'REPO_ID', $id));
                break;
            case 'figure':
                echo json_encode(dbAccess::get_data('figure', 'FIGURE_ID', $id));
                break;
            case 'temp-figure':
                $img = dirname(__FILE__, 4)."/usr/img/figure/$id/demo.png";
                if(file_exists($img)){
                    echo "$img";
                    break;
                }
                echo 'no-file';
                break;
            case 'quest':
                require_once 'latex.php';
                $quest = dbAccess::get_data('QUEST_LIST', 'ID', $id, TRUE);
                $ansr = dbAccess::get_data('answer', 'QUEST_ID', $id, true);
                $fig = Quest::get_figures($id);
                $tex = Tex::get_quest($quest, $ansr, $fig);
                Quest::focus($id, filter_input(INPUT_GET, 'task'));
                echo json_encode(array('QUEST'=>$quest,'ANS'=>$ansr,'FIG'=>$fig,'TEX'=>$tex));
                break;
        }
        break;
    case 'create':
        require_once dirname(__FILE__, 4) . '/epiqworx/logic/validation.php';
        $target = filter_input(INPUT_POST, 'target');
        $id = filter_input(INPUT_POST, 'id');
        switch ($target) {
            case 'this':
                $name = filter_input(INPUT_POST, 'name');
                $repo = Item1::add($name,TRUE);
                if(is_array($repo)){                    echo json_encode($repo);break;}
                echo "0!!fail";
                break;
            case 'figure':
                $label = filter_input(INPUT_POST, 'label');
                $desc = Filter::escape(filter_input(INPUT_POST, 'desc'));
                $ftype = filter_input(INPUT_POST, 'type');
                define('FIGURE',dirname(__FILE__, 4)."/usr/img/figure/$id/demo.png");
                
                if(!file_exists(FIGURE)){echo json_encode(array('REPORT'=>'figure image file not found'));break;}
                $fid = Figure::add($label, $desc, $ftype, $id,$_SESSION['id']);
                if(Text::is_int($fid)){ $fid = intval($fid);}
                else{
                    unlink(FIGURE);
                    echo "$fid";
                    break;
                }
                $newname = dirname(__FILE__, 4)."/usr/img/figure/$id/$fid.png";
                if (!rename(FIGURE, $newname)) {
                    unlink(FIGURE);
                    dbAccess::delete('figure', 'FIGURE_ID', $fid, TRUE);
                    echo json_encode(array('REPORT'=>'couldn\'t write image on server'));
                    break;
                }
                $figure = dbAccess::get_data('figure', 'FIGURE_ID', $fid, TRUE);
                echo json_encode(array_merge($figure, array('REPORT'=>'success')));
                break;
            case 'figure-img':
                $icon = $_POST['canvas'];
                define('FIGURE', dirname(__FILE__, 4) . "/usr/img/figure/$id/demo.png");
                if (!empty($icon)) {
                    list($type, $icon) = explode(';', $icon);
                    list(,$icon) = explode(',', $icon);
                    $img = str_replace(' ', '+', $icon);
                    $icon = base64_decode($img);
                    if(!is_dir($img_dir = dirname(__FILE__, 4)."/usr/img/figure/$id")){mkdir($img_dir);}
                    if(!is_dir($img_dir)){echo 'couldn\'t write on server document root';break;}
                    if(file_exists(FIGURE)){unlink(FIGURE);}
                    file_put_contents(FIGURE, $icon);
                }
                if(file_exists(FIGURE)){echo 'success';break;}
                echo 'fail';
                break;
            case 'quest':
                $qtype = filter_input(INPUT_POST, 'qtype');
                $state = Quest::add($id, $qtype, true);
                echo "$state";
                break;
            case 'quest-text':
                $qid = filter_input(INPUT_POST,'qid');
                $lid = filter_input(INPUT_POST,'lid');
                $qtext = filter_input(INPUT_POST,'text');
                echo $state = Quest::add_text($qid, $lid, $qtext);
                break;
            case 'quest-ansr':
                $txt = filter_input(INPUT_POST, 'txt');
                $sound = filter_input(INPUT_POST, 'sound');
                $misnomer = filter_input(INPUT_POST, 'misnomer');
                $type = Quest::get_type($id);
                $reply = Quest::add_answer($id, $txt, $sound, $misnomer);
                
                if($reply === 'success'){
                    $ans_id = Quest::get_answer_id($id, $txt);
                    $answer = Quest::ls_ansr($id);
                    $fit = Quest::get_state($id);
                    echo json_encode(array('REQUEST'=>$reply,'ANS'=>$answer,'FIT'=>$fit,'ID'=>$ans_id));
                }else{
                    echo $reply;
                }
                break;
        }
        break;
    case 'update':
        $target = filter_input(INPUT_POST, 'target');
        $id = filter_input(INPUT_POST,'id');
        switch ($target) {
            case 'this':
                $instance_name = trim(filter_input(INPUT_POST,'name'));
                $instance_desc = Filter::escape(filter_input(INPUT_POST,'desc'));
                
                Item1::update($id,$instance_name,$instance_desc,TRUE);
                echo "0";
                break;
            case 'repo-icon':
                $icon = $_POST['canvas'];                
                define('INSTANCE_ICON',dirname(__FILE__, 4)."/usr/img/item1/$id.png");
                if(!empty($icon)){
                    list($type,$icon) = explode(';', $icon);
                    list(,$icon) = explode(',', $icon);
                    $img = str_replace(' ', '+', $icon);
                    $icon = base64_decode($img);
                    if(!is_dir($img_dir = dirname(__FILE__, 4).'/usr/img/item1')){mkdir($img_dir);}
                    if(!is_dir($img_dir)){echo 'couldn\'t write on server document root';break;}
                    if(file_exists(INSTANCE_ICON)){unlink(INSTANCE_ICON);}
                    file_put_contents(INSTANCE_ICON, $icon);
                }
                if (file_exists(INSTANCE_ICON)) {
                    echo Item1::set_icon($id, 'png', true);
                    break;
                }
                echo 'operation failed';
                break;
            case 'figure':
                $label = filter_input(INPUT_POST, 'label');
                $desc = Filter::escape(filter_input(INPUT_POST, 'desc'));
                $ftype = filter_input(INPUT_POST, 'type');
                $repo = dbAccess::get_data('figure', 'FIGURE_ID', $id, TRUE)['REPO_ID'];
                
                $icon = dirname(__FILE__, 4)."/usr/img/figure/$repo/$id.png";
                echo $state = Figure::update($label, $desc, $ftype, $id, true);
                break;
            case 'figure-img':
                $icon = $_POST['canvas']; 
                $repo = dbAccess::get_data('figure', 'FIGURE_ID', $id, TRUE)['REPO_ID'];
                $file = dirname(__FILE__, 4)."/usr/img/figure/$repo/$id.png";
                if(!empty($icon)){
                    list($type,$icon) = explode(';', $icon);
                    list(,$icon) = explode(',', $icon);
                    $img = str_replace(' ', '+', $icon);
                    $icon = base64_decode($img);
                    if(file_exists($file)){unlink($file);}
                    file_put_contents($file, $icon);
                }
                echo 'success';
                break;
            case 'quest-fig':
                $dml = filter_input(INPUT_POST,'dml');
                $fid = intval(filter_input(INPUT_POST, 'fid'));
                echo Quest::set_figure($id, $fid,$dml);
                break;
        }
        break;
    case 'rm':
        $target = filter_input(INPUT_POST, 'target');
        $id = filter_input(INPUT_POST, 'id');
        switch ($target) {
            case 'this':
                if(file_exists($icon_file = dirname(__FILE__,4)."/usr/img/item1/$id.". Item1::get_icon($id))){unlink($icon_file);}
                echo $state = Item1::rm($id);   //------------------ delete instance record in database
                break;
            case 'this-icon':
                define('INSTANCE_ICON',dirname(__FILE__, 4)."/usr/img/item1/$id.".Item1::get_icon($id));
                dbAccess::clear_field('repo', array('REPO_ICON'), NULL, 'REPO_ID', $id);
                Item1::set_icon($id, null, true);
                unlink(INSTANCE_ICON);
                echo 'success';
                break;
            case 'quest':
                echo $state = Quest::rm($id);
                break;
            case 'quest-text':
                $qid = filter_input(INPUT_POST, 'qid');
                $lid = filter_input(INPUT_POST, 'lid');
                $report = Quest::rm_text($qid, $lid);
                echo json_encode(dbAccess::get_data('language', 'LANG_ID', $lid, true));
                break;
            case 'figure':
                $repo = dbAccess::get_data('figure', 'FIGURE_ID', $id, TRUE)['REPO_ID'];
                $state = Figure::rm($id, true);
                if($state === 'success'){
                    $img = dirname(__FILE__, 4) . "/usr/img/figure/$repo/$id.png";
                    unlink($img);
                    if(file_exists($img)){  //  verify if file is gone
                        echo 'couldn\'t delete file';
                        break;
                    }
                }else{
                    echo "$state";
                    break;
                }
                echo "$state";
                break;
            case 'quest-ansr':
                $aid = Quest::get_quest_id_by_ansr($id);
                $report = Quest::rm_answer($id);
                $fit = Quest::get_state($aid);
                echo json_encode(array('REQUEST'=>$report,'STATE'=>$fit));
                break;
        }
        break;
    case 'test':
        echo 'connected!';
        break;
    default :
        echo 'nothing';
        break;
}