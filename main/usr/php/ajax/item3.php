<?php

require_once dirname(__FILE__, 4) . '/epiqworx/logic/sample.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/handler.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/reuse.php';
require_once dirname(__FILE__, 4) . '/model.php';
require_once dirname(__FILE__, 5) . '/work/model.php';
require_once dirname(__FILE__, 5) . '/profile/model.php';

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'test';
    }
}
switch ($action) {
    case 'count':
        $subject = filter_input(INPUT_GET, 'subject');
        $id = filter_input(INPUT_GET, 'id');
        switch ($subject){
            case 'this':
                echo dbAccess::count('paper', 'USER_ID', $_SESSION['id'], 'PAPER_ID');
                break;
        }
        break;
    case 'test':
        $subject = filter_input(INPUT_GET, 'subject');
        echo $subject;
        break;
    default :
        echo 'nothing';
        break;
}