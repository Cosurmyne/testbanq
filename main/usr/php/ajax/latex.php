<?php

abstract class Tex{
    public static function get_quest($quest,$ansr,$fig){
        $tex = '';  //  end result
        $repo = Item1::get_name($quest['REPO']);    //  repo name
        $qtex = dbAccess::get_data('quest_text', 'QUEST_ID', $quest['ID'], true);   //  question in all available languages
        
        //  question text
        $question = '';
        foreach ($qtex as $value) {
            if(is_array($value)){
                $question .= "  \\{$value['LANG_ID']}{{$value['QUESTION_TEXT']}}\n";
            }else{
                $question = "  {$qtex['QUESTION_TEXT']}\n";
            }
        }
                
        //  question answers
        $baker = array('1'=>'\\correctchoice','0'=>'\\wrongchoice');
        $tf = array('T'=>'TRUE','F'=>'FALSE');
        
        $answers = '';
        foreach ($ansr as $value){
            if(is_array($value)){
                if($quest['QTYPE_DESC'] === 'true/false') {$temp = $tf[$value['TEXT']];}
                else {$temp = $value['TEXT'];}
                $answers .= "   {$baker[$value['CORRECT']]}{{$temp}}\n";
            }else{
                $answers = 'something is wrong with answers';
            }
        }
        
        if(empty($fig)){
            $tex = "\\begin{question}{{$repo}-{$quest['ID']}}\n";
            $tex .= "$question";
            $tex .= " \\begin{choices}\n";
            $tex .= "$answers";
            $tex .= " \\end{choices}\n";
            $tex .= "\\end{question}";
        }else{
            $tex = "\\element{{$repo}}{\n";
            $tex .= " \\begin{figure}[p]\n";
            $tex .= "  \\centering\n";
            $tex .= "  \\includegraphics[width=.6\\linewidth]{{$fig['FIGURE_ID']}.png}\n";
            $tex .= "  \\caption{{$fig['DESCRIPTION']}}\n";
            $tex .= "  \\AMClabel{{$fig['FIGURE_LABEL']}}\n";
            $tex .= " \\end{figure}\n\n";
            
            $tex .= " \\begin{question}{{$quest['ID']}}\n";
            $tex .= "$question";
            $tex .= " \\begin{choices}\n";
            $tex .= "$answers";
            $tex .= " \\end{choices}\n";
            $tex .= " \\end{question}";
            $tex .= "}";
        }
        
        
        return $tex;
    }
}