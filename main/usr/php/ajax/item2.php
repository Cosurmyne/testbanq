<?php

require_once dirname(__FILE__, 4) . '/epiqworx/logic/sample.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/handler.php';
require_once dirname(__FILE__, 4) . '/epiqworx/db/reuse.php';
require_once dirname(__FILE__, 4) . '/model.php';
require_once dirname(__FILE__, 5) . '/work/model.php';
require_once dirname(__FILE__, 5) . '/profile/model.php';

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'test';
    }
}
switch ($action) {
    case 'count':
        $subject = filter_input(INPUT_GET, 'subject');
        $id = filter_input(INPUT_GET, 'id');
        switch($subject){
            case 'this':
                echo dbAccess::count('topic', 'USER_ID', $_SESSION['id']);
                break;
            case 'quest':
                echo dbAccess::count('quest_topic', 'TOPIC_ID', $id, 'QUEST_ID');
                break;
        }
        break;
    case 'ls':
        $subject = filter_input(INPUT_GET, 'subject');
        $id = filter_input(INPUT_GET, 'id');
        switch ($subject){
            case 'this':
                echo json_encode(Item2::ls());
                break;
            case 'types':
                echo json_encode(dbAccess::get_all('topic_type'));
                break;
            case 'index':
                echo json_encode(Item2::ls_quest($id));
                break;
            case 'this-quest':
                echo json_encode(Item2::ls_repo_quest($id, filter_input(INPUT_GET, 'rid')));
                break;
        }
        break;
    case 'create':
        $subject = filter_input(INPUT_POST, 'subject');
        switch ($subject) {
            case 'this':
                $ttype = filter_input(INPUT_POST, 'ttype');
                $ttitle = filter_input(INPUT_POST, 'ttitle');
                $state = Item2::add($ttype, $ttitle);
                if (empty($state)) { echo 'success';} 
                else { echo $state;}
                break;
        }
        break;
    case 'update':
        $subject = filter_input(INPUT_POST, 'subject');
        $id = filter_input(INPUT_POST, 'id');
        switch ($subject){
            case 'this':
                $ttype = filter_input(INPUT_POST, 'ttype');
                $ttitle = filter_input(INPUT_POST, 'ttitle');
                $state = Item2::update($ttype, $ttitle, $id);
                if (empty($state)) { echo 'success';} 
                else { echo $state;}
                break;
        }
        break;
    case 'select':
        $subject = filter_input(INPUT_GET, 'subject');
        $id = filter_input(INPUT_GET, 'id');
        switch ($subject){
            case 'this':
                echo json_encode(Item2::get_this($id));
                break;
        }
        break;
    case 'rm':
        $subject = filter_input(INPUT_POST, 'subject');
        $id = filter_input(INPUT_POST, 'id');
        switch ($subject){
            case 'this':
                $state = dbAccess::delete('topic', 'TOPIC_ID', $id, TRUE);
                if (empty($state)) { echo 'success';} 
                else { echo $state;}
                break;
            case 'this-quest':
                $qid = filter_input(INPUT_POST, 'qid');
                $state = Item2::rm_quest($id, $qid);
                if (empty($state)) { echo 'success';} 
                else { echo $state;}
                break;
        }
        break;
    case 'assign':
        $tid = filter_input(INPUT_POST, 'tid');
        $qid = filter_input(INPUT_POST, 'qid');
        $state = array('REPLY'=>Item2::add_quest($tid, $qid));
        if (empty($state['REPLY'])) {
            $state = array('REPLY'=>'success','LIST'=>Item2::ls_quest($tid));
            echo json_encode($state);
        }
        else { echo json_encode($state);}
        break;
    case 'test':
        echo 'testing';
        break;
}