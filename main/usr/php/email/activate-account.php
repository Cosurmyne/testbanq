<div style="font-family:Arial;min-height: 100%;color: #444;background-color: #d5d5d5;background: url(http://sict-iis.nmmu.ac.za/testbank/main/usr/img/background/bg-paper-01.png) no-repeat 0 -300px ">
    <header style="color: #737373;height: 70px;position: relative;width: 100%" class="main">
        <div class="container">
            <div style="float:left;height: 100%;max-width: 72px" id="branding">
                <a href="http://sict-iis.nmmu.ac.za/testbank" target="_blank" class="home-key" title="Home Page" style="height:100%"><img alt="Logo" class="logo" src="http://sict-iis.nmmu.ac.za/testbank/main/usr/img/sys/qb.png"  style="height:70px"></a>
            </div>
            <nav style="float: right;height: 100%">
                <h1 style=";color:#444;text-align:center"><span>Welcome to the </span><span><strong style="color:#d5d5d5;background-color:#444">&nbsp;testbank&nbsp;</strong></span></h1>
            </nav>
        </div>
    </header>
    <hr style="height:5px;background-color: #444;margin: 0"/>
    <section>
        <div class="container">
            <p class="big">
                <b>[</b> the science of making <strong style="background-color: #444;color:#d5d5d5">&nbsp;lecturer life&nbsp;</strong>,<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                equivalent to a land flowing with <strong style="background-color: #444;color:#d5d5d5">&nbsp;milk and honey&nbsp;</strong><br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                ...delivering the best <b>user expirience</b> money can by - <b>]</b>
            </p>
            <p>
                You've received this mail upon signing up for a <b>testbank</b><sup>TM</sup> account.<br/>
                Click the "<b style='color:#444'>Lecturer</b>" button below to finalize process as a Lecturer.
            </p>


            <p>
                <em>If you are a student, please  Click the "<b style='color:#444'>Student</b>" button bellow to finalize process as student.</em>
            </p>

            <p>
                Activating account also certifies your acceptance of the testbank <a href="http://sict-iis.nmmu.ac.za/testbank?action=about-tnc" target="_blank" style="font-weight: 700;color: #444">Terms of Service</a>.<br/>
                You might want to read these before continuing.
            </p>
            <p style="font-family:Arial;padding:0 2em"><b style="font-size:9pt">HINT: </b><br/>
                After activating account, this mail becomes absolete, so there's no point keeping it in your mailbox.
            </p>
            <br/>
            <p>
                <b class="cap-line"><b>R</b><span>egards</span></b><br/>
                the Sanhedrin<br/>
                <b style="font-weight:100;font-size:9pt">
                    School of ICT (3224)<br/>
                    <a href="http://www.mandela.ac.za" target="_blank">Nelson Mandela University</a>
                </b>
            </p>
        </div>
    </section>
        <hr/>
        <div style="position:relative;width: 100%" class="container clearfix foot">
            <img src="http://sict-iis.nmmu.ac.za/testbank/main/usr/img/user/dev/compact-sanhedrin.png" alt='sanhedrin' height="170" />
            <img src="http://sict-iis.nmmu.ac.za/testbank/main/usr/img/sys/qb-ink.png" alt='testbank' height="170"/>
        </div>
</div>
<style>
    /*.container{width: 100%;margin:auto;overflow:hidden}*/
    p{padding: 0 2em}
    .cap-line{font-size:16pt}
    .cap-line b{float: left;width: 0.7em;font-size: 300%;line-height: 80%;margin-right: .1em}
    .cap-line span{margin-left: -.1em}
    .clearfix:after{content: "";display: table; clear:both}
    .small{display: none}
    .foot{height:170px;display: block;position: relative}
    .foot img{float: right}
    @media screen and (max-width: 480px){
        nav h1 span{display: block}
        .container{width: 100%;margin: 0}
        header{text-align: center}
        header h1{margin: .2em 0}
        p{padding: 0 .6em}
        .small{display: block}
        .big{display: none}
        .foot{height: 48px}
        
    }
</style>