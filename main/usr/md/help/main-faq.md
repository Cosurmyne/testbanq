![Dashboard](main/usr/img/help/workspace.png){.dash-faq} 
# Components {.faq-head-aside}
* ## 1 Main navigation bar
* ## 2 Dashboard navigation bar
* ## 3 Side bar
* ## 4 Instance menu
* ## 5 Status bar
* ## 6 Workspace 
<br/>

* **Main navigation bar** Contains navigation options that allow you to navigate through the web-site to pages outside of the current page you currently are on.
Hover over the expandable menu to have more navigation options revealed, under the various menu headings. <br/>
* ### Dashboard navigation bar
The tabs allow you to switch between views of the dashboard that allow to complete the desired task.   <br/>
*  ### Question Bank
 All your repositories and questions can be managed from here.  
  **Topics** Topic creation and management is done under this tab.  
  **Scripts** Where users can create and organize scripts using questions from the Question Bank. <br/>
  **Statistics** Useful statistics about tests and questions can be found here.  <br/>
* ### Side bar
 Context sensitive side bar allows you to edit, delete or  add an instance if none exists.<br/>
* ### Instance menu
 displays available instances according to which page you are on. <br/>
* ### Status bar
 The bottom status bar gives context sensitive information about the available and selected instances. <br/>
<br/><br/>
![Add Databank](main/usr/img/help/new-repo.png){.add-thumb-faq} <br/>
# Adding Question Bank 
When creating a new Data Bank clicking the add button brings up a hidden control.
this hidden control allows you to name your data bank, once you have one population can commence.  
Once a Question Bank exists selecting one will then reveal controls that allow progress.  

![Add Databank](main/usr/img/help/editing.png){.edit-thumb-faq}
*  ## 7 Update Button 
Allows you to update Question bank particulars including allowing a user you to assign
an image to the Question Bank icon. <br/>
*  ## 8 Update form 
Where any changes actually go, from this form you can update the name of your Question Bank and assign or change the icon if one has been assigned. <br/>
*  ## 9 Bread crumb 
The status bar displays context sensitive information including navigation bread-crumb
shows current location and allows navigation. <br/>
*  ## 10 Back Button 
No matter which tab of the menu you are in the back button serves as means to return or deselect current/active instance. <br/><br/>
<br/>
![Databank Functionality](main/usr/img/help/figure.png){.figure-thumb-faq} <br/>
*  ## 11 Add question
When an instance or a Data bank exists controls to add a question is made available. <br/>
*  ## 12 Add figure
When you desire to add a figure this control will allow you to then select and crop a figure as you see fit.
*  ## 13 Figure 
figures that already exist can be viewed if you select the view figure option. <br/>
*  ## 14 Figure table
Here all available figures will be spread out in the table and you may view and edit them. <br/>
*  ## 15 Right click menu
if you right click on a figure you may choose to view or edit it from the menu that then appears. <br/><br/><br/><br/>