##### Your continued use of the **this project** certifies your acceptance of the following terms and conditions.       Revoking such acceptance is simply attainable through withdrawing use of website.

1. # Content Policy
    #### T
    he core of the **this project** is to provide web developers with cohesive tools to instantly build a responsive and effective custom web application.  
    The project works by by dividing server technologies into branches, with the **master** branch affiliated with none but **AJAX** and **CSV**.

    Users reserve all rights to the content and information they post on this **project**, and control how it is published through integrated options and settings.

1. # Usage Constraints
    This project is developed and maintained by **the Sanhedrin**. This, naturally, obliges this party to monitor content on the website.  
    However, the following is a list of amendments garranteed by **the Sanhedrin** as part of the policy bundled with the use of website:-
    1. **The Sanhedrin** notifies users before-hand in making changes to these terms, granting the opportunity to review and comment on the revised terms before further use of the project.
    1. On making of changes to terms or policies referenced in or incorporated by this article, such changes are to be reflected with immediate effect.
    1. Persistent use of the project upon notice of the changes to terms or policies, constitutes acceptance of the newly amended terms or policies.

1. # Licensing
    #### A
    ll content and building blocks of website, from the technology that constructs the back-end, to the artwork that furnishes the structure, is powered by [free](https://www.gnu.org/philosophy/free-sw.en.html) and
    open-source software, licensed under the [Free Software Foundation](http://www.fsf.org)'s [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html), 
    [Apache Software Foundations](https://www.apache.org/)'s **Apache License**, and other relevant licenses, (except for non -licensed open standards).  
    This choice of licensing ensures continued development of the project, without any obstacles that might be caused by legal stunts pulled by well-established industry bullies, in the long run.

    The following is a sample of such technologies:-
    1. [GNU](https://www.gnu.org/)'s **GNU/Linux** (Operating System)
    1. [Apache Software Foundations](https://www.apache.org/)'s **Apache2** (HTTP server)
    1. [Oracle](https://www.oracle.com/)'s **MySQL Server 5.7 Community** (Database Management System)
    1. [World Wide Web Consortium](https://www.w3.org/)'s **HTML5** & **CSS3/4** (Graphical User Interface)
    1. [Netscape](http://isp.netscape.com/)'s **JavaScript** (Client-Side Scripting)
    1. [Zend Technologies](http://www.zend.com/)'s **_PHP7.2_** (Server-Side Scripting)
    1. [Sun Microsystems](https://www.oracle.com/sun/)' **NetBeans IDE** (Integrated Development Environment)
    1. [Linus Torvalds](https://github.com/torvalds)' **Git** (Version Control System)
    1. [Mozilla Corp.](https://www.mozilla.org/)'s **Firefox** (Testing Browser)
    1. [Bram Moolenaar](http://moolenaar.net/)'s **Vim** (Power Text Editor)
    1. [GNU](https://www.gimp.org/)'s **GIMP** (Image Editor)

    **The Sanhedrin** might not be formally licensed under [FSF](http://www.fsf.org)'s [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html) as of yet, but heavily employ and embraces their philosophy.  
    This statement publicly declares the project, by **the Sanhedrin**, to be non-proprietary, with the following public repository: [https://epiquadruple@bitbucket.org/epiquadruple/testbank.git](https://epiquadruple@bitbucket.org/epiquadruple/testbank.git),  
    This privacy-respecting philosophy allows for the public to detect any form malicious code designed to spy on its users, and make informed decisions.

    Opening the homengine source code for public view also invites the public to review and further improve the project functionality.
1. # External Sources
    #### T
    he **project** strives to keep dependencies to external forces to the minimal, particularly data required from its users, and integrating third-party systems.  
    However, the potent nature of some of the user's mandatory fields cannot be ignored, nor overlooked.  
    One instance of such is the user's personal email account, which is required at the very initial stage of user account creation.  
    The user's email account is the sole means by which the **project** is able to communicate with its users, apart from itself, of cause.  
    For this reason, a verification email is sent to user for the express purpose verifying email account authenticity.

    User account cration happens in two stages:
    1. The initial phase where the user basically walks away with a unique username, and initial system role, on success.
    1. After email verification, user is then forced to create a strong password to protect account from infiltration. Other fields are also available to fill in this stage.

    The mandatory requirement of user's personal email address also aids to help user reset password.  
    On the event that the user forgets login details, the email account poses as a force to be reckoned with.

    ###### **project**^TM^ is an unregistered trademark of the **Sanhedrin**, all rights reserved by the [Nelson Mandela University](https://www.mandela.ac.za).        This project launched as an academic exercise of module code **ONT3660**, of qualification **3224**, School of Information and Communication Technology, [Nelson Mandela University](http://mandela.ac.za), 2018.
