# What is TestBanq? {.title-head}
The human race in general don't enjoy unnecessarily going through a tedious processes time and time again.

However, for lecturers this burden is inescapable in the subject of preparing test papers for students.
This in some cases explains why test papers have been almost identical throughout the years.

In a world, however, where student competence is at question, the above described senior becomes not so ideal.

The following is a list of solutions attempting to rectify status quo

| Application | Platform |
| ------ | ----------- |
| AMC (Auto-Multiple Choice)   | GNU/Linux |
| Make Your Own Test | Android |
| CAD (Create A Quiz )    | Android |

Amongst many shortcomings of the mentioned solutions, the obvious one that stands out is that, they are all platform specific.

Another noteworthy shortcoming of one solution in particular, **AMC**, is that it put the technical expertise of the users into question, by means of a _'techno-jargon'_ language, LaTeX.

**TestBanq** is a web-based application for creating and managing test/exam papers, with great convenience.