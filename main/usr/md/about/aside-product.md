### How It Works {.title-head}

All user content is stored inside reposotories, which they can create at any time.
This content include **questions**, and **figures** that some of the questions might depend upon.

These **questions** can then be used to instantly set question papers to test subjects, or give an exam.

When question papers are ready, they are printed and given to test subjects to take in **hard copy**.

Question papers are in **multiple choice** form, and subjects **shade boxes** parallel to the relevant answers.

Answer scripts are then scanned on submission, for **system-automated marking**.

Test Subjects are then **notified** of their performance in test/exam.