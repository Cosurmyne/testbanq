function load() {
    window.sessionStorage.clear();
	/* containers containing javascript content are hidden by default
	* the following lines of code removes the css class hiding them
	*/
	var container = document.getElementsByClassName('js');
	for (var i = container.length - 1; i >= 0; i--) {
		container[i].classList.remove('js');
	}
        // initializing communication panes
	var cmc = document.getElementsByClassName('cmc');
	for (var i = 0; i < cmc.length; i++) {
		cmc[i].innerHTML = cmc[i].title;
	}
	if(url_exists(window.localStorage.item3)){
            get_ajax(window.localStorage.item3,prepare_list);
            get_ajax(window.localStorage.item3 + '?action=count&subject=this',function(count){
                m_osd_1.innerHTML = osd_1.innerHTML = count;
            });
        }
	else {
		instance_h1.innerHTML = 'Datasource Error';
		instance_p1.innerHTML = 'Item list can\'t be rendered';
		instance_p2.innerHTML = 'Address the following anormally;-';
		instance_err.innerHTML = 'file \''+ localStorage.item3 +'\' missing!';
		return;
	}
	sideline('100',true);	// active button on the left sideline to add instance
}
document.body.onload = load;
document.body.addEventListener('keypress',function(e){
	switch(e.key){
		case 'F1':
		console.log('mayday!');
		break;
		case 'F4':
		case 'F8':
		case 'F9':
		test(e.key);
		break;
		default:
		// console.log(e.key);
		break;
	}
});
//<editor-fold desc="Scripts" defaultstate="collapsed">
function prepare_list(data){
	let record = csv_query(data,';',2,sessionStorage.userid);
	if(record.length > 0)	record.forEach(present_instance);
	else {
		var li = document.createElement('li');
		li.innerHTML = 'list empty';
		instance_list.appendChild(li);
	}
}
function present_instance(instance,parent) {
    
}
//</editor-fold>
//<editor-fold desc="Side Line" defaultstate="collapsed">
function instance_dml(action){
    purge_panels();
    switch_panels(panel_dml, panel_default);
    l2_nav(action + ' script',action);
    switch(action){
        case 'new':
            instance_ls_shield.classList.remove('display-none');
            btn_save_instance.onclick = create_topic;
            break;
    }
}
//<editor-fold desc="Event Handling" defaultstate="collapsed">
btn_sl_mk.onclick = function(e){instance_dml('new');};
//</editor-fold>
//</editor-fold>
//<editor-fold desc="Navigation" defaultstate="collapsed">
function purge_panels(boogeyman) {
    sideline('000', false);	// activate * buttons on sideline
    if (typeof boogeyman !== "string" || boogeyman.trim().length < 1) boogeyman = 'display-none';
    //  -------------------------------------- remove Instance Select panel
    /* The Dash Has 3 Cells
     * cell 1 list instances of current tab item
     * cell 2 is the main functionality content on the selected instance
     * cell 3 is the aside panel, with additional functionality
     */
    // cell 2
    instance_ls_shield.classList.add(boogeyman);
    panel_default.classList.add(boogeyman);
    panel_dml.classList.add(boogeyman);
}
function m_crumb_nav() {
    let crumb = document.getElementsByClassName('crumb+');
    let buttom_ctrl = document.getElementsByClassName('link_addx');
    let cmp = document.getElementsByClassName('instance-active');
    switch (crumb.length) {
        case 2:
            window.sessionStorage.removeItem('paper_id');
            purge_panels();
            panel_default.classList.remove('display-none');
            switch_panels(list_panel, panel_content, 'x768');
            // change instance selection to active
            for (i = 0; i < cmp.length; i++) cmp[i].classList.remove('instance-active');
            // return initial page status
            instance_h1.innerHTML = 'No Script Selected';
            instance_p1.innerHTML = 'Please select Script to your left.';
            instance_p2.innerHTML = 'Or use large cross to add one.';
            instance_err.innerHTML = null;
            //  conceal instance manipulation bottom keys
            for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.add('display-none');
            osd_3.innerHTML = m_osd_3.innerHTML = 'null';
            // reset mobile keys
            m_btn_dml.classList.remove('shift');
            m_panel_dml.classList.remove('shift');
            m_btn_back_to_ls.classList.add('display-none');
            list_panel.classList.remove('x1024');
            _btnback.classList.add('display-none');
            wrap_btn_back.classList.add('display-none');
            sideline('100', true);
            break;
    }
    m_nav_crumb.removeChild(m_nav_crumb.lastChild);
    bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
}
function l2_nav(panel, specific) {
    _btnback.classList.remove('display-none');
    btn_back.classList.remove('display-none');
    wrap_btn_back.classList.remove('display-none');	// reveal back button
    // mobile navigation breadcrumb
    let aug = document.getElementsByClassName('crumb+');
    if (aug.length < 2) {
        if (aug.length > 1) {
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        } // ensures 2nd slice on bread
        //  accumulate breadcrumb with current panel
        crumbplus(aug, panel, specific);
        // mobile view navigation
        m_btn_dml.classList.add('shift');
        m_panel_dml.classList.add('shift');
        m_btn_back_to_ls.classList.remove('display-none');
        list_panel.classList.add('x1024');
    } else {
        bn_l1.firstChild.innerHTML = panel;
    }
}
//<editor-fold desc="Event Handling" defaultstate="collapsed">
btn_back.onclick = m_crumb_nav;
_btnback.onclick = m_crumb_nav;
//</editor-fold>
//</editor-fold>
//<editor-fold desc="Form Input" defaultstate="collapsed">
function create_script(stage,reply){
    if(typeof stage !== 'number')   stage = 1;
    input_error_instance.innerHTML = null;
    switch(stage){
        case 1:
            
            break;
        case 2:
            
            break;
    }
}
//</editor-fold>