function load() {
    window.sessionStorage.clear();
	/* containers containing javascript content are hidden by default
	* the following lines of code removes the css class hiding them
	*/
	var container = document.getElementsByClassName('js');
	for (var i = container.length - 1; i >= 0; i--) {
		container[i].classList.remove('js');
	}
        // initializing communication panes
	var cmc = document.getElementsByClassName('cmc');
	for (var i = 0; i < cmc.length; i++) {
		cmc[i].innerHTML = cmc[i].title;
	}
        window.sessionStorage.removeItem('repo_id');
        window.sessionStorage.removeItem('quest_id');
        
	if(url_exists(window.localStorage.item1))	{
            get_ajax(window.localStorage.item1+'?action=ls&target=this',function(data){prepare_list(data,instance_list,'local');});  // prepare list of instances (repos)
            get_ajax(window.localStorage.item1+'?action=count&target=this',function(data){  // count objects in instance indexes
                repo_count_local.innerHTML= data;	// item 1 instance list quantity
                window.sessionStorage.removeItem('repo_id');
            });
            get_ajax(window.localStorage.item1+'?action=ls&target=this-groups',group_ls);
            get_ajax(window.localStorage.item1+'?action=count&target=groups',function(data){osd_1.innerHTML = m_osd_1.innerHTML = data;});
            get_ajax(window.localStorage.item1 + '?action=ls&target=quest-types', function (data) {
            let qtypes = JSON.parse(data);
            input_quest_07.innerHTML = null;
            qtypes.forEach(function (record) {
                let opt = document.createElement('option');
                opt.value = record['QTYPE_ID'];
                opt.innerHTML = record['QTYPE_DESC'];
                if (record['QTYPE_DESC'] === 'multichoice') opt.selected = true;
                if(record['QTYPE_ID'] !== 'C1S')    input_quest_07.appendChild(opt);
                if(experimental.value === '1' & record['QTYPE_ID'] === 'C1S') input_quest_07.appendChild(opt);
            });
    });
            get_ajax(window.localStorage.item1 + '?action=ls&target=fig-types',update_fig_type_ls);
        }
	else {
		instance_h1.innerHTML = 'Datasource Error';
		instance_p1.innerHTML = 'Item list can\'t be rendered';
		instance_p2.innerHTML = 'Address the following anormally;-';
		instance_err.innerHTML = 'file \''+ localStorage.item1 +'\' missing!';
		return;
	}
        
        //  context sensitive help
        // create quest
        let help = [document.createElement('a')];
        
        help[0].innerHTML = 'Create Question';
        help[0].href = 'javascript:';
        help[0].classList = 'isolate';
        
        for(let i = 0 ; i < help.length ; i++) big_menu_help.appendChild(help[i]);
        
        let temp_man = big_menu_man;
        let temp_faq = big_menu_faq;
        big_menu_help.removeChild(big_menu_man);
        big_menu_help.removeChild(big_menu_faq);
        big_menu_help.appendChild(temp_man);
        big_menu_help.appendChild(temp_faq);
        
	sideline('100',true);	// active button on the left sideline to add instance
}
//<editor-fold desc="repository" defaultstate="collapsed">
// presentation
function prepare_list(data,parent,target,remote){
    let record = JSON.parse(data);
    if (record.length < 1) {
        var li = document.createElement('li');
        li.innerHTML = 'list empty';
        li.classList = target + '-instance-zero';
        parent.appendChild(li);
    }else if(typeof record === 'object' && !Array.isArray(record)){
        record['IMG'] = item_icon(record, false);
        present_instance(record,parent,remote);
    }else{
        for (var i = 0; i < record.length; i++) {
            record[i]['IMG'] = item_icon(record[i], false);
            present_instance(record[i],parent,remote);
        }
    }
}
function group_ls(data){
    let groups = JSON.parse(data);
    if (groups.length < 1) return;
    else if(typeof groups === 'object' && !Array.isArray(groups)){
        build_group_ls(groups);
    }else{
        groups.forEach(build_group_ls);
    }
}
function build_group_ls(group){
    let count = document.createElement('span');
    count.id = 'repo_count_' + group['GROUP_NAME'];
    count.innerHTML = group['REPOS'];
    count.classList = 'instance-count';
    count.title = 'repo count';
    
    let btn = document.createElement('button');
    btn.classList = 'collapsible';
    btn.innerHTML = group['GROUP_NAME'];
    btn.appendChild(count);
    btn.addEventListener('click',function(e){folding(this);});
    
    let ul = document.createElement('ul');
    ul.id = group['GROUP_ID'] + '_repo_ls';
    
    let fold = document.createElement('div');
    fold.classList = 'fold';
    fold.appendChild(ul);
    
    list_panel.appendChild(btn);
    list_panel.appendChild(fold);
    get_ajax(window.localStorage.profile + '?action=group-repos&gid='+group['GROUP_ID'],function(data){
        prepare_list(data,ul,group['GROUP_NAME'],true);
    });
}
// each instance is to be present in a single unordered list item
function present_instance(instance,parent,remote){
	if(instance.length < 3) return;	// validate instance
	/* create instance element child nodes
	*/
    if(typeof remote !== 'boolean') remote = false;

    let owner = instance['USERNAME'];
       if(owner === user_online.innerHTML) owner = 'yours';
       else{
           if(owner.substr(owner.length-1,1) === 's') owner += '\'';
           else owner += '\'s';
       }

	// instance icon
	var img = document.createElement('img');
        img.id = 'img_' + instance['REPO_ID'];
	img.height = 48;
	img.alt = 'icon';
	img.src = instance['IMG'];

	// instance name
	var span_name = document.createElement('span');
	span_name.classList.add('list-title-text');
	span_name.innerHTML = instance['REPO_NAME'];
    if(owner === 'yours' && remote) span_name.innerHTML = instance['REPO_NAME'] + ' <b>(yours)</b>';

	// instance description
	let desc = '';
        if(typeof instance['DESCRIPTION'] === 'string' && instance['DESCRIPTION'].length > 0){
            desc = instance['DESCRIPTION'].replace('\\xd\\xa',' ').replace('/',' ');
            desc = desc.replace('/',' ');
            if(desc.length > 32) desc = desc.substr(0,32)+ '...';
        }
	var span_desc = document.createElement('span');
	span_desc.classList.add('list-desc-text');
	span_desc.innerHTML = desc;

	// instance [name + description] container
	var text = document.createElement('h1');
	text.appendChild(span_name);
	text.appendChild(span_desc);
    if(owner === 'yours' && remote) text.classList = 'cursor-default';


	// instance text ultimate container
	var text_div = document.createElement('div');
	text_div.classList.add('flex-column');
	text_div.classList.add('list-body');
	text_div.appendChild(text);

	// instance 1st generation child
	var container = document.createElement('div');
	container.classList.add('flex-row');
	container.appendChild(img);
	container.appendChild(text_div);

	var li = document.createElement('li');
	if(owner === 'yours' && remote) li.classList = 'dim cursor-default';
    else li.addEventListener('click',function(e){instance_select(this,instance);});
	li.appendChild(container);
    li.title = owner;
    li.setAttribute('data-owner',instance['USERNAME']);
	parent.appendChild(li);
}
function instance_select(ctrl,instance){
    // change instance selection to active
    let cmp = document.getElementsByClassName('instance-active');
    for (i = 0; i < cmp.length; i++) cmp[i].classList.remove('instance-active');
    ctrl.classList.add('instance-active');
    
    panel_quest_preview.classList.add('display-none');
    if(crumbsection() !== 'instance-editing') {
        panel_index2_ls.classList.remove('display-none');
    }

    // keep instance id to perform updates
    window.sessionStorage.repo_id = instance_id.value = rm_instance_icon.value = figure_repo_id.value = instance['REPO_ID'];
    // display instance information
    tf_instance_name_00.value = tf_instance_name_00.title = instance['REPO_NAME'];
    ta_instance_desc_01.value = ta_instance_desc_01.title = instance['DESCRIPTION'];
    instance_icon_display.src = input_instance_02.title = item_icon(instance, true);
    input_instance_02.value = null;
    if (typeof instance['REPO_ICON'] === 'string' && instance['REPO_ICON'].length > 0) {
        item_icon_ctrl.classList.remove('display-none');
        m_btn_view_icon.classList.remove('display-none');
    } else {
        item_icon_ctrl.classList.add('display-none');
        m_btn_view_icon.classList.add('display-none');
    }
    _btnback.classList.remove('display-none');
    wrap_btn_back.classList.remove('display-none');	// conceal back button
    // check selected instance index I objects
    get_ajax(window.localStorage.item1+'?action=ls&target=index&id='+instance['REPO_ID'],function(data){
        present_indices(instance,data);
    });
    // mobile navigation breadcrumb
    let aug = document.getElementsByClassName('crumb+');
    if(aug.length < 2){
        if(aug.length > 1) {
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        } // ensures 2nd slice on bread

        //  accumulate breadcrumb with current panel
        crumbplus(aug,instance['REPO_NAME']);
        //  reveal instance manipulation bottom keys

        // mobile view navigation
        m_btn_dml.classList.add('shift');
        m_panel_dml.classList.add('shift');
        m_btn_back_to_ls.classList.remove('display-none');
        // on mobile, alternate instance list panel with that listing index[1] items
        switch_panels(panel_content, list_panel, 'x768');
        list_panel.classList.add('x1024');
    }else {
        bn_l1.firstChild.innerHTML = instance['REPO_NAME'];
        if(crumbsection() === 'quest-prev' && aug.length > 2){
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        }
    }
    // active UPDATE and DELETE instance buttons
    if (panel_dml.classList.contains('display-none')) {
        if(ctrl.title === 'yours') {
            sideline('011', true);
            let buttom_ctrl = document.getElementsByClassName('link_addx');
            for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.remove('display-none');
        }
        else {
            sideline('000',true);
            let buttom_ctrl = document.getElementsByClassName('link_addx');
            for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.add('display-none');
        }
     }
}
function present_indices(repo, data) {
    let index = JSON.parse(data);
    let aug = document.getElementsByClassName('crumb+');
    tbl_ls_x2.innerHTML = tbl_ls_x1.innerHTML = null;   //  clear tables

    if (index['QUEST'].length < 1 && index['FIG'].length < 1) {
        osd_3.innerHTML = m_osd_3.innerHTML = osd_2.innerHTML = m_osd_2.innerHTML = 0;
        instance_h1.innerHTML = 'Repo <b>\'' + repo['REPO_NAME'] + '\'</b> Empty';
        instance_p1.innerHTML = 'Each repo has two indexes to populate, <b>Questions</b>, & <b>Figures</b> that some of the questions might depend on.';
        instance_p2.innerHTML = 'Use <b>\'+\'</b> buttons below to add content to each index.';
        switch_panels(instance_info,ls_quest);
        panel_index2_ls.classList.add('display-none');
    } else {
        instance_h1.innerHTML = null;
        instance_p1.innerHTML = null;
        instance_p2.innerHTML = null;
        if(aug.length<3)panel_index2_ls.classList.remove('display-none');
        switch_panels(ls_quest,instance_info);
    }
    //  -------------------------------------------------------------- question
    if (index['QUEST'].length < 1){
        let cell = document.createElement('td');
        cell.innerHTML = 'repo contains no questions yet';
        cell.classList = 'align-center ls-empty';

        let row = document.createElement('tr');
        row.classList = 'ls-rm-quest';
        row.appendChild(cell);
        tbl_ls_x1.appendChild(row);
    } else if(typeof index['QUEST'] === 'object' && !Array.isArray(index['QUEST'])){
        build_quest_ls(index['QUEST']);
    } else {
        for (let i = 0; i < index['QUEST'].length; i++) build_quest_ls(index['QUEST'][i]);
    }
    //  ---------------------------------------------------------------- figure
    if (index['FIG'].length < 1){
        let cell = document.createElement('td');
        cell.innerHTML = 'repo contains no figures yet';
        cell.classList = 'align-center ls-empty';

        let row = document.createElement('tr');
        row.classList = 'ls-rm-fig';
        row.appendChild(cell);
        tbl_ls_x2.appendChild(row);
    } else if(typeof index['FIG'] === 'object' && !Array.isArray(index['FIG'])){
        build_fig_ls(index['FIG']);
    } else {
        for (let i = 0; i < index['FIG'].length; i++) build_fig_ls(index['FIG'][i]);
    }

    get_ajax(window.localStorage.item1 + '?action=count&target=databank&id=' + window.sessionStorage.repo_id, function (count) {
        count = JSON.parse(count);
        osd_3.innerHTML = m_osd_3.innerHTML = count['QUESTIONS'];
        osd_2.innerHTML = m_osd_2.innerHTML = count['FIGURES'];
    });
}
function item_icon(field,absolute){// locate item icon image file
	var img_file = '../main/usr/img/sys/logo.png';
	switch(field['REPO_ICON']){	// field[5] stores file extention of icon image, empty if no icon assigned
		case null:
		if(absolute) return img_file;
		switch(field['PUBLIC']){	// field[3] stores item availability status, 1 = public, 0 = private
			case '1': img_file = '../main/usr/img/sys/public-instance.png';	break;
			case '0': img_file = '../main/usr/img/sys/private-instance.png';	break;
		}
		break;
		default:
		// 'path' + primary key + '.' + img extention = file name
		img_file = '../main/usr/img/item1/'+field['REPO_ID']+'.'+field['REPO_ICON'];
		break;
	}
	return img_file;
}
// repo template maneuver
function sideline_add(active){
	let new_selexion;
	if(active){
		new_selexion = '0' + sideline_status.value.toString().substr(1,2);	// deactivate 'add; key
		form_add_item.classList.remove('display-none');
		txt_instance_name.focus();
	}else{
		new_selexion = '1' + sideline_status.value.toString().substr(1,2);	// activate 'add; key
		form_add_item.classList.add('display-none');
	}
	sideline(new_selexion,true);
}
function edit_instance(edit){
    if (edit) {
        purge_panels();
        switch_panels(panel_dml, panel_default);	// switch default panel with input form
        panel_img.classList.remove('display-none');	// reveal instance icon panel
        sideline('001', false);	// deactivate update button on sideline
        wrap_btn_back.classList.remove('display-none');	// reveal back button
        instance_ls_shield.classList.add('display-none');   // shield protecting against selecting another instance
        //  -------------------------------------------- remove Instance Buttom ctrl keys
        let buttom_ctrl = document.getElementsByClassName('link_addx');
        for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.add('display-none');
        
        //  accumulate breadcrumb with current panel
        let aug = document.getElementsByClassName('crumb+');
        while(aug.length > 2){
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        }
        crumbplus(aug,'repo edit','instance-editing');
        
        list_panel.classList.add('big-1280');
        _btnback.classList.remove('display-none');
    } else {
        switch_panels(panel_default, panel_dml);	// switch input form with default panel
        panel_img.classList.add('display-none');	// conceal instance icon panel
        sideline(sideline_status.value, false);	// activate update button on sideline
        let buttom_ctrl = document.getElementsByClassName('link_addx');
        for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.remove('display-none');
        list_panel.classList.remove('big-1280');
    }
}
// repo DML
function new_repo(stage,target){
    switch(stage){
        case 1:
            switch_panels(instance_add_vss,target);
            if (!validate_repo_name(txt_instance_name)) {
                switch_panels(target,instance_add_vss);
                break;
            }
            post_ajax(window.localStorage.item1,'action=create&target=this&name='+txt_instance_name.value,function(data){
                new_repo(2,data);
            });
            break;
        case 2:console.log(target);
            try {
                let = repo = JSON.parse(target);

                txt_instance_name.value = null; //  clear 'create repo' input text field
                repo['IMG'] = item_icon(repo, false);   //  assign default repo icon
                let rm = document.getElementsByClassName('local-instance-zero');  //  remove list item reporting asbsence of repos
                for(let i = 0; i < rm.length ; i++) instance_list.removeChild(rm[i]);
                present_instance(repo,instance_list); //  add new repo list bitem
                switch_panels(btn_add_instance,instance_add_vss); //  return 'creste repo' save button
                sideline_add(false);    //  hide 'create repo' panel
                //  count repo total
                get_ajax(localStorage.item1 + '?action=count&target=this', function (data) {
                    repo_count_local.innerHTML = data;	// item 1 instance list quantity
                });
            }catch(ex){
                instance_err.innerHTML = target;
                console.log(ex);
            }
            break;
    }
}
function update_repo(stage,target){
    if(typeof stage !== "number")  stage = 1;
    switch(stage){
        case 1:
            switch_panels(instance_update_vss,target);
            let valid = validate_field_0(tf_instance_name_00);
            
            if(!valid){
                switch_panels(target,instance_update_vss);
                break;
            }
            post_ajax(window.localStorage.item1,
            'action=update&target=this&id='+instance_id.value+'&name='+tf_instance_name_00.value+'&desc='+ta_instance_desc_01.value,
            function(data){update_repo(2,data);});
            break;
        case 2:
            if(target==='0') window.location ='.';
            switch_panels(instance_update_vss,instance_update_vss);
            break;
    }
}
function rm_repo(stage,target){
    if(typeof stage !== 'number') stage = 1;
    switch(stage){
        case 1:
            switch_panels(vss_sl_rm,btn_sl_rm);
            post_ajax(window.localStorage.item1,'action=rm&target=this&id=' + window.sessionStorage.repo_id,function(reply){
                rm_repo(2,reply);
            });
            break;
        case 2:
            switch(target){
                    case 'success':
                        window.location = '.';
                        break;
                    default:
                        content_err.innerHTML = target;
                        switch_panels(btn_sl_rm,vss_sl_rm);
                        break;
                }
            break;
    }
}
//</editor-fold>
//<editor-fold desc="question" defaultstate="collapsed">
function x1_add(){
    purge_panels(); //  reset workspace panel
    panel_purge_quest('quest add'); // reset question panel
    panel_x101.classList.remove('display-none');    //  reveal 'new quest' panel
    nav_section_02.classList.remove('display-none'); // reveal 'next section nav' key
    panel_quest_intro.classList.remove('display-none');
    
    nav_section_05.childNodes[1].classList.remove('display-none');
    
    window.sessionStorage.removeItem('quest_id');
    window.sessionStorage.removeItem('quest_type');
    input_quest_10.setAttribute('data-id','');
    input_ansr_text.setAttribute('data-action','');
}
function build_quest_ls(quest) {
    if (quest['USERNAME'] !== user_online.innerHTML && quest['READY'] !== '1') return;// return complete questions only, on remote repos
    
    let qtxt = quest['QUESTION'];
    let txtlen = 32;
    if(typeof qtxt === 'string' && qtxt.trim().length > txtlen) qtxt = qtxt.substr(0, txtlen) + '...';
    
    let cell = [document.createElement('td'),document.createElement('td'), document.createElement('td'),document.createElement('td'), document.createElement('td')];
    let shell = [document.createElement('span'),document.createElement('span'),document.createElement('span'), document.createElement('span')];
    for(let i = 0 ; i < shell.length ; i++) cell[i].appendChild(shell[i]);
    
    let context_menu_items = [
        {
            'icon' : 'fa fa-eye',
            'id': quest['ID'],
            'action': 'View',
            'text'  :   'View Question',
            'click' :   function(e){
                get_ajax(window.localStorage.item1 + '?action=select&target=quest&task=V&id=' + quest['ID'], function (data) {
                    quest_select(document.getElementById('row_quest_' + quest['ID']), data);
                });

            }
        },
        {
            'icon' : 'fa fa-edit',
            'id': quest['ID'],
            'action': 'Edit',
            'text'  :   'Edit Question',
            'click' :   function(e){
                get_ajax(window.localStorage.item1 + '?action=select&target=quest&id=' + quest['ID'], function (data) {
                    mv_quest(data);
                });
            }
        },
        {
            'icon' : 'fa fa-file-code-o',
            'id': quest['ID'],
            'action': 'Print',
            'text'  :   'Download TEX',
            'click' :   function(e){
                get_ajax(window.localStorage.item1 + '?action=select&target=quest&task=D&id=' + quest['ID'],function(data){
                    download(JSON.parse(data)['TEX'],'q'+quest['ID']+'.tex','latex');
                });
            }
        }
    ];
    
    shell[0].classList = 'display-none quest-shell-target';
    shell[0].innerHTML = qtxt;
    shell[0].setAttribute('data-id',quest['ID']);
    cell[0].id = 'cell_qtext_' + quest['ID'];
    cell[0].title = quest['QUESTION'];
    
    shell[1].innerHTML = quest['CREATED'];
    shell[1].classList = 'quest-shell';
    cell[1].title = 'dated created';
    cell[1].classList = 'cell-768 extra';
    
    shell[2].innerHTML= quest['QTYPE_DESC'];
    shell[2].classList = 'quest-shell';
    cell[2].classList = '';
    cell[2].title = 'type';
    
    let btn_del = [document.createElement('i'),document.createElement('i')];
    btn_del[0].title = 'delete question';
    btn_del[0].classList = 'fa fa-remove rm-quest';
    btn_del[0].id = 'btn_rm_quest_'+quest['ID'];
    btn_del[0].setAttribute('data-id',quest['ID']);
    btn_del[1].id = 'vss_rm_quest_'+quest['ID'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';
    btn_del[0].addEventListener('click', function (e) { rm_quest(1,quest['ID']);});
    
    shell[3].classList = 'quest-shell';
    if(quest['READY'] === '1'){
        shell[3].innerHTML = 'ready';
        cell[3].classList = 'note positive extra';
        cell[3].title = 'fit for duty';
    }else if(quest['READY'] === '0'){
        shell[3].innerHTML = 'unfit';
        cell[3].classList = 'note extra';
        cell[3].title = 'missing fields';
    }
    
    cell[4].classList = 'control';
    for(let i=0 ; i < btn_del.length;i++) cell[4].appendChild(btn_del[i]);
    
    let cols = cell.length;
    if(quest['USERNAME'] !== user_online.innerHTML) cols--;   //  -------------- only owners can remove quest
    let row = document.createElement('tr');
    row.id = 'row_quest_' + quest['ID'];
    row.classList = 'row-quest-ctrl';
    row.setAttribute('data-id',quest['ID']);
    row.oncontextmenu = function(e){
        let custom_menu = context_menu_items;
        if (quest['USERNAME'] === user_online.innerHTML) {

            if (quest['READY'] === '0')
                custom_menu = [custom_menu[1]];

            if (this.classList.contains('quest-active')) {
                switch (this.getAttribute('data-action')) {
                    case 'view':
                        custom_menu = [custom_menu[1], custom_menu[2]];
                        break;
                }
            }
            oncontextmenu(e, 'row-quest-ctrl', custom_menu);
        } else {
                custom_menu = [custom_menu[0], custom_menu[2]];
            if (this.classList.contains('quest-active')) {
                switch (this.getAttribute('data-action')) {
                    case 'view':
                        custom_menu = [custom_menu[1]];
                        break;
                }
            }
            oncontextmenu(e, 'row-quest-ctrl', custom_menu);
        }
    };
    if(typeof qtxt === 'string' && qtxt.trim().length > 0) {
        row.onclick = function(e){qtxt_preview(shell[0],(quest['USERNAME'] === user_online.innerHTML));};
        row.classList.add('cursor-pointer');
    }
    for (let k = 0; k < cols; k++) row.appendChild(cell[k]);
    tbl_ls_x1.appendChild(row);
}
function quest_select(ctrl,data){
    try {
        let quest = JSON.parse(data);
        if(quest['QUEST']['READY'] === '0')return;
        //  accumulate breadcrumb with current panel
        let aug = document.getElementsByClassName('crumb+');
        while (aug.length > 2) {
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        } // ensures 2nd slice on bread
        crumbplus(aug, 'quest preview','quest-prev');
        // change figure selection to active
        let cmp = document.getElementsByClassName('quest-active');
        for (let i = 0; i < cmp.length; i++) cmp[i].classList.remove('quest-active');
        ctrl.classList.add('quest-active');
        ctrl.setAttribute('data-action', 'view'); //  update context
        
        switch_panels(panel_quest_preview,panel_index2_ls);
        //  populate form
        let qtxt = quest['QUEST']['QUESTION'];
        if(qtxt.length > 48) qtxt = qtxt.substr(0,48) + '...';
        
        let qtype = {'true/false':'TRUE/FALSE','multichoice':'MULTIPLE CHOICE'};
        let tf = {'T':'TRUE','F':'FALSE'};
        
        //  reset table
        tbl_quest_preview.innerHTML = null;
        //  build question description cells
        let qdesc = [document.createElement('td'),document.createElement('td')];
        qdesc[1].colSpan = '2';
        qdesc[1].innerHTML = qtype[quest['QUEST']['QTYPE_DESC']];
        qdesc[1].classList = 'align-right';
        
        let trdesc = document.createElement('tr');
        for(let i = 0 ; i < qdesc.length ; i++) trdesc.appendChild(qdesc[i]);
        tbl_quest_preview.appendChild(trdesc);
        
        //  build figure cell
        let focusfig = PATH.value + '/main/usr/img/figure/'+quest['QUEST']['REPO']+'/';
        let img = document.createElement('img');
        img.alt = 'FIGURE';
        
        let f_td = document.createElement('td');
        f_td.colSpan = '3';
        
        let tr_img = document.createElement('tr');
        tr_img.appendChild(f_td);
        
        if(quest['FIG'].length < 1) {}
        else if(typeof quest['QUEST'] === 'object' && !Array.isArray(quest['QUEST'])){
             focusfig += quest['FIG']['FIGURE_ID']+'.png';
             img.src = focusfig;
             f_td.appendChild(img);
        }else{
            quest['FIG'][0]['FIGURE_ID']+'.png';
            img.src = focusfig;
            f_td.appendChild(img);
        }
        tbl_quest_preview.appendChild(tr_img);
        
        //  build question cell
        let q_td = document.createElement('td');
        q_td.colSpan = '3';
        q_td.innerHTML = qtxt;
        q_td.title = quest['QUEST']['QUESTION'];
        //  build question row
        let q_tr = document.createElement('tr');
        q_tr.appendChild(q_td);
        tbl_quest_preview.appendChild(q_tr);
        
        //  prepare icon to symbolize correct/incorrect answers
        let state = ['fa fa-times','fa fa-check'];
        
        for(let k = 0 ; k < quest['ANS'].length; k++){//  build answer cells
            let ansr = document.createElement('i');
            ansr.classList = state[parseInt(quest['ANS'][k]['CORRECT'])];
            
            let cell = [document.createElement('td'),document.createElement('td')];
            cell[0].appendChild(ansr);
            cell[0].classList = 'symbol';
            cell[1].colSpan = '2';
            cell[1].classList = 'answer';
            
            switch(quest['QUEST']['QTYPE_DESC']){
                case 'true/false':
                    cell[1].innerHTML = tf[quest['ANS'][k]['TEXT']];
                    break;
                case 'multichoice':
                    cell[1].innerHTML = quest['ANS'][k]['TEXT'];
                    break;
            }
            let tr = document.createElement('tr');
            for(let i = 0 ; i < cell.length ; i++) tr.appendChild(cell[i]);
            
            tbl_quest_preview.appendChild(tr);
        }
        //  present LaTeX
        pre_tex_out.innerHTML = quest['TEX'];
    } catch (ex) {
        content_err.innerHTML = data;
        console.log(ex);
    }
}
function mv_quest(data){
    purge_panels();
    panel_purge_quest('quest edit',2);
    
    let qtype = {'multichoice':'M1M','true/false':'T1S'};
    
    let quest = JSON.parse(data);
    window.sessionStorage.quest_id = quest['QUEST']['ID']; // keep ID of current to imput field
    window.sessionStorage.quest_type = qtype[quest['QUEST']['QTYPE_DESC']];
    quest_phase_2(quest['QUEST']['QTYPE_DESC']);
    
    if(quest['QUEST']['READY'] === '1'){
        btn_done_x1sec04.classList.remove('display-none'); // reveal 'done' button
        btn_next_x1sec04.classList.remove('display-none'); // reveal 'next' button
        nav_section_05.childNodes[1].classList.add('display-none');
        nav_section_05.setAttribute('curr-state','active');
    }else {
        btn_done_x1sec04.classList.add('display-none'); // conceal 'done' button
        btn_next_x1sec04.classList.add('display-none'); // conceal 'next' button
        nav_section_05.childNodes[1].classList.remove('display-none');
        nav_section_05.setAttribute('curr-state','na');
    }
    
    get_ajax(window.localStorage.item1 + '?action=ls&target=quest-lang&id='+quest['QUEST']['ID'], function (data) {
                    let qtypes = JSON.parse(data);
                    btn_next_x1sec02.classList.remove('display-none');
                    nav_section_04.childNodes[1].classList.add('display-none');
                    input_quest_09.innerHTML = null;
                    
                    if(qtypes.length < 1){}
                    else if(typeof qtypes === 'object' && !Array.isArray(qtypes))select_option(input_quest_09,{'VALUE':qtypes['LANG_ID'],'DISPLAY':qtypes['LANG_DESC']},'lang','EN');
                    else qtypes.forEach(function(data){select_option(input_quest_09,{'VALUE':data['LANG_ID'],'DISPLAY':data['LANG_DESC']},'lang','EN');});
                });
    get_ajax(window.localStorage.item1 + '?action=ls&target=quest-txt&id=' + quest['QUEST']['ID'],function(data){
        let rm = document.getElementsByClassName('ls-rm-quest-text');
        for (let i = 0; i < rm.length; i++) tbl_quest_text_ls.removeChild(rm[i]);
        let qt = JSON.parse(data);
        if (qt.length < 1) {
            table_data_empty(document.getElementById('tbl_quest_text_ls'),'add the <b>English</b> text at least','ls-rm-quest-text');
            btn_next_x1sec02.classList.add('display-none');
            nav_section_04.childNodes[1].classList.remove('display-none');
        }
        else if(typeof qt === 'object' && !Array.isArray(qt)) build_quest_txt_ls(qt['QUESTION_TEXT'],{'id':qt['LANG_ID'],'desc':qt['LANG_DESC']});
        else qt.forEach(function(data){build_quest_txt_ls(data['QUESTION_TEXT'],{'id':data['LANG_ID'],'desc':data['LANG_DESC']});});
    });
}

function new_quest(stage,target){
    if(typeof stage !== "number")  stage = 1;
    switch(stage){
        case 1:
            switch_panels(vss_add_x1item,btn_add_x1item);
            post_ajax(window.localStorage.item1,'action=create&target=quest&id=' + window.sessionStorage.repo_id + '&qtype=' + input_quest_07.value,function(reply){
                new_quest(2,reply);
            });
            break;
        case 2:
            if(is_int(target)){
                nav_section_01.classList.add('display-none');
                window.sessionStorage.quest_id = target; // keep ID of current to input field
                window.sessionStorage.quest_type = input_quest_07.value; // keep question type
                // mark relevant navigation key as active
		nav_section_01.classList.remove('active');	
		
                quest_phase_2(input_quest_07.value);
                get_ajax(window.localStorage.item1 + '?action=ls&target=quest-lang&id='+target, function (data) {
                    let qtypes = JSON.parse(data);
                    input_quest_09.innerHTML = null;
                    qtypes.forEach(function(data){select_option(input_quest_09,{'VALUE':data['LANG_ID'],'DISPLAY':data['LANG_DESC']},'lang','EN');});
                });
                get_ajax(window.localStorage.item1 + '?action=count&target=quest&id='+window.sessionStorage.repo_id,function(count){
                        osd_3.innerHTML = m_osd_3.innerHTML = count;
                    });
            }
            else {
                input_error_qtype.classList.remove('display-none');
                input_error_qtype.innerHTML = target;
            }
            switch_panels(btn_add_x1item,vss_add_x1item);
            break;
    }
}
function rm_quest(stage,id,state){
    switch(stage){
        case 1:
            switch_panels(document.getElementById('vss_rm_quest_'+id), document.getElementById('btn_rm_quest_'+id));  // replace 'delete' with spinning icon, to demonstrate progress
            post_ajax(window.localStorage.item1,'action=rm&target=quest&id='+id,function(reply){
                rm_quest(2,id,reply);
            });
            break;
        case 2:
            switch(state){
                case 'success':
                    tbl_ls_x1.removeChild(document.getElementById('row_quest_'+id));    // remove row from table
                    get_ajax(window.localStorage.item1 + '?action=count&target=quest&id='+window.sessionStorage.repo_id,function(count){
                        if(id === window.sessionStorage.quest_id) window.location = '.';
                        osd_3.innerHTML = m_osd_3.innerHTML = count;
                        if (count === '0') {
                            let cell = document.createElement('td');
                            cell.innerHTML = 'repo contains no questions';
                            cell.classList = 'align-center ls-empty';

                            let row = document.createElement('tr');
                            row.classList = 'ls-rm-quest';
                            row.appendChild(cell);
                            tbl_ls_x1.appendChild(row);
                        }
                    });
                    break;
                default:
                    content_err.innerHTML = state;
                    break;
            }
            break;
    }
}
function quest_phase_2(type){
    let qtd = document.getElementsByClassName('qtitle-dynamic');
    panel_quest_intro.classList.add('display-none');
    
    switch(type){
        case 'C1S':
            switch_panels(panel_x103,panel_x101);   // alternate intro panel with of the column-match section panel
            nav_section_03.childNodes[1].classList.add('display-none');	// activate
            switch_panels(nav_section_03,nav_section_02);
            nav_section_03.classList.add('active');
            break;
        case 'M1M':case 'multichoice':
            //  set section title
            for(let i = 0; i < qtd.length ;i++) qtd[i].innerHTML = 'Multiple Choice';
            
            switch_panels(panel_x102,panel_x101);   // alternate intro panel with of the mutiple-choice section panel
            nav_section_02.childNodes[1].classList.add('display-none');	// activate
            nav_section_02.classList.add('active');
            panel_quest_ls.classList.remove('display-none');
            tbl_quest_text_ls.innerHTML = null;
            table_data_empty(tbl_quest_text_ls,'add the <b>English</b> text at least','ls-rm-quest-text');
            nav_section_04.classList.remove('display-none');
            nav_section_05.classList.remove('display-none');
            sec_ansr_mc.classList.remove('display-none');   //  multiple choice answer text input
            input_ansr_text.value = input_ansr_14.value;
            tbl_quest_text_ls.classList.remove('display-none');
            
            break;
        case 'T1S':case 'true/false':
            //  set section title
            for(let i = 0; i < qtd.length ;i++) qtd[i].innerHTML = 'True/False';
            
            switch_panels(panel_x102,panel_x101);   // alternate intro panel with of the true/false section panel
            nav_section_02.childNodes[1].classList.add('display-none');	// activate
            nav_section_02.classList.add('active');
            panel_quest_ls.classList.remove('display-none');
            tbl_quest_text_ls.innerHTML = null;
            table_data_empty(tbl_quest_text_ls,'add the <b>English</b> text at least','ls-rm-quest-text');
            nav_section_04.classList.remove('display-none');
            nav_section_05.classList.remove('display-none');
            input_ansr_text.value = input_ansr_15.value;
            tbl_quest_text_ls.classList.remove('display-none');
            break;
    }
    input_ansr_text.setAttribute('data-action',type);
}
function quest_answrs(){
    panel_purge_quest(null,4);
    panel_quest_ls.classList.remove('display-none');
    let btn_nav = document.getElementsByClassName('btn_nav_section');
    for(let i = 1 ; i < btn_nav.length ; i+=3) btn_nav[i].classList.remove('display-none');
    btn_nav[1].childNodes[1].classList.add('display-none');   // shield all navigation keys
    
    panel_x104.classList.remove('display-none');   // reveal answers section panel
    tbl_quest_ansr_ls.classList.remove('display-none');
    get_ajax(window.localStorage.item1 + '?action=ls&target=quest-ansr&id='+window.sessionStorage.quest_id,function(data){
        //  clear table
        tbl_quest_ansr_ls.innerHTML = null;
        let ansr = JSON.parse(data);
        if(ansr.length < 1) table_data_empty(tbl_quest_ansr_ls,'add false and and correct answers','ls-rm-quest-ansr');
        else if(typeof ansr === 'object' && !Array.isArray(ansr)) build_quest_ansr_ls(ansr); 
        else ansr.forEach(build_quest_ansr_ls);
    });
    
    switch(window.sessionStorage.quest_type){
        case 'M1M':
            sec_ansr_mc.classList.remove('display-none');
            sec_ansr_sate.classList.remove('display-none');
            let correct = document.getElementsByName('ansr-state');
            for(let i = 0 ; i < correct.length ; i++) correct[i].checked = false;
            break;
        case 'T1S':
            sec_ansr_tf.classList.remove('display-none');
            sec_misconcept.classList.remove('display-none');
            break;
    }
}
function quest_figure(){
    panel_purge_quest(null,5);
    let btn_nav = document.getElementsByClassName('btn_nav_section');
    for(let i = 1 ; i < btn_nav.length ; i+=3) btn_nav[i].classList.remove('display-none');
    btn_nav[1].childNodes[1].classList.add('display-none');   // shield all navigation keys
    nav_section_04.classList.remove('display-none');
    nav_section_04.childNodes[1].classList.add('display-none');
    
    panel_x105.classList.remove('display-none');   // reveal figures section panel
    panel_quest_fig.classList.remove('display-none');
    //  assign default display image
    img_quest_fig.src = PATH.value +'/main/usr/img/sys/qb-ink.png';
    
    get_ajax(window.localStorage.item1 + '?action=ls&target=fig-types',function(data){
        let fig = JSON.parse(data);
        input_quest_16.innerHTML = null;
        select_option(input_quest_16,{'DISPLAY':'--','VALUE':'--'});
        if(typeof fig === 'object' && !Array.isArray(fig)) select_option(input_quest_16,{'DISPLAY':fig['FIGURE_TYPE'],'VALUE':fig['FIGURE_TYPE']}); 
        else fig.forEach(function(data){select_option(input_quest_16,{'DISPLAY':data['FIGURE_TYPE'],'VALUE':data['FIGURE_TYPE']});});
    });
    get_ajax(window.localStorage.item1 + '?action=ls&target=this-figs&id='+window.sessionStorage.repo_id,select_figure_ls);
}
function select_figure_ls(data){
    let fig = JSON.parse(data);
    input_quest_17.innerHTML = null;
    select_option(input_quest_17,{'VALUE': 0, 'DISPLAY': '--'},'figls');
    if (fig.length < 1) select_option(input_quest_17,{'VALUE': 0, 'DISPLAY': 'list empty'},'figls');
    else if (typeof fig === 'object' && !Array.isArray(fig)) select_option(input_quest_17,{'VALUE': fig['FIGURE_ID'], 'DISPLAY': fig['FIGURE_LABEL']},'figls');
    else fig.forEach(function(data){select_option(input_quest_17,{'VALUE': data['FIGURE_ID'], 'DISPLAY': data['FIGURE_LABEL']},'figls');});
    get_ajax(window.localStorage.item1 + '?action=ls&target=quest-fig&id='+window.sessionStorage.quest_id,quest_figure_ls);
}
function quest_figure_ls(data){
    let fig = JSON.parse(data);
    btn_done_x1sec05.classList.remove('display-none');
    tbl_quest_fig_ls.innerHTML = null;
    if (fig.length < 1) {
        let cell = document.createElement('td');
        cell.classList = 'align-center';
        cell.innerHTML = 'no figure assigned to question';

        let row = document.createElement('tr');
        row.classList = 'quest-fig-null';
        row.appendChild(cell);
        tbl_quest_fig_ls.appendChild(row);
        btn_done_x1sec05.classList.add('display-none');
    } else if (typeof fig === 'object' && !Array.isArray(fig)) build_quest_fig_ls(fig);
    else fig.forEach(build_quest_fig_ls);
}
function build_quest_fig_ls(fig){
    let cell = [document.createElement('td'), document.createElement('td'), document.createElement('td')];
    
    cell[0].innerHTML = fig['FIGURE_LABEL'];
    
    cell[1].innerHTML = fig['FIGURE_TYPE'];
    cell[1].classList = 'extra';
    
    let btn_del = [document.createElement('i'),document.createElement('i')];
    btn_del[0].title = 'remove figure';
    btn_del[0].classList = 'fa fa-remove';
    btn_del[0].id = 'btn_rm_quest_fig_'+fig['FIGURE_ID'];
    btn_del[1].id = 'vss_rm_quest_fig_'+fig['FIGURE_ID'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';
    btn_del[0].setAttribute('data-id',fig['FIGURE_ID']);
    btn_del[0].addEventListener('click', function (e) { unlink_quest_fig(1,fig['FIGURE_ID']);});
    
    cell[2].classList = 'control align-center';
    for(let i=0 ; i < btn_del.length;i++) cell[2].appendChild(btn_del[i]);
    
    let tr = document.createElement('tr');
    tr.onmouseover = function(e){
        img_quest_fig.setAttribute('src-data',img_quest_fig.src);
        img_quest_fig.src = PATH.value + '/main/usr/img/figure/'+fig['REPO_ID']+'/'+fig['FIGURE_ID'] + '.png';
    };
    tr.onmouseout = function(e){
        img_quest_fig.src = img_quest_fig.getAttribute('src-data');
        img_quest_fig.removeAttribute('src-data');
    };
    for(let i = 0 ; i < cell.length ; i++)  tr.appendChild(cell[i]);
    
    tbl_quest_fig_ls.appendChild(tr);
    input_quest_17.removeChild(document.getElementById('opt_figls_'+fig['FIGURE_ID']));
}
function unlink_quest_fig(stage,id,reply){
    if(typeof stage !== 'number') stage = 1;
    let btn = document.getElementById('btn_rm_quest_fig_'+id);
    let vss = document.getElementById('vss_rm_quest_fig_'+id);
    switch(stage){
        case 1:
            switch_panels(vss,btn);
            post_ajax(window.localStorage.item1,'action=update&target=quest-fig&dml=rm&id='+window.sessionStorage.quest_id+'&fid='+id,function(reply){
                unlink_quest_fig(2,id,reply);
            });
            break;
        case 2:
            switch(reply){
                case 'ereased':
                    get_ajax(window.localStorage.item1 + '?action=ls&target=this-figs&id='+window.sessionStorage.repo_id,select_figure_ls);
                    break;
                default:
                    input_error_qansr.innerHTML = reply;
                    break;
            }
            switch_panels(btn,vss);
            break;
    }
}

function qtxt_preview(cmp,owner){
    let aug = document.getElementsByClassName('crumb+');
    let id = cmp.getAttribute('data-id');
    switch(aug.length){
        case 2:
            tbl_ls_x1.setAttribute('quest-preview','1');
            
            //  conceal 'delete' keys
            let rm_btn = document.getElementsByClassName('rm-quest');
            for(let i = 0 ; i < rm_btn.length ; i++) rm_btn[i].classList.add('display-none');
            if(owner)document.getElementById('btn_rm_quest_'+id).classList.remove('display-none');
            //  conceal all targets
            let target = document.getElementsByClassName('quest-shell-target');
            for(let i = 0 ; i < target.length ; i++) target[i].classList.add('display-none');
            //  reveal target of interest
            cmp.classList.remove('display-none');

            let rm = document.getElementsByClassName('quest-shell');
            for(let i = 0 ; i < rm.length ; i++) rm[i].classList.add('display-none');
            break;
        case 3:
            get_ajax(window.localStorage.item1 + '?action=select&target=quest&id=' + id, function (data) {
                quest_select(document.getElementById('row_quest_' + id), data);
            });
            break;
    }
}
function qtxt_prev_out(cmp){
    if(cmp.classList.contains('quest-shell') || cmp.tagName === 'TD') return;
    tbl_ls_x1.setAttribute('quest-preview','0');
    //  conceal all targets
    let target = document.getElementsByClassName('quest-shell-target');
    for(let i = 0 ; i < target.length ; i++) target[i].classList.add('display-none');
    
    let rm = document.getElementsByClassName('quest-shell');
    for(let i = 0 ; i < rm.length ; i++) rm[i].classList.remove('display-none');
    //  reveal 'delete' keys
    let btn_rm_quest = document.getElementsByClassName('rm-quest');
    for(let i = 0 ; i < btn_rm_quest.length ; i++) btn_rm_quest[i].classList.remove('display-none');
//    console.log('quest-prev-out');
}

function add_quest_text(stage,state){
    if(typeof stage !== "number")  stage = 1;
    switch(stage){
        case 1:
            switch_panels(vss_save_x1sec02,btn_save_x1sec02);
            input_error_qtext.innerHTML = null;
            if(input_quest_10.value.trim().length < 1){
                err_field(input_quest_10,'field required');
                switch_panels(btn_save_x1sec02,vss_save_x1sec02);
                break;
            }
            post_ajax(window.localStorage.item1,'action=create&target=quest-text&qid=' + window.sessionStorage.quest_id + '&lid=' + input_quest_09.value + '&text=' + input_quest_10.value,function(reply){
                add_quest_text(2,reply);
            });
            break;
        case 2:
            switch (state) {
                case 'success':
                    // remove row encouraging user to add question text, if still intact
                    let rm = document.getElementsByClassName('ls-rm-quest-text');
                    for (let i = 0; i < rm.length; i++) tbl_quest_text_ls.removeChild(rm[i]);
                    //  get currently selected option
                    let lang_select = document.getElementById('opt_lang_' + input_quest_09.value);
                    //  add new language text to running list
                    build_quest_txt_ls(input_quest_10.value,{'id':input_quest_09.value,'desc':lang_select.innerHTML});   //  accumulate running list of quest text in table with a single row
                    //  remove option from 'select' element
                    
                    input_quest_09.removeChild(lang_select);
                    input_quest_10.value = null;    // clear question text input field
                    btn_next_x1sec02.classList.remove('display-none');  // reveal button allow users to move to the next step
                    nav_section_04.childNodes[1].classList.add('display-none');
                    break;
                default:
                    input_error_qtext.classList.remove('display-none');
                    input_error_qtext.innerHTML = state;
                    break;
            }
            switch_panels(btn_save_x1sec02,vss_save_x1sec02);
            break;
    }
}
function build_quest_txt_ls(quest_text,lang){
    let text = quest_text;
    if(text.length > 32) text = text.substr(0,32) + '... ';
    
    let  tr = document.createElement('tr');
    tr.id = 'tr_qtext_' + lang['id'];
    
    let btn_del = [document.createElement('i'),document.createElement('i')];
    btn_del[0].title = 'remove text';
    btn_del[0].classList = 'fa fa-remove';
    btn_del[0].id = 'btn_rm_'+ lang['id'];
    btn_del[0].setAttribute('data-id',lang['id']);
    btn_del[1].id = 'vss_rm_' + lang['id'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';
    btn_del[0].addEventListener('click', function (e) { rm_quest_text(1,lang['id']);});
    
    let cell = [document.createElement('td'),document.createElement('td'),document.createElement('td')];
    cell[0].innerHTML = lang['id'];
    cell[0].title = lang['desc'];
    cell[0].classList = 'extra';
    cell[1].innerHTML = text;
    cell[1].title = quest_text;
    
    cell[2].classList = 'control';
    for(let i = 0; i < btn_del.length ; i++) cell[2].appendChild(btn_del[i]);
    
    
    for(let i= 0 ; i < cell.length; i++) tr.appendChild(cell[i]);
    tbl_quest_text_ls.appendChild(tr);
    
    if(input_quest_09.children.length < 1) btn_save_x1sec02.disabled = true;
}
function rm_quest_text(stage,id,state){
    if(typeof stage !== "number")  stage = 1;
    let btn = document.getElementById('btn_rm_'+id);
    let vss = document.getElementById('vss_rm_'+id);
    switch(stage){
        case 1:
            switch_panels(vss,btn);
            post_ajax(window.localStorage.item1,'action=rm&target=quest-text&qid='+window.sessionStorage.quest_id+'&lid='+id,function(reply){
                rm_quest_text(2,id,reply);
            });
            break;
        case 2:
            try{
                let data = JSON.parse(state);
                let opt = {'VALUE':data['LANG_ID'],'DISPLAY':data['LANG_DESC']};
                select_option(input_quest_09,opt,'lang','EN'); // return option back to combobox
                btn_save_x1sec02.disabled = false;   //  activate 'save' button
                tbl_quest_text_ls.removeChild(document.getElementById('tr_qtext_'+id)); // remove text from running list
                if(tbl_quest_text_ls.children.length < 1) { // if running lis is empty
                    table_data_empty(tbl_quest_text_ls,'add the <b>English</b> text at least','ls-rm-quest-text'); // return text encouring user to add question text
                    btn_next_x1sec02.classList.add('display-none'); // conceal 'done' button
                    nav_section_04.childNodes[1].classList.remove('display-none');
                }
            }catch(ex){
                input_error_qtext.innerHTML = state;
            }
            switch_panels(btn,vss);
            break;
    }
}

function add_quest_ansr(stage,state){
    if(typeof stage !== "number")  stage = 1;
    switch(stage){
        case 1:
            switch_panels(vss_add_x1sec04,btn_save_x1sec04);
            input_error_qansr.innerHTML = null
            let valid = true;
            let tempval = 0;
            let sound = -1;
            switch(window.sessionStorage.quest_type){
                case 'M1M':
                    if(input_ansr_14.value.trim().length < 1) {
                        err_field(input_ansr_14, 'field required');
                        valid = false;
                    }
                    if(!radio_valid('ansr-state',err_field,'nothing selected')) valid = false;
                    tempval = input_ansr_14.value;
                    sound = radio_value('ansr-state');
                    break;
                case 'T1S':
                    tempval = input_ansr_15.value;
                    sound = '1';
                    break;
            }
            
            if(!valid){
                switch_panels(btn_save_x1sec04,vss_add_x1sec04);
                break;
            }
            post_ajax(window.localStorage.item1,
            'action=create&target=quest-ansr&id='+window.sessionStorage.quest_id+'&txt='+tempval+'&sound='+sound+'&misnomer='+input_ansr_13.value,function(reply){
                add_quest_ansr(2,reply);
            });
            break;
        case 2:
            try{
                let reply = JSON.parse(state);
                if(reply['REQUEST'] === 'success'){
                    let rm = document.getElementsByClassName('ls-rm-quest-ansr');
                    for (let i = 0; i < rm.length; i++) tbl_quest_ansr_ls.removeChild(rm[i]);
                                        
                     //  clear table
                    tbl_quest_ansr_ls.innerHTML = null;
                    if(reply['ANS'].length < 1) table_data_empty(tbl_quest_ansr_ls,'add false and and correct answers','ls-rm-quest-ansr');
                    else if(typeof reply['ANS'] === 'object' && !Array.isArray(reply['ANS'])) build_quest_ansr_ls(reply['ANS']);
                    else reply['ANS'].forEach(build_quest_ansr_ls);
        
                    if(reply['FIT'] === '1') {
                        btn_done_x1sec04.classList.remove('display-none'); // reveal 'done' button
                        btn_next_x1sec04.classList.remove('display-none'); // reveal 'next' button
                        nav_section_05.childNodes[1].classList.add('display-none');
                    }
                    input_ansr_13.value = null;
                }else{
                    input_error_qansr.classList.remove('display-none');
                    input_error_qansr.innerHTML = reply['STATE'];
                }
            }catch(ex){
                input_error_qansr.innerHTML = state;
            }
            switch_panels(btn_save_x1sec04,vss_add_x1sec04);
            break;
    }
}
function build_quest_ansr_ls(answr){
    let text = answr['TEXT'];
    let text_complete = answr['TEXT'];
    let sound = answr['CORRECT'];
    let ansr = {'1':'fa-thumbs-up','0':'fa-thumbs-down'};
    let misnomer = {'1':'','0':answr['MISCONCEPTION']};
    let tf = {'T':'TRUE','F':'FALSE'};
    
    switch(answr['QTYPE_ID']){
        case 'M1M':
            if(text.length > 32) text = text.substr(0,32) + '... ';
            input_ansr_text.value = input_ansr_14.value = null;
            break;
        case 'T1S':
            text = tf[answr['TEXT']];
            input_ansr_text.value = input_ansr_15.value;
            break;
    }
    let  tr = document.createElement('tr');
    tr.id = 'tr_qansr_' + answr['ANS_ID'];
    
    let btn_del = [document.createElement('i'),document.createElement('i')];
    btn_del[0].title = 'remove answer';
    btn_del[0].classList = 'fa fa-remove';
    btn_del[0].id = 'btn_rm_ans_'+ answr['ANS_ID'];
    btn_del[0].setAttribute('data-id',answr['ANS_ID']);
    btn_del[1].id = 'vss_rm_ans_' + answr['ANS_ID'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';
    btn_del[0].addEventListener('click', function (e) { rm_quest_ansr(1,answr['ANS_ID']);});
    
    let cell = [document.createElement('td'),document.createElement('td'),document.createElement('td')];
    let ans_icon = document.createElement('i');
    ans_icon.classList = 'fa ' + ansr[sound];
    cell[0].appendChild(ans_icon);
    cell[0].title = misnomer[sound];
    cell[0].classList = 'extra';
    cell[1].innerHTML = text;
    cell[1].title = text_complete;
    
    
    if(answr['QTYPE_ID'] === 'T1S') cell.splice(2);
    else {
        cell[2].classList = 'control';
        for(let i = 0; i < btn_del.length ; i++) cell[2].appendChild(btn_del[i]);
    }
    
    for(let i= 0 ; i < cell.length; i++) tr.appendChild(cell[i]);
    tbl_quest_ansr_ls.appendChild(tr);
}
function rm_quest_ansr(stage,id,state){
    if(typeof stage !== "number")  stage = 1;
    let btn = document.getElementById('btn_rm_ans_'+id);
    let vss = document.getElementById('vss_rm_ans_'+id);
    switch(stage){
        case 1:
            switch_panels(vss,btn);
            post_ajax(window.localStorage.item1,'action=rm&target=quest-ansr&id='+id,function(reply){
                rm_quest_ansr(2,id,reply);
            });
            break;
        case 2:
            try {
                let reply = JSON.parse(state);
                switch (reply['REQUEST']) {
                    case 'success':
                        tbl_quest_ansr_ls.removeChild(document.getElementById('tr_qansr_' + id));
                        if (tbl_quest_ansr_ls.children.length < 1) { // if running lis is empty
                            table_data_empty(tbl_quest_ansr_ls,'add false and and correct answers','ls-rm-quest-ansr'); // return text encouring user to add question text
                            btn_done_x1sec04.classList.add('display-none'); // conceal 'done' button
                            btn_next_x1sec04.classList.add('display-none'); // conceal 'next' button
                            nav_section_05.childNodes[1].classList.remove('display-none');
                            break;
                        }
                        if(reply['STATE'] === '0'){
                            btn_done_x1sec04.classList.add('display-none'); // conceal 'done' button
                            btn_next_x1sec04.classList.add('display-none'); // conceal 'next' button
                            nav_section_05.childNodes[1].classList.remove('display-none');
                        }
                        break;
                    default:
                        input_error_qansr.innerHTML = reply['REQUEST'];
                        break;
                }
            } catch (ex) {
                input_error_qansr.innerHTML = state;
                console.log(ex);
            }

            switch_panels(btn, vss);
            break;
    }
}
function misconcept(cmp){
    err_field(cmp);
    switch(cmp.value){
        case '1':
            sec_misconcept.classList.add('display-none');
            break;
        case '0':
            sec_misconcept.classList.remove('display-none');
            break;
    }
}
//</editor-fold>
//<editor-fold desc="figure" defaultstate="collapsed">
function build_fig_ls(fig) {
    let context_menu_items = [
        {
            'icon' : 'fa fa-eye',
            'id': fig['FIGURE_ID'],
            'action': 'View',
            'text'  :   'View Figure',
            'click' :   function(e){
                figure_select(1,document.getElementById('fig_' + fig['FIGURE_ID']));
            }
        },
        {
            'icon' : 'fa fa-edit',
            'id': fig['FIGURE_ID'],
            'action': 'Edit',
            'text'  :   'Edit Figure',
            'click' :   function(e){
                mv_figure(1,document.getElementById('fig_' + fig['FIGURE_ID']));
            }
        }
    ];
    
    let cell = [document.createElement('td'), document.createElement('td'), document.createElement('td')];
    let shell = [document.createElement('span'),document.createElement('span')];
    for(let i = 0 ; i < shell.length ; i++) cell[i].appendChild(shell[i]);
    
    shell[0].innerHTML = fig['FIGURE_LABEL'];
    cell[0].title = 'click to preview';
    cell[0].id = 'fig_' + fig['FIGURE_ID'];
    cell[0].classList = 'cell-fig-ctrl';
    cell[0].setAttribute('data-id',fig['FIGURE_ID']);
    cell[0].setAttribute('data-action','view');
    cell[0].onclick = function (e) {figure_select(1,this);};
    cell[0].oncontextmenu = function(e){
        if(this.parentNode.classList.contains('fig-active')) {
            switch(this.getAttribute('data-action')){
                case 'view':if(fig['USERNAME'] === user_online.innerHTML)oncontextmenu(e,'cell-fig-ctrl',[context_menu_items[1]]);break;
                case 'edit':oncontextmenu(e,'cell-fig-ctrl',[context_menu_items[0]]);break;
            }
        }
        else {
            if(fig['USERNAME'] === user_online.innerHTML) oncontextmenu(e,'cell-fig-ctrl',context_menu_items);
            else oncontextmenu(e,'cell-fig-ctrl',[context_menu_items[0]]);
        }
    };
    
    shell[1].innerHTML = fig['FIGURE_TYPE'];
    cell[1].id = 'fig_type_' + fig['FIGURE_ID'];
    cell[1].classList = 'extra cell-fig-ctrl';
    
    let btn_del = [document.createElement('i'),document.createElement('i')];
    btn_del[0].title = 'delete figure';
    btn_del[0].classList = 'fa fa-remove';
    btn_del[0].id = 'btn_rm_fig_'+fig['FIGURE_ID'];
    btn_del[1].id = 'vss_rm_fig_'+fig['FIGURE_ID'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';
    btn_del[0].setAttribute('data-id',fig['FIGURE_ID']);
    btn_del[0].addEventListener('click', function (e) { rm_figure(1,fig['FIGURE_ID']);});
    
    cell[2].classList = 'control align-center';
    for(let i=0 ; i < btn_del.length;i++) cell[2].appendChild(btn_del[i]);
    
    let cols = cell.length;
    if(fig['USERNAME'] !== user_online.innerHTML) cols--;   //  ------------------------------------------- only admin can remove members
    let row = document.createElement('tr');
    row.id = 'row_fig_' + fig['FIGURE_ID']; 
    row.classList = 'tr-fig';
    for (let k = 0; k < cols; k++) row.appendChild(cell[k]);
    tbl_ls_x2.appendChild(row);
}
function x2_add(){
    purge_panels();
    //	conceal this key
    link_addx2.classList.add('display-none');
    btn_addx2.classList.add('display-none');
    //  reset rows
    let tr = document.getElementsByClassName('tr-fig');
    for (let i = 0; i < tr.length; i++) {
        tr[i].firstChild.setAttribute('data-action', 'edit');
        tr[i].firstChild.onclick = function (e) {mv_figure(1, this);};
        tr[i].firstChild.title = 'click to update';
        tr[i].classList.remove('fig-active');
    }
    // clear input form
    input_figure_03.value = input_figure_4.value = input_figure_06.value = null;
    panel_index2_dml.classList.remove('display-none');
    list_panel.classList.add('big-1280');

    wrap_btn_back.classList.remove('display-none');	// show back button
    instance_ls_shield.classList.remove('display-none');
    

    // navigation breadcrumb
    let aug = document.getElementsByClassName('crumb+');
    while (aug.length > 2) {
        m_nav_crumb.removeChild(m_nav_crumb.lastChild);
        bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
    } // ensures 3rd slice on bread
    crumbplus(aug, 'new figure', 'new figure');
    // panel listing current figures
    panel_index2_ls.classList.remove('display-none');
    panel_index2_ls.classList.add('big-cell');
    
    btn_save_figure.onclick = new_figure;
}
function figure_select(stage,ctrl,data){
    switch(stage){
        case 1:
            //  reset controls
            x2_update_success.classList.add('display-none');
            
            ls_quest_vss.parentNode.classList.add('display-flex');
            switch_panels(fig_preview, tbl_ls_x1);
            switch_panels(ls_quest_vss,fig_preview);
            //  reset rows
            let tr = document.getElementsByClassName('tr-fig');
            for (let i = 0; i < tr.length; i++) {
                tr[i].firstChild.setAttribute('data-action', 'view');
                tr[i].firstChild.onclick = function (e) {figure_select(1,this);};
                tr[i].firstChild.title = 'click to preview';
            }
            // change figure selection to active
            let cmp = document.getElementsByClassName('fig-active');
            for (let i = 0; i < cmp.length; i++) cmp[i].classList.remove('fig-active');
            ctrl.parentNode.classList.add('fig-active');
            ctrl.title = '';
            ctrl.onclick = null;
            
            switch_panels(panel_default, panel_index2_dml);
            switch_panels(ls_quest, instance_info);

            //  reveal key to add figure
            link_addx2.classList.remove('display-none');
            btn_addx2.classList.remove('display-none');
            get_ajax(window.localStorage.item1 + '?action=select&target=figure&id='+ctrl.getAttribute('data-id'),function(data){
                figure_select(2,ctrl,data);
            });
            break;
        case 2:
            let fig = JSON.parse(data);
            fig_preview.src = '../main/usr/img/figure/' + fig['REPO_ID'] + '/' + fig['FIGURE_ID'] + '.png';
            //  accumulate breadcrumb with current panel
            let aug = document.getElementsByClassName('crumb+');
            while (aug.length > 2) {
                m_nav_crumb.removeChild(m_nav_crumb.lastChild);
                bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
            } // ensures 2nd slice on bread
            crumbplus(aug, 'figure preview');
            instance_ls_shield.classList.remove('display-none');   // shield protecting against selecting another instance
            ls_quest_vss.parentNode.classList.remove('display-flex');
            switch_panels(fig_preview,ls_quest_vss);
            break;
    }
}
function mv_figure(stage,ctrl,data){
    switch (stage) {
        case 1:
            //  reset controls
            x2_update_success.classList.add('display-none');
//            btn_save_figure.innerHTML = 'save';
            //  reset rows
            let tr = document.getElementsByClassName('tr-fig');
            for (let i = 0; i < tr.length; i++) {
                tr[i].firstChild.setAttribute('data-action', 'edit');
                tr[i].firstChild.onclick = function (e) {mv_figure(1,this);};
                tr[i].firstChild.title = 'click to update';
            }
            // change figure selection to active
            let cmp = document.getElementsByClassName('fig-active');
            for (let i = 0; i < cmp.length; i++)    cmp[i].classList.remove('fig-active');
            ctrl.parentNode.classList.add('fig-active');
            ctrl.title = '';
            ctrl.onclick = null;
            get_ajax(window.localStorage.item1 + '?action=select&target=figure&id='+ctrl.getAttribute('data-id'),function(data){
                mv_figure(2,ctrl,data);
            });
            break;
        case 2:
            let fig = JSON.parse(data);
            // populate input form
            switch_panels(panel_index2_dml, panel_default);
            input_figure_03.value = fig['FIGURE_LABEL'];
            input_figure_4.value = fig['DESCRIPTION'];
            input_figure_05.setAttribute('data-id',fig['FIGURE_ID']);
            input_figure_06.value = fig['FIGURE_TYPE'];
            // conceal key to add figure
            link_addx2.classList.add('display-none');
            btn_addx2.classList.add('display-none');
            //  accumulate breadcrumb with current panel
            let aug = document.getElementsByClassName('crumb+');
            while (aug.length > 2) {
                m_nav_crumb.removeChild(m_nav_crumb.lastChild);
                bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
            } // ensures 2nd slice on bread
            crumbplus(aug, 'figure update','update figure');
            btn_save_figure.onclick = function(e){update_figure(1,fig['FIGURE_ID']);};    // switch save button to update figure
            instance_ls_shield.classList.remove('display-none');   // shield protecting against selecting another instance
            break;
    }
    
    
    
}
function rm_figure(stage,id,state){
    switch(stage){
        case 1:
            switch_panels(document.getElementById('vss_rm_fig_'+id), document.getElementById('btn_rm_fig_'+id));  // replace 'delete' with spinning icon, to demonstrate progress
            post_ajax(window.localStorage.item1,'action=rm&target=figure&id='+id,function(reply){
                rm_figure(2,id,reply);
            });
            break;
        case 2:
            switch(state){
                case 'success':
                    tbl_ls_x2.removeChild(document.getElementById('row_fig_'+id));
                    get_ajax(window.localStorage.item1 + '?action=count&target=figure&id='+window.sessionStorage.repo_id,function(count){
                        osd_2.innerHTML = m_osd_2.innerHTML = count;
                        if (count === '0') {
                            let cell = document.createElement('td');
                            cell.innerHTML = 'repo contains no figures';
                            cell.classList = 'align-center ls-empty';

                            let row = document.createElement('tr');
                            row.classList = 'ls-rm-fig';
                            row.appendChild(cell);
                            tbl_ls_x2.appendChild(row);
                        }
                    });
                    break;
                default:
                    input_error_figure.classList.remove('display-none');
                    input_error_figure.innerHTML = state;
                    break;
            }
            
            break;
    }
}
function new_figure(stage, target){
    if(typeof stage !== "number")  stage = 1;
    switch(stage){
        case 1:	//------------------------------------------------------------------ VALIDATION
            switch_panels(x2_update_vss,btn_save_figure);
            var valid = true;
            if (!validate_field_3(input_figure_03)) valid = false;
            if (!validate_field_5(input_figure_05)) valid = false;
            if (!validate_field_6(input_figure_06)) valid = false;
            
            if (!valid) {
                switch_panels(btn_save_figure,x2_update_vss);
                break;
            }
            if(!panel_crop_space.classList.contains('display-none')){   // if use selected image,but forgort to finalize crop process
                figure_crop(null, function () {
                    post_ajax(window.localStorage.item1, 
                    'action=create&target=figure&id=' + window.sessionStorage.repo_id + '&label=' + input_figure_03.value + '&desc=' + input_figure_4.value + '&type=' + input_figure_06.value, 
                    function (data) {
                        new_figure(2, data);
                    });
                });
                break;
            }
            post_ajax(window.localStorage.item1,
            'action=create&target=figure&id='+window.sessionStorage.repo_id+'&label='+input_figure_03.value+'&desc='+input_figure_4.value+'&type='+input_figure_06.value,
            function(data){
                new_figure(2,data);
            });
            break;
        case 2:
            switch_panels(btn_save_figure, x2_update_vss);
            var figure;
            try {
                figure = JSON.parse(target);
            } catch (ex) {
                input_error_figure.classList.remove('display-none');
                input_error_figure.innerHTML = ex.message;
                break;
            }
            switch(figure['REPORT']){
                case 'success':
                    input_figure_03.value = input_figure_4.value = input_figure_05.value = input_figure_06.value = null;
                    switch_panels(panel_index2_ls,panel_img);
                    let rm = document.getElementsByClassName('ls-rm-fig');
                    for(let i = 0 ; i < rm.length; i++) tbl_ls_x2.removeChild(rm[i]);
                    
                    build_fig_ls(figure,'1');
                    get_ajax(window.localStorage.item1 + '?action=count&target=figure&id='+window.sessionStorage.repo_id,function(count){
                        osd_2.innerHTML = m_osd_2.innerHTML = count;
                    });
                    get_ajax(window.localStorage.item1 + '?action=ls&target=fig-types',update_fig_type_ls);
                    break;
                default:
                    input_error_figure.classList.remove('display-none');
                    input_error_figure.innerHTML= figure['REPORT'];
                    break;
            }
            
            break;
    }
}
function update_figure(stage,target,reply){
    if(typeof stage !== "number")  stage = 1;
    switch(stage){
        case 1:
            switch_panels(x2_update_vss,btn_save_figure);
            var valid = true;
            if (!validate_field_3(input_figure_03)) valid = false;
            if (!validate_field_6(input_figure_06)) valid = false;
            
            if (!valid) {
                switch_panels(btn_save_figure,x2_update_vss);
                break;
            }
            post_ajax(window.localStorage.item1,
            'action=update&target=figure&id='+target+'&label='+input_figure_03.value+'&desc='+input_figure_4.value+'&type='+input_figure_06.value,
            function(data){
                update_figure(2,target,data);
            });
            break;
        case 2:
            switch(reply){
                case 'success':
                    x2_update_success.classList.remove('display-none');
//                    btn_save_figure.innerHTML = 'saved!';
                    document.getElementById('fig_type_'+target).innerHTML = input_figure_06.value;
                    break;
                default :
                    input_error_figure.classList.remove('display-none');
                    input_error_figure.innerHTML= reply;
                    break;
            }
            switch_panels(btn_save_figure,x2_update_vss);
            break;
    }
}
function update_fig_type_ls(data) {
    x2_type_ls.innerHTML = null;
    let ls = JSON.parse(data);
    for (let i = 0; i < ls.length; i++) {
        let opt = document.createElement('option');
        opt.value = ls[i]['FIGURE_TYPE'];
        x2_type_ls.appendChild(opt);
    }
}
//</editor-fold>
//<editor-fold desc="navigation" defaultstate="collapsed">
function purge_panels(boogeyman) {
	sideline('000',false);	// activate * buttons on sideline
	if(typeof boogeyman !== "string" || boogeyman.trim().length < 1) boogeyman = 'display-none';
	//  -------------------------------------- remove Instance Select panel
	panel_default.classList.add(boogeyman);
        ls_quest.classList.add(boogeyman);
	//	------------------------------------- remove Instance Update panel
	panel_dml.classList.add(boogeyman);
        panel_index1_dml.classList.add(boogeyman);
	panel_index2_dml.classList.add(boogeyman);
	panel_index2_ls.classList.add(boogeyman);
        panel_quest_preview.classList.add(boogeyman);
        x2_update_success.classList.add(boogeyman);
        panel_quest_preview.classList.add(boogeyman);
//	instance_ls_shield.classList.add(boogeyman);
//	panel_crop_space.classList.add(boogeyman);	// hide instance icon crop panel
//	//  -------------------------------------------- add Instance Buttom ctrl keys
//	var buttom_ctrl = document.getElementsByClassName('link_addx');
//	for (var i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.remove(boogeyman);
//
//	wrap_btn_back.classList.add(boogeyman);	// hide back button
//	panel_img.classList.add(boogeyman);	// hide instance icon panel
}
function panel_purge_quest(action,section,boogeyman){
    if(typeof section !== 'number') section = 1;
    if(typeof boogeyman !== 'string' || boogeyman.trim().length < 1) boogeyman = 'display-none';
    
    //  clear question type error display
    input_error_qtype.innerHTML = input_error_qtext.innerHTML = null;
    //  reveal status quo
    panel_index1_dml.classList.remove('display-none');
    //  conceal sidebar panels
    panel_quest_intro.classList.add('display-none');
    panel_quest_ls.classList.add('display-none');
    panel_quest_fig.classList.add('display-none');
    //	conceal add quest key
    link_addx1.classList.add('display-none');
    btn_addx1.classList.add('display-none');
    
    let quest_sec = document.getElementsByClassName('panel_instance_section');
    let btn_nav = document.getElementsByClassName('btn_nav_section');
    let tbl_ls = document.getElementsByClassName('tbl-quest');
    
    
    //  conceal list tables
    for(let i = 0 ; i < tbl_ls.length ; i++) tbl_ls[i].classList.add('display-none');
    
    for(let i = 0 ; i < quest_sec.length; i++) {
        quest_sec[i].classList.add(boogeyman);  //  conceal context panel
        btn_nav[i].classList.remove('active');  //  flag all navigation as not active
        btn_nav[i].classList.add(boogeyman);    //  conceal allnavigation keys
        if(btn_nav[i].getAttribute('curr-state')=== 'na') btn_nav[i].childNodes[1].classList.remove(boogeyman);   // shield all navigation keys
    }
    
    //  hide answer section panels
    sec_ansr_mc.classList.add(boogeyman);
    sec_ansr_tf.classList.add(boogeyman);
    sec_ansr_sate.classList.add(boogeyman);
    sec_misconcept.classList.add(boogeyman);
    
    
    //  prevent users from being able to switch between panels
    instance_ls_shield.classList.remove(boogeyman);
    
    document.getElementById('nav_section_0'+section).classList.remove(boogeyman);
    document.getElementById('nav_section_0'+section).classList.add('active'); //  present first dection tab as active
    document.getElementById('nav_section_0'+section).childNodes[1].classList.add(boogeyman);
    // navigation breadcrumb
    if(action !== null){
        let aug = document.getElementsByClassName('crumb+');
        while (aug.length > 2) {
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        } // ensures 3rd slice on bread
        crumbplus(aug, action);
    }
    //  clear * error messages
    let errmsg = document.getElementsByClassName('err-msg');
    for(let i=0;i<errmsg.length;i++) errmsg[i].innerHTML = null;
}
function m_crumb_nav() {
    let crumb = document.getElementsByClassName('crumb+');
    let cmp = document.getElementsByClassName('instance-active');
	
    switch (crumb.length) {
        case 2:
            window.sessionStorage.removeItem('repo_id');
            // change instance selection to active
            for (i = 0; i < cmp.length; i++) cmp[i].classList.remove('instance-active');
            // reset mobile keys
            m_btn_dml.classList.remove('shift');
            m_panel_dml.classList.remove('shift');
            m_btn_back_to_ls.classList.add('display-none');
            // switch panels
            switch_panels(list_panel,panel_content, 'x768');
            // return initial page status
            instance_h1.innerHTML = 'No Repository Selected';
            instance_p1.innerHTML = 'Please select Repository to your left.';
            instance_p2.innerHTML = 'Or use large cross to add one.';
            instance_err.innerHTML = null;
            //  hide instance manipulation bottom keys
            let buttom_ctrl = document.getElementsByClassName('link_addx');
            for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.add('display-none');
            osd_3.innerHTML = osd_2.innerHTML = m_osd_3.innerHTML = m_osd_2.innerHTML = 'null';
            instance_info.classList.remove('display-none');
            sideline('100', true);
            _btnback.classList.add('display-none');
            wrap_btn_back.classList.add('display-none');	// conceal back button
            tbl_ls_x2.innerHTML = tbl_ls_x1.innerHTML = null;   //  clear tables
            ls_quest.classList.add('display-none');
            panel_index2_ls.classList.add('display-none');
            list_panel.classList.remove('x1024');
            
            break;
        case 3:
            // remove none default panels
            purge_panels(); // *
            let quest_dml = document.getElementsByClassName('quest-dml');
            for(let i = 0 ; i < quest_dml.length ; i++) quest_dml[i].classList.add('display-none');
            edit_instance(false);   // edit instance
            instance_ls_shield.classList.add('display-none');   // shield protecting against selecting another instance
            panel_default.classList.remove('display-none');// return default panel
            get_ajax(window.localStorage.item1 + '?action=select&target=this&id='+window.sessionStorage.repo_id,function(data){
                m_nav_crumb.removeChild(m_nav_crumb.lastChild);
                bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
                instance_select(cmp[0],JSON.parse(data));
            });
            instance_icon_display.classList.add('dim');
            input_instance_02.value = input_figure_05.value = null;
            fig_preview.src = '../main/usr/img/sys/logo.png';
            switch_panels(tbl_ls_x1,fig_preview);
        break;
        case 4:
            switch_panels(panel_dml, panel_crop_space, 'big-cell');
            switch(l2_slice.firstChild.innerHTML){
                case 'edit':
                    switch_panels(panel_dml,panel_crop_space, 'big-cell');
                    panel_img.classList.add('big-cell');
                    input_instance_02.value = null;
                    switch_panels(panel_img, panel_crop_space);
                    break;
                case 'new figure':
                    get_ajax(window.localStorage.item1 + '?action=select&target=temp-figure&id=' + window.sessionStorage.repo_id, function(reply){
                        if(reply !== 'no-file') switch_panels(panel_img,panel_crop_space);
                        else {
                            switch_panels(panel_index2_ls,panel_crop_space);
                            input_figure_05.value = null;
                        }
                    });
                    break;
                case 'figure update':
                    switch_panels(panel_index2_ls,panel_crop_space);
                    break;
            }
            break;
    }
    m_nav_crumb.removeChild(m_nav_crumb.lastChild);
    bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
}
function quest_preview_nav(cmp){
    if(cmp.classList.contains('active'))return;
    //  switch selection
    let key = document.getElementsByClassName('quest-preview-nav');
    for(let i = 0 ; i < key.length ; i++) key[i].classList.remove('active');
    cmp.classList.add('active');
    switch(cmp.id){
        case 'sec_quest_form':
            switch_panels(panel_quest_form,panel_quest_tex);
            break;
        case 'sec_quest_tex':
            switch_panels(panel_quest_tex,panel_quest_form);
            break;
    }
}
//</editor-fold>
//<editor-fold desc="image crop" defaultstate="collapsed">
function detatch_crop(rm, cropping) {
    modal_img.classList.remove('display-none');
    let crop = document.getElementById('crop_space');
    modal_crop_body.appendChild(crop);
    switch_panels(rm, panel_crop_space);
    modal_btn_close_crop.onclick = function (e) {
        panel_crop_space.appendChild(crop);
        modal_img.classList.add('display-none');
        switch_panels(panel_crop_space, rm);
    };
    modal_btn_crop.onclick = cropping;
}
function instance_crop(ev){
    switch_panels(btn_crop_vss,btn_crop);
    switch_panels(img_vss,modal_btn_crop);
    c.result('canvas').then(function (canvas) {
        post_ajax(window.localStorage.item1, 'action=update&target=repo-icon&id=' + window.sessionStorage.repo_id + '&canvas=' + canvas, function (output) {
            //  return crop buttons from spinning progress icon
            switch_panels(btn_crop, btn_crop_vss);
            switch_panels(modal_btn_crop, img_vss);
            switch (output) {
                case 'success':
                    document.getElementById('img_' + window.sessionStorage.repo_id).src = instance_icon_display.src = canvas;
                    modal_img.classList.add('display-none');
                    
                    //  controls to remove icon
                    rm_instance_icon.value = window.sessionStorage.repo_id;
                    input_instance_02.value = null;
                    item_icon_ctrl.classList.remove('display-none');
                    m_btn_view_icon.classList.remove('display-none');
                    //  back one level up
                    m_crumb_nav();
                    switch_panels(panel_img,panel_crop_space);
                    discard_croppie(c);
                    break;
                default:
                    err_field(input_instance_02, output);
                    break;
            }

        });
    }).catch(function (ex) {
        input_error_instance.classList.remove('display-none');
        input_error_instance.innerHTML = ex.message;
        window.alert(ex.message);
    });
}
function figure_crop(ev,callback){
    switch_panels(btn_crop_vss, btn_crop);
    switch_panels(img_vss, modal_btn_crop);
    let params; // action to perform with image crop canvas
    
    switch (l2_slice.title) {
        case 'new figure':
            params = 'action=create&target=figure-img&id=' + window.sessionStorage.repo_id;
            break;
        case 'update figure':
            params = 'action=update&target=figure-img&id=' + input_figure_05.getAttribute('data-id');
            break;
    }
    c.result('canvas').then(function (canvas) {
        post_ajax(window.localStorage.item1, params  + '&canvas=' + canvas, function (output) {
            //  return crop buttons from spinning progress icon
            switch_panels(btn_crop, btn_crop_vss);
            switch_panels(modal_btn_crop, img_vss);
            switch(output){
                case 'success':
                    instance_icon_display.src = canvas; // use instance icon panerl to preview current figure in question
                    modal_img.classList.add('display-none');    // remove image crop modal
                    
                    switch_panels(panel_index2_dml, panel_crop_space, 'big-cell');  // on mobile devices
                    panel_index2_ls.classList.add('display-none');  // on * devices
                    instance_icon_display.classList.remove('dim');
                    //  controls to remove icon
                    rm_instance_icon.value = null;
                    item_icon_ctrl.classList.add('display-none');
                    m_btn_view_icon.classList.add('display-none');
                    //  back one level up
                    m_crumb_nav();
                    discard_croppie(c);
                    if(typeof callback === 'function')  callback();
                    break;
                default:
                    err_field(input_figure_05, output);
                    break;
            }
        });
    }).catch(function (ex) {
        input_error_figure.classList.remove('display-none');
        input_error_figure.innerHTML = ex.message;
        window.alert(ex.message);
    });
}
//</editor-fold>

//<editor-fold desc="VALIDATION" defaultstate="collapsed">
function err_focus_2(field, txt) {
    var popup = document.getElementById("popup_" + field.id);	//	create popup element to display error message
    popup.innerHTML = txt;	// displaying error message
    
    //	reseting elements
    popup.classList.remove("show");
    field.classList.remove('err');

    if (typeof txt === "string" && txt.length > 0) {
        popup.classList.add("show");
        field.classList.add('err');
    }
}
// ----------------------------------------------------------------------- repo
function validate_repo_name(target) {
    //	reset input field flag (format as normal)
    err_focus_2(target, null);
    //	confirm input text
    if (target.value.trim().length < 1) {
        err_focus_2(target, 'field empty');
        return false;
    }
    if (is_symbol(target.value)) {
        err_focus_2(target, 'symbol detected');	// check for non-alphabet symbols
        return false;
    }
    return true;
}
function validate_field_0(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'field empty');
		return false;
	}
	if(is_symbol(target.value)){
		err_field(target,'symbol detected');	// check for non-alphabet symbols
		return false;
	}
	return true;
}
// --------------------------------------------------------------------- figure
function validate_field_3(target){
	//	confirm input text
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'field empty');
		return false;
	}
	var txt = is_name_plus(target.value,2,20);	// verify correct naming conversion
	if(txt !== "success"){
		err_field(target,txt);
		return false;
	}
	if(is_symbol(target.value)){
		err_field(target,'symbol detected');	// check for non-alphabet symbols
		return false;
	}
	return true;
}
function validate_field_5(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'field empty');
		return false;
	}
	return true;
}
function validate_field_6(target){
	//	confirm input text
	if(target.value.trim().length < 1) return true;
	var txt = is_name_plus(target.value,2,20);	// verify correct naming conversion
	if(txt !== "success"){
		err_field(target,txt);
		return false;
	}
	if(is_symbol(target.value)){
		err_field(target,'symbol detected');	// check for non-alphabet symbols
		return false;
	}
	return true;
}
// ------------------------------------------------------------------- question
//</editor-fold>
//<editor-fold desc="Event Handling" defaultstate="collapsed">
document.body.onload = load;
document.body.addEventListener('keypress',function(e){
	switch(e.key){
		case 'F1':
		console.log('mayday!');
		break;
		case 'F4':
		case 'F8':
		case 'F9':
		test(e.key);
		break;
		default:
		// console.log(e.key);
		break;
	}
});
// ------------------------------------------------------------------------- sideline keys
btn_sl_mk.addEventListener('click',function(e){sideline_add(true);});
btn_sl_mv.addEventListener('click',function(e){edit_instance(true);});
btn_sl_rm.addEventListener('click',rm_repo);

btn_rm_repo_icon.addEventListener('click',function(e){
    switch_panels(vss_rm_repo_icon,btn_rm_repo_icon);
    post_ajax(window.localStorage.item1,'action=rm&target=this-icon&id='+window.sessionStorage.repo_id,function(reply){
        switch(reply){
            case 'success':
                window.location = '.';
                break;
            default:
                switch_panels(btn_rm_repo_icon,vss_rm_repo_icon);
                break;
        }
    });
});

btn_add_instance.addEventListener('click',function(e){new_repo(1,this);});
btn_update_instance.addEventListener('click',function(e){
    update_repo(1,this);
});
btn_back.addEventListener('click',m_crumb_nav);

btn_addx1.addEventListener('click',x1_add);
link_addx1.addEventListener('click',x1_add);
btn_addx2.addEventListener('click',x2_add);
link_addx2.addEventListener('click',x2_add);

btn_add_x1item.addEventListener('click',new_quest);
btn_save_x1sec02.addEventListener('click',add_quest_text);
btn_next_x1sec02.addEventListener('click',quest_answrs);

nav_section_02.firstChild.addEventListener('click',function(e){
    if(this.parentNode.classList.contains('active'))return;
    get_ajax(window.localStorage.item1 + '?action=select&target=quest&id=' + window.sessionStorage.quest_id, function (data) {
                    mv_quest(data);
                });
});
nav_section_04.firstChild.addEventListener('click',function(e){
    if(this.parentNode.classList.contains('active'))return;
    quest_answrs();
});

btn_next_x1sec04.addEventListener('click',quest_figure);
nav_section_05.firstChild.addEventListener('click',function(e){
    if(this.parentNode.classList.contains('active'))return;
    quest_figure();
});

input_quest_10.addEventListener('input',function(e){err_field(this);});

input_ansr1_08.addEventListener('change',function(e){misconcept(this);});
input_ansr0_08.addEventListener('change',function(e){misconcept(this);});
btn_save_x1sec04.addEventListener('click',add_quest_ansr);

input_quest_17.addEventListener('change',function(e){
    err_field(input_quest_17);
    switch(input_quest_17.value){
        case '0':
            img_quest_fig.src = PATH.value +'/main/usr/img/sys/qb-ink.png';
            break;
        default:
            let imgpath = PATH.value + '/main/usr/img/figure/' + window.sessionStorage.repo_id + '/' + input_quest_17.value + '.png';
            img_quest_fig.src = imgpath;
            break;
    }
});
btn_local_repo.addEventListener('click',function(e){folding(this);});

m_btn_view_icon.addEventListener('click',function(e){
	switch_panels(panel_img,panel_dml,'big-cell');
	// mobile navigation breadcrumb        
        crumbplus(document.getElementsByClassName('crumb+'),'icon');
});
_btnback.addEventListener('click',m_crumb_nav);
//-------------------------------------------------------------------------- repo edit panel
txt_instance_name.addEventListener('input',function(e){err_focus_2(this);});
txt_instance_name.addEventListener('change',function(e){validate_repo_name(this);});
tf_instance_name_00.addEventListener('input',function(e){err_field(this);});
tf_instance_name_00.addEventListener('change',function(e){validate_field_0(this);});
//-------------------------------------------------------------------------- figure panel
input_figure_03.addEventListener('input',function(e){err_field(this);});
input_figure_03.addEventListener('change',function(e){validate_field_3(this);});
input_figure_06.addEventListener('input',function(e){err_field(this);});
input_figure_06.addEventListener('change',function(e){validate_field_6(this);});

btn_done_x1sec04.addEventListener('click',function(e){
    window.location = '.';
});

// ------------------------------------------------------------------------- image crop
input_instance_02.addEventListener('change',function(){
    err_field(this);
    try {
        c = upload_img(this, crop_space, panel_img, 280, 280, false, 'img-rotate');
    } catch (ex) {
        discard_croppie(c);
        c = upload_img(this, crop_space, panel_img, 280, 280, false, 'img-rotate');
    }
    switch_panels(panel_crop_space, panel_dml, 'big-cell');

    // mobile navigation breadcrumb
    let aug = document.getElementsByClassName('crumb+');
   while(aug.length > 3) {
       m_nav_crumb.removeChild(m_nav_crumb.lastChild);
       bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
   } // ensures 3rd slice on bread
    crumbplus(aug, 'crop');

    btn_crop.onclick = instance_crop;
    btn_close_crop.onclick = function (e) {
        discard_croppie(c);
        m_crumb_nav();
        input_instance_02.value = null;
    };
    btn_detach_crop.onclick = function(ev){
        detatch_crop(panel_img,instance_crop);
    };
});
input_figure_05.addEventListener('change',function (e) {
    err_field(this);
    try {
        c = upload_img(this, crop_space, panel_index2_ls, 320, 320, true, 'img-rotate');
    } catch (ex) {
        discard_croppie(c);
        c = upload_img(this, crop_space, panel_index2_ls, 320, 320, true, 'img-rotate');
    }
    switch_panels(panel_crop_space,panel_index2_dml,'big-cell');
    panel_img.classList.add('display-none');
    // mobile navigation breadcrumb
    let aug = document.getElementsByClassName('crumb+');
   while(aug.length > 3) {
       m_nav_crumb.removeChild(m_nav_crumb.lastChild);
       bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
   } // ensures 3rd slice on bread
    crumbplus(aug, 'crop');
    
    btn_crop.onclick = figure_crop;
    btn_close_crop.onclick = function (e) {
        discard_croppie(c);
        m_crumb_nav();
    };
    btn_detach_crop.onclick = function(ev){
        detatch_crop(panel_index2_ls,figure_crop);
    };
});
btn_reset_instance.addEventListener('click',function(e){
    instance_icon_display.classList.add('dim');
    instance_icon_display.src = input_instance_02.title;
    imagebase64_instance_browse_img.value = null;
});

input_ansr_14.addEventListener('input',function(e){
    input_ansr_text.value = this.value
    err_field(this);
});
input_ansr_14.addEventListener('change',function(e){
    input_ansr_text.value = this.value;
});
input_ansr_15.addEventListener('change',function(e){
    input_ansr_text.value = this.value
    err_field(this);
});


input_quest_16.addEventListener('change',function(e){
    get_ajax(window.localStorage.item1 + '?action=ls&target=quest-fig-short&id='+window.sessionStorage.repo_id+'&type='+input_quest_16.value,function(ls){
        let fig = JSON.parse(ls);
        input_quest_17.innerHTML = null;
        select_option(input_quest_17,{'VALUE':0,'DISPLAY':'--'});
        if(typeof fig === 'object' && !Array.isArray(fig)) select_option(input_quest_17,{'VALUE': fig['FIGURE_ID'], 'DISPLAY': fig['FIGURE_LABEL']},'figls'); 
        else fig.forEach(function(data){select_option(input_quest_17,{'VALUE': data['FIGURE_ID'], 'DISPLAY': data['FIGURE_LABEL']},'figls');});
    });
});
btn_save_x1sec05.addEventListener('click',function(e){
    switch_panels(vss_add_x1sec05,this);
    let fig = parseInt(input_quest_17.value);
    if(fig < 1){
        err_field(input_quest_17,'nothing selected');
        switch_panels(this,vss_add_x1sec05);
        return;
    }
    post_ajax(window.localStorage.item1,'action=update&target=quest-fig&dml=mk&id='+window.sessionStorage.quest_id+'&fid='+input_quest_17.value,function(reply){
        switch(reply){
            case 'assigned':
                get_ajax(window.localStorage.item1 + '?action=ls&target=this-figs&id='+window.sessionStorage.repo_id,select_figure_ls);
                break;
            default:
                input_error_qansr.innerHTML = reply;
                break;
        }
        switch_panels(btn_save_x1sec05,vss_add_x1sec05);
    });
});
btn_done_x1sec05.addEventListener('click',function(e){
    window.location = '.';
});
sec_quest_form.addEventListener('click',function(e){quest_preview_nav(this);});
sec_quest_tex.addEventListener('click',function(e){quest_preview_nav(this);});
//	------------------------------------------------------------------------ add inatance panel lost focus
document.getElementsByTagName('body')[0].addEventListener('click',function(e){
    var button = e.which || e.button;
    if (button === 1) {
        let cm = document.getElementsByClassName('context-menu');
        for (let i = 0; i < cm.length; i++) cm[i].classList.add('display-none');
    }
    switch (e.target.id) {
        case 'instance_add_vss':
        case 'txt_instance_name':
        case 'btn_add_instance':
        case 'btn_sl_mk':
            break;
        default:
            if (!form_add_item.classList.contains('display-none')) sideline_add(false);
            break;
    }
    if(tbl_ls_x1.getAttribute('quest-preview')=== '1') qtxt_prev_out(e.target);
});
window.onkeyup = function(e) {
    if ( e.keyCode === 27 ) {
      let cm = document.getElementsByClassName('context-menu');
      for(let i = 0 ; i < cm.length ; i++) cm[i].classList.add('display-none');
      qtxt_prev_out(e.target)
      if (!form_add_item.classList.contains('display-none')) sideline_add(false);
    }
  };
//</editor-fold>