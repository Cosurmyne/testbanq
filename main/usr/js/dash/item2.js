function load() {
    window.sessionStorage.clear();
	/* containers containing javascript content are hidden by default
	* the following lines of code removes the css class hiding them
	*/
	var container = document.getElementsByClassName('js');
	for (var i = container.length - 1; i >= 0; i--) {
		container[i].classList.remove('js');
	}
        // initializing communication panes
	var cmc = document.getElementsByClassName('cmc');
	for (var i = 0; i < cmc.length; i++) {
		cmc[i].innerHTML = cmc[i].title;
	}
	if(url_exists(window.localStorage.item2)){	
            get_ajax(window.localStorage.item2+'?action=ls&subject=this',function(reply){
                prepare_list(reply,instance_list,'prime');
            });
            get_ajax(window.localStorage.item2+'?action=count&subject=this',function(reply){osd_1.innerHTML = m_osd_1.innerHTML = reply;});
            get_ajax(window.localStorage.item2+'?action=ls&subject=types',function(reply){
                populate_input_topic_00(JSON.parse(reply));
            });
            get_ajax(window.localStorage.item1+'?action=ls&target=this-groups',populate_input_topic_02);
        
    }else {
		instance_h1.innerHTML = 'Datasource Error';
		instance_p1.innerHTML = 'Item list can\'t be rendered';
		instance_p2.innerHTML = 'Address the following anormally;-';
		instance_err.innerHTML = 'file \''+ window.localStorage.item2 +'\' missing!';
		return;
	}
	sideline('100',true);	// active button on the left sideline to add instance
}
document.body.onload = load;
document.body.addEventListener('keypress',function(e){
	switch(e.key){
		case 'F1':
		console.log('mayday!');
		break;
		case 'F4':
		case 'F8':
		case 'F9':
		test(e.key);
		break;
		default:
		// console.log(e.key);
		break;
	}
});
//<editor-fold desc="Topics" defaultstate="collapsed">
function prepare_list(data,parent,target){
    let record = JSON.parse(data);
    if (record.length < 1) {
        var li = document.createElement('li');
        li.innerHTML = 'list empty';
        li.classList =  target + '-instance-zero';
        parent.appendChild(li);
    } else if (typeof record === 'object' && !Array.isArray(record)) {
        present_instance(record, parent);
    } else {
        for (var i = 0; i < record.length; i++) {
            present_instance(record[i], parent);
        }
    }
}
function present_instance(instance,parent) {
    // instance name
    let span_name = document.createElement('span');
    span_name.classList.add('list-title-text');
    span_name.innerHTML = instance['TOPIC_TITLE'];
    
    let span_desc = document.createElement('span');
    span_desc.classList.add('list-desc-text');
    span_desc.innerHTML = instance['TTYPE_DESC'];
    
    // instance [name + description] container
    let text = document.createElement('h1');
    text.appendChild(span_name);
    text.appendChild(span_desc);
    
    // instance text ultimate container
    let text_div = document.createElement('div');
    text_div.classList.add('flex-column');
    text_div.classList.add('list-body');
    text_div.appendChild(text);
    
    // instance 1st generation child
    let container = document.createElement('div');
    container.classList.add('flex-row');
    container.appendChild(text_div);
    
    var li = document.createElement('li');
    li.addEventListener('click', function (e) {if(!this.classList.contains('instance-active'))instance_select(this, instance);});
    li.appendChild(container);
    li.setAttribute('data-id', instance['TOPIC_ID']);
    parent.appendChild(li);
}
function instance_select(ctrl,instance){
    window.sessionStorage.topic_id = instance['TOPIC_ID'];
    content_err.innerHTML = null;   // clear error message
    // change instance selection to active
    let cmp = document.getElementsByClassName('instance-active');
    for (i = 0; i < cmp.length; i++) cmp[i].classList.remove('instance-active');
    ctrl.classList.add('instance-active');
    
    _btnback.classList.remove('display-none');
    wrap_btn_back.classList.remove('display-none');
    
    switch_panels(panel_content, list_panel, 'x768');
    
    if (panel_dml.classList.contains('display-none')) {
        // active UPDATE and DELETE instance buttons
        sideline('011', true);
        //  reveal key to assign questions
        let buttom_ctrl = document.getElementsByClassName('link_addx');
        for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.remove('display-none');
        l2_nav(instance['TTYPE_DESC'].split(' ')[0],'previewing');
        // check selected instance indices
        get_ajax(window.localStorage.item2 + '?action=ls&subject=index&id=' + instance['TOPIC_ID'], function (data) {
            present_index(instance, data);
        });
    }else{
        btn_save_instance.onclick = update_topic;
        panel_this_update(instance);
    }
    
    if (!panel_assign_quest.classList.contains('disply-none')) {
        switch (input_topic_03.value) {
            case 'NaN':
                select_option(input_topic_04, {'DISPLAY': '--', 'VALUE': 'NaN'}, 'tquest');
                break;
            default:
                get_ajax(window.localStorage.item2 + '?action=ls&subject=this-quest&rid=' + input_topic_03.value + '&id=' + window.sessionStorage.topic_id, populate_input_topic_04);
                break;
        }
    }
}
function present_index(topic, data) {
    let index = JSON.parse(data);
    tbl_ls_quest.innerHTML = null;   //  clear table
    panel_topic_quest.classList.remove('display-none');
    
    instance_h1.innerHTML = topic['TOPIC_TITLE'];
    instance_p1.innerHTML = topic['TTYPE_DESC'];
    instance_p2.innerHTML = null;

    if (index.length < 1) {
        instance_h1.innerHTML = 'Topic <b>\'' + topic['TOPIC_TITLE'] + '\'</b> Empty';
        instance_p1.innerHTML = 'Each topic can be assigned many questions, and the same questions can be assigned to many topics.';
        instance_p2.innerHTML = 'Use <b>\'+\'</b> buttons below to assign question questions to topic.';
    }
    present_quest(index);
}
function present_quest(quest){
    tbl_ls_quest.innerHTML = null;
    if(quest.length < 1) table_data_empty(tbl_ls_quest,'no questions assigned','rm-ls-topic-quest');
    else if (typeof quest === 'object' && !Array.isArray(quest)) build_quest_ls(quest);
    else quest.forEach(build_quest_ls);
    
    get_ajax(window.localStorage.item2 + '?action=count&subject=quest&id=' + window.sessionStorage.topic_id, function (count) {
        osd_3.innerHTML = m_osd_3.innerHTML = count;
    });
}
function quest_select(){
    switch (input_topic_03.value) {
        case 'NaN':
            select_option(input_topic_04, {'DISPLAY': '--', 'VALUE': 'NaN'}, 'tquest');
            break;
        default:
            get_ajax(window.localStorage.item2 + '?action=ls&subject=this-quest&rid=' + input_topic_03.value+'&id='+window.sessionStorage.topic_id,populate_input_topic_04);
            break;
    }
}
//</editor-fold>
//<editor-fold desc="Topic Questions" defaultstate="collapsed">
function build_quest_ls(quest){
    let qtxt = quest['QUESTION'];
    let txtlen = 32;
    if(typeof qtxt === 'string' && qtxt.trim().length > txtlen) qtxt = qtxt.substr(0, txtlen) + '...';
    
    let cell = [document.createElement('td'),document.createElement('td'), document.createElement('td')];
    let btn_del = [document.createElement('i'),document.createElement('i')];
    for(let i=0 ; i < btn_del.length;i++) cell[2].appendChild(btn_del[i]);
    
    
    cell[0].innerHTML = quest['CREATED'];
    cell[0].classList = 'extra';
    
    cell[1].innerHTML = quest['QTYPE_DESC'];
    
    cell[2].classList = 'control';
    
    btn_del[0].title = 'remove question';
    btn_del[0].classList = 'fa fa-remove rm-quest';
    btn_del[0].id = 'btn_rm_tquest_'+quest['ID'];
    btn_del[0].setAttribute('data-id',quest['ID']);
    btn_del[1].id = 'vss_rm_tquest_'+quest['ID'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';
    btn_del[0].addEventListener('click', function (e) { rm_quest(1,quest['ID']);});
    
    
    let tr = document.createElement('tr');
    tr.id = 'row_tquest_' + quest['ID'];
    tr.setAttribute('data-id',quest['ID']);
    tr.title = quest['QUESTION'];
    for(let i = 0 ; i < cell.length ; i++) tr.appendChild(cell[i]);
    
    tbl_ls_quest.appendChild(tr);
}
function rm_quest(stage,id,reply){
    if(typeof stage !== 'number') stage = 1;
    let rmbtn = document.getElementById('btn_rm_tquest_' + id);
    let rmvss = document.getElementById('vss_rm_tquest_' + id);
    switch(stage){
        case 1:
            switch_panels(rmvss,rmbtn);
            post_ajax(window.localStorage.item2,'action=rm&subject=this-quest&&id='+window.sessionStorage.topic_id + '&qid=' + id,function(reply){
                rm_quest(2,id,reply);
            });
            break;
        case 2:
            switch(reply){
                case 'success':
                    get_ajax(window.localStorage.item2 + '?action=select&subject=this&id=' + window.sessionStorage.topic_id, function(reply){
                        let instance = JSON.parse(reply);
                        get_ajax(window.localStorage.item2 + '?action=ls&subject=index&id=' + instance['TOPIC_ID'], function (data) {
                            present_index(instance, data);
                        });
                    });
                    quest_select(); // update question 'select' element
                    break;
                default:
                    content_err.innerHTML = reply;
                    break;
            }
            switch_panels(rmvss,rmbtn);
            break;
    }
}
function x1_add(){
    purge_panels(); //  reset workspace panel
    panel_assign_quest.classList.remove('display-none');
    panel_topic_quest.classList.remove('display-none');
    //  hide instance manipulation bottom keys
    let buttom_ctrl = document.getElementsByClassName('link_addx');
    for (let i = buttom_ctrl.length - 1; i >= 0; i--)   buttom_ctrl[i].classList.add('display-none');
    
    let aug = document.getElementsByClassName('crumb+');
        while (aug.length > 2) {
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        } // ensures 3rd slice on bread
        crumbplus(aug, 'quest');
}

function populate_input_topic_02(data){
    input_topic_02.innerHTML = null;
    let groups = JSON.parse(data);
    select_option(input_topic_02,{'DISPLAY':'local','VALUE':'0'},'tg','0');
    if (groups.length < 1) return;
    else if(typeof groups === 'object' && !Array.isArray(groups)){
        select_option(input_topic_02,{'DISPLY':groups['GROUP_NAME'],'VALUE':groups['GROUP_ID']},'tg','0');
    }else{
        groups.forEach(function(data){select_option(input_topic_02,{'DISPLAY':data['GROUP_NAME'],'VALUE':data['GROUP_ID']},'tg','0');});
    }
    switch(input_topic_02.value){
        case '0':
            get_ajax(window.localStorage.item1 + '?action=ls&target=this',populate_input_topic_03);
            break;
        default:
            get_ajax(window.localStorage.profile + '?action=group-repos&gid=' + input_topic_02.value,populate_input_topic_03);
            break;
    }
}
function populate_input_topic_03(data){
    input_topic_03.innerHTML = null;
    let repos = JSON.parse(data);
    if (repos.length < 1) select_option(input_topic_03,{'DISPLAY':'--','VALUE':'NaN'},'tr');
    else if(typeof repos === 'object' && !Array.isArray(repos)){
        select_option(input_topic_03,{'DISPLAY':repos['REPO_NAME'],'VALUE':repos['REPO_ID']},'tr');
    }else{
        repos.forEach(function(data){select_option(input_topic_03,{'DISPLAY':data['REPO_NAME'],'VALUE':data['REPO_ID']},'tr');});
    }
    quest_select(); // update question 'select' element
}
function populate_input_topic_04(data){
    input_topic_04.innerHTML = null;
    let quest = JSON.parse(data);
    if (quest.length < 1)   select_option(input_topic_04, {'DISPLAY': '--', 'VALUE': 'NaN'}, 'tquest');
    else if (typeof quest === 'object' && !Array.isArray(quest)) {
        let qtxt = quest['QUESTION'];
        let txtlen = 32;
        if (typeof qtxt === 'string' && qtxt.trim().length > txtlen)    qtxt = qtxt.substr(0, txtlen) + '...';
        select_option(input_topic_04, {'DISPLAY': qtxt, 'VALUE': quest['ID']}, 'tquest');
    } else {
        quest.forEach(function (data) { 
            let qtxt = data['QUESTION'];
            let txtlen = 32;
            if (typeof qtxt === 'string' && qtxt.trim().length > txtlen)    qtxt = qtxt.substr(0, txtlen) + '...';
            select_option(input_topic_04, {'DISPLAY': qtxt, 'VALUE': data['ID']}, 'tquest');
        });
    }
}
function save_tquest(stage,reply){
    if(typeof stage !== 'number') stage = 1;
    switch(stage){
        case 1:
            switch_panels(vss_save_tquest,btn_save_tquest);
            if(input_topic_04.value === 'NaN'){
                err_field(input_topic_04,'nothing to commit');
                switch_panels(btn_save_tquest,vss_save_tquest);
                break;
            }
            post_ajax(window.localStorage.item2,'action=assign&tid='+window.sessionStorage.topic_id+'&qid='+input_topic_04.value,
            function(reply){save_tquest(2,reply);});
            break;
        case 2:
            try{
                let data = JSON.parse(reply);
                switch(data['REPLY']){
                    case 'success':
                            present_quest(data['LIST']);    // pouplate table listing questions associated with topic
                            quest_select(); // update question 'select' element
                            break;
                default:
                    input_error_tquest.innerHTML = data['REPLY'];
                    break;
                }
            }catch(ex){
                input_error_tquest.innerHTML = ex;
            }
            switch_panels(btn_save_tquest,vss_save_tquest);
            break;
    }
}
//<editor-fold desc="Event Handling" defaultstate="collapsed">
btn_addx1.onclick = link_addx1.onclick = x1_add;
input_topic_02.onchange = function (e) {
    switch (this.value) {
        case '0':
            get_ajax(window.localStorage.item1 + '?action=ls&target=this', populate_input_topic_03);
            break;
        default:
            get_ajax(window.localStorage.profile + '?action=group-repos&gid=' + input_topic_02.value, populate_input_topic_03);
            break;
    }
};
input_topic_03.onchange = quest_select;
btn_save_tquest.onclick = save_tquest;
//</editor-fold>
//</editor-fold>
//<editor-fold desc="Side Line" defaultstate="collapsed">
function instance_dml(action){
    purge_panels();
    switch_panels(panel_dml, panel_default);
    l2_nav(action + ' topic',action);
    switch(action){
        case 'new':
            instance_ls_shield.classList.remove('display-none');
            btn_save_instance.onclick = create_topic;
            input_topic_01.value = null;
            break;
        case 'edit':
            btn_save_instance.onclick = update_topic;
            get_ajax(window.localStorage.item2 + '?action=select&subject=this&id='+window.sessionStorage.topic_id,function(reply){
                panel_this_update(JSON.parse(reply));
            });
            break;
    }
}
function populate_input_topic_00(data,natural){
    input_topic_00.innerHTML = null;
    if(data.length < 1) select_option(input_topic_00,{'DISPLAY':'--','VALUE':'-1'},'ttype',natural);
    else if(typeof data === 'object' && !Array.isArray(data))select_option(input_topic_00,{'DISPLAY':data['TTYPE_DESC'],'VALUE':data['TTYPE_ID']},'ttype',natural);
    else for(let i = 0 ; i < data.length ; i++) select_option(input_topic_00,{'DISPLAY':data[i]['TTYPE_DESC'],'VALUE':data[i]['TTYPE_ID']},'ttype',natural);
}
function panel_this_update(instance){
    input_topic_01.value = instance['TOPIC_TITLE'];
    get_ajax(window.localStorage.item2 + '?action=ls&subject=types',function(reply){
        populate_input_topic_00(JSON.parse(reply),instance['TTYPE_ID']);
    });
}
//<editor-fold desc="Event Handling" defaultstate="collapsed">
btn_sl_mk.onclick = function(e){instance_dml('new');};
btn_sl_mv.onclick = function(e){instance_dml('edit');};
btn_sl_rm.onclick = rm_topic;
//</editor-fold>
//</editor-fold>
//<editor-fold desc="Navigation" defaultstate="collapsed">
function purge_panels(boogeyman) {
    sideline('000', false);	// activate * buttons on sideline
    if (typeof boogeyman !== "string" || boogeyman.trim().length < 1) boogeyman = 'display-none';
    //  -------------------------------------- remove Instance Select panel
    /* The Dash Has 3 Cells
     * cell 1 list instances of current tab item
     * cell 2 is the main functionality content on the selected instance
     * cell 3 is the aside panel, with additional functionality
     */
    // cell 2
    instance_ls_shield.classList.add(boogeyman);
    panel_default.classList.add(boogeyman);
    panel_dml.classList.add(boogeyman);
    panel_topic_quest.classList.add(boogeyman);
    panel_assign_quest.classList.add(boogeyman);
}
function m_crumb_nav() {
    let crumb = document.getElementsByClassName('crumb+');
    let buttom_ctrl = document.getElementsByClassName('link_addx');
    let cmp = document.getElementsByClassName('instance-active');
    switch (crumb.length) {
        case 2:
            window.sessionStorage.removeItem('topic_id');
            purge_panels();
            panel_default.classList.remove('display-none');
            switch_panels(list_panel, panel_content, 'x768');
            // change instance selection to active
            for (i = 0; i < cmp.length; i++) cmp[i].classList.remove('instance-active');
            // return initial page status
            instance_h1.innerHTML = 'No Topic Selected';
            instance_p1.innerHTML = 'Please select Topic to your left.';
            instance_p2.innerHTML = 'Or use large cross to add one.';
            instance_err.innerHTML = null;
            //  conceal instance manipulation bottom keys
            for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.add('display-none');
            osd_3.innerHTML = m_osd_3.innerHTML = 'null';
            // reset mobile keys
            m_btn_dml.classList.remove('shift');
            m_panel_dml.classList.remove('shift');
            m_btn_back_to_ls.classList.add('display-none');
            list_panel.classList.remove('x1024');
            _btnback.classList.add('display-none');
            wrap_btn_back.classList.add('display-none');
            sideline('100', true);
            break;
        case 3:
            switch_panels(panel_default,panel_assign_quest);
            sideline(sideline_status.value, true);
            //  reveal instance manipulation bottom keys
            for (let i = buttom_ctrl.length - 1; i >= 0; i--) buttom_ctrl[i].classList.remove('display-none');
            break;
    }
    
    m_nav_crumb.removeChild(m_nav_crumb.lastChild);
    bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
}
function l2_nav(panel, specific) {
    _btnback.classList.remove('display-none');
    btn_back.classList.remove('display-none');
    wrap_btn_back.classList.remove('display-none');	// reveal back button
    // mobile navigation breadcrumb
    let aug = document.getElementsByClassName('crumb+');
    if (aug.length < 2) {
        if (aug.length > 1) {
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        } // ensures 2nd slice on bread
        //  accumulate breadcrumb with current panel
        crumbplus(aug, panel, specific);
        // mobile view navigation
        m_btn_dml.classList.add('shift');
        m_panel_dml.classList.add('shift');
        m_btn_back_to_ls.classList.remove('display-none');
        list_panel.classList.add('x1024');
    } else {
        bn_l1.firstChild.innerHTML = panel;
    }
}
//<editor-fold desc="Event Handling" defaultstate="collapsed">
btn_back.onclick = m_crumb_nav;
_btnback.onclick = m_crumb_nav;
//</editor-fold>
//</editor-fold>
//<editor-fold desc="Form Input" defaultstate="collapsed">
//<editor-fold desc="Validation" defaultstate="collapsed">
function validate_field_1(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'field empty');
		return false;
	}
	var txt = is_name_plus(target.value,2,35);	// verify correct naming conversion
	if(txt !== "success"){
		err_field(target,txt);
		return false;
	}
	if(is_symbol(target.value)){
		err_field(target,'symbol detected');	// check for non-alphabet symbols
		return false;
	}
	return true;
}
//</editor-fold>
function create_topic(stage,reply){
    if(typeof stage !== 'number')   stage = 1;
    input_error_instance.innerHTML = null;
    switch(stage){
        case 1: //  validation
            switch_panels(vss_save_instance,btn_save_instance);
            let valid = validate_field_1(input_topic_01);
            if(!valid){
                switch_panels(btn_save_instance,vss_save_instance);
                break;
            }
            post_ajax(window.localStorage.item2,'action=create&subject=this&ttype='+input_topic_00.value+'&ttitle='+input_topic_01.value,function(reply){
                create_topic(2,reply);
            });
            break;
        case 2:
            switch(reply){
                case 'success':
                    window.location = '?action=work-item2';
                    break;
                default:
                    input_error_instance.innerHTML = reply;
                    break;
            }
            switch_panels(btn_save_instance,vss_save_instance);
            break;
    }
}
function update_topic(stage,reply){
    if(typeof stage !== 'number') stage = 1;
    input_error_instance.innerHTML = null;
    switch (stage) {
        case 1:
            switch_panels(vss_save_instance, btn_save_instance);
            let valid = validate_field_1(input_topic_01);
            if(!valid){
                switch_panels(btn_save_instance,vss_save_instance);
                break;
            }
            post_ajax(window.localStorage.item2,'action=update&subject=this&id=' + window.sessionStorage.topic_id
                    + '&ttype=' + input_topic_00.value + '&ttitle='+input_topic_01.value,function(reply){
                        update_topic(2,reply);
                    });
            break;
        case 2:
            switch (reply) {
                case 'success':
                window.location = '?action=work-item2';
                break;
                default:
                    input_error_instance.innerHTML = reply;
                    break;
            }
            switch_panels(btn_save_instance, vss_save_instance);
            break;
    }
}
function rm_topic(stage,reply){
    if(typeof stage !== 'number') stage = 1;
    switch(stage){
        case 1:
            switch_panels(vss_sl_rm,btn_sl_rm);
            post_ajax(window.localStorage.item2,'action=rm&subject=this&id='+window.sessionStorage.topic_id,function(reply){console.log(reply);
                rm_topic(2,reply);
            });
            break;
        case 2:
            switch(reply){
                case 'success':
                    window.location = '?action=work-item2';
                    break;
                default:
                    input_error_instance.innerHTML = reply;
                    break;
            }
            switch_panels(btn_sl_rm,vss_sl_rm);
            break;
    }
}
//<editor-fold desc="Event Handling" defaultstate="collapsed">
input_topic_01.oninput = function(e){err_field(this);};
input_topic_01.onchange = function(e){validate_field_1(this);};
//</editor-fold>
//</editor-fold>