function load() {
    /* containers containing javascript content are hidden by default
     * the following lines of code removes the css class hiding them
     */
    var container = document.getElementsByClassName('js');
    for (var i = container.length - 1; i >= 0; i--) {
        container[i].classList.remove('js');
    }
    current_res();	// display resolution
    if(tf_subject.value.length > 0) reset_action();
}
document.body.onload = load;
document.body.addEventListener('keypress', function (e) {
    switch (e.key) {
        case 'F1':
            console.log('mayday!');
            break;
        case 'F4':
        case 'F8':
        case 'F9':
            test(e.key);
            break;
        default:
            // console.log(e.key);
            break;
    }
});
// Error TextField Formatting
function err_field(field, txt) {
    //  reseting elements
    field.classList.remove("err");
    reset_stumble.innerHTML = null;
    // check for error text
    if (typeof txt === "string" && txt.length > 0) {
        field.classList.add("err");
        reset_stumble.innerHTML = txt;
    }
}
function perform_reset(data) {
    switch (document.body.id) {
        case 'help_purge':
            if (data === 'success') {
                update_vss.classList.remove('display-none');
                btn_submit.disabled = true;
                btn_submit.innerHTML = 'purged';
                h1_feedback.innerHTML = 'Account Purged!';
                err_field(tf_subject);
            } else {
                err_field(tf_subject, data);
            }
            break;
        case 'help_reset':console.log(data);
            let response = data.split('$$');
            switch(parseInt(response[1])){
                case 0:
                    update_vss.classList.remove('display-none');
                    btn_submit.disabled = true;
                    btn_submit.innerHTML = 'sent';
                    h1_feedback.innerHTML = 'Reset Email Sent!';
                    err_field(tf_subject);
                    break;
                default:
                    err_field(tf_subject, response[0]);
                    break;
            }
            break;
    }
    switch_panels(btn_submit, sending_vss);
}
function reset_action(e) {
    // -------------------------------- validation
    if (tf_subject.value.trim().length < 1) {
        err_field(tf_subject, 'filed required!');
        return;
    }
    if (document.body.id === 'help_reset' && !is_email(tf_subject.value)) {
        err_field(tf_subject, 'email invalid');
        return;
    }
    //	------------------------------ retrieve info
    switch_panels(sending_vss, btn_submit);
    if (url_exists(localStorage.profile)) {	// checke if file exists
        post_ajax(localStorage.profile, 'action=' + document.body.id + '&subject=' + tf_subject.value, function (data) {
            perform_reset(data);
        });	//	forward operation to 'step 2'
    } else {
        err_field(tf_subject, 'file \'' + localStorage.profile + '\' missing!');
        switch_panels(btn_submit, sending_vss);
        return;
    }
}
tf_subject.addEventListener('input', function (e) {err_field(this);});
btn_submit.addEventListener('click', reset_action);
