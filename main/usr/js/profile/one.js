function load(){
	/* containers containing javascript content are hidden by default
	* the following lines of code removes the css class hiding them
	*/
	var container = document.getElementsByClassName('js');
	for (var i = container.length - 1; i >= 0; i--) {
		container[i].classList.remove('js');
	}
        //  check ajax file
    if (url_exists(window.localStorage.profile)) {	// checke if file exists
        post_ajax(window.localStorage.profile ,'action=one', function (data) {  // load user data from database
            user_details(data); // display information to form
            });
    } else {
        input_error.classList.remove('display-none');	// invoke the error description element
        input_error.innerHTML = 'file <b>\'' + window.localStorage.profile + '\'</b> missing!';
    }
    current_res();
}
document.body.onload = load;
document.body.addEventListener('keypress',function(e){
	switch(e.key){
		case 'F1':
		console.log('mayday!');
		break;
		case 'F4':
		case 'F8':
		case 'F9':
		test(e.key);
		break;
		default:
		// console.log(e.key);
		break;
	}
});

function user_details(data){
	let detail = JSON.parse(data);
        
	txt_input_0.title = txt_input_0.value = detail['USERNAME'];
	txt_input_1.title = txt_input_1.value = detail['USERMAIL'];
	txt_input_2.title = txt_input_2.value = detail['USERPHONE'];
	txt_input_3.title = txt_input_3.value = detail['FIRST_NAME'];
	txt_input_4.title = txt_input_4.value = detail['LAST_NAME'];
	sbm_btn.disabled = true;
	// initializing communication panes
	var cmc = document.getElementsByClassName('cmc');
	for (var i = 0; i < cmc.length; i++) {
		cmc[i].innerHTML = cmc[i].title;
	}
}
function err_field(field,txt){
	var cmc = document.getElementById("cmc_"+field.id.substr(field.id.length-1,1));	//	create cmc element to display error message
	//	reseting elements
	cmc.classList.remove("err");
	cmc.innerHTML = cmc.title;
	field.classList.remove('err');
	sbm_btn.disabled = false;

	if(typeof txt === "string" && txt.length > 0) {
		cmc.classList.add("err");
		field.classList.add('err');
		sbm_btn.disabled = true;
		cmc.innerHTML = txt;	// displaying error message
	}
}

//<editor-fold desc="VALIDATION" defaultstate="collapsed">
function validate_field_0(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'field empty');
		return false;
	}
	var txt = is_name_plus(target.value,2,20);	// verify correct naming conversion
	if(txt !== "success"){
		err_field(target,txt);
		return false;
	}
	if(is_symbol(target.value)){
		err_field(target,'symbol detected');	// check for non-alphabet symbols
		return false;
	}
	return true;
}
function validate_field_1(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'field empty');
		return false;
	}
	// check correct use of email address
	if(!is_email(target.value)){
		err_field(target,'email invalid');
		return false;
	}

	return true;
}
function validate_field_2(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		return true;
	}
	if(!is_int(target.value)){
		err_field(target,'please use only digits');
		return false;
	}
	if(anti_whitespace(target.value).length !== 10){
		err_field(target,'RSA phone contains 10 digits');
		return false;
	}

	return true;
}
function validate_field_3(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		return true;
	}
	var txt = is_name_plus(target.value,2,20);	// verify correct naming conversion
	if(txt !== "success"){
		err_field(target,txt);
		return false;
	}
	return true;
}
function validate_field_4(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		return true;
	}
	var txt = is_name_plus(target.value,2,20);	// verify correct naming conversion
	if(txt !== "success"){
		err_field(target,txt);
		return false;
	}
	return true;
}

function save_data(stage, target) {
    switch (stage) {
        case 1:	//------------------------------------------------------------------ VALIDATION
            var valid = true;
            if (!validate_field_0(txt_input_0)) valid = false;
            if (!validate_field_1(txt_input_1)) valid = false;
            if (!validate_field_2(txt_input_2)) valid = false;
            if (!validate_field_3(txt_input_3)) valid = false;
            if (!validate_field_4(txt_input_4)) valid = false;

            if (!valid) break;

            if (url_exists(window.localStorage.profile)) {	// checke if file exists
                if (txt_input_0.title !== txt_input_0.value.trim()) {
                    get_ajax(window.localStorage.profile + '?action=check&relation=user&target=USERNAME&value=' + txt_input_0.value, function (data) {
                        save_data(2, data);
                    });
                    break;
                }
                save_data(3);
            } else {
                input_error.classList.remove('display-none');	// invoke the error description element
                input_error.innerHTML = 'file \'' + window.localStorage.profile + '\' missing!';
                switch_panels(sbm_btn, update_vss);
                break;
            }
            break;
        case 2:	//------------------------------------------------------------------ VERIFICATION
            if (target === '1')
            {
                err_field(txt_input_0, 'unavailable');
                switch_panels(sbm_btn, update_vss);
                break;
            }
            save_data(3);
            break;
        case 3:
            if (txt_input_1.title !== txt_input_1.value.trim()) {
                get_ajax(window.localStorage.profile + '?action=check&relation=user&target=USERMAIL&value=' + txt_input_1.value, function (data) {
                    save_data(4, data);
                });
                break;
            }
            save_data(5);
            break;
        case 4:	//------------------------------------------------------------------ VERIFICATION
            if (target === '1')
            {
                err_field(txt_input_1, 'unavailable');
                switch_panels(sbm_btn, update_vss);
                break;
            }
            save_data(5);
            break;
        case 5:
            if (txt_input_2.title !== txt_input_2.value.trim()) {
                get_ajax(window.localStorage.profile + '?action=check&relation=user&target=USERPHONE&value=' + txt_input_2.value, function (data) {
                    save_data(6, data);
                });
                break;
            }
            save_data(7);
            break;
        case 6:
            if (target === '1')
            {
                err_field(txt_input_2, 'unavailable');
                switch_panels(sbm_btn, update_vss);
                break;
            }
            save_data(7);
            break;
        case 7:
            let q = '&uname='+txt_input_0.value+'&umail='+txt_input_1.value+'&uphone='+txt_input_2.value+'&fname='+txt_input_3.value+'&lname='+txt_input_4.value;
            post_ajax(window.localStorage.profile,'action=one-update'+q,function(data){save_data(8,data);});
            break;
        case 8:
            switch (parseInt(target.split('$$')[1])) {
                case 0:
                    uname_online.innerHTML = user_online.innerHTML = txt_input_0.value;
                    update_success.classList.remove('display-none');
                    sbm_btn.innerHTML = 'saved!';
                    sbm_btn.disabled = true;
                    break;
                case - 1:
                    console.log('validation');
                    break;
                case - 2:
                    console.log('home directory rename');
                    break;
                case - 3:
                    console.log('update error');
                    break;
                default:
                    console.log(target);
                    break;
            }
            switch_panels(sbm_btn, update_vss);
            break;
	}
}
//</editor-fold>
//<editor-fold desc="event handling" defaultstate="collapsed">
txt_input_0.addEventListener('input',function(e){err_field(this);});
txt_input_0.addEventListener('change',function(e){validate_field_0(this);});

txt_input_1.addEventListener('input',function(e){err_field(this);});
txt_input_1.addEventListener('change',function(e){validate_field_1(this);});

txt_input_2.addEventListener('input',function(e){err_field(this);});
txt_input_2.addEventListener('change',function(e){validate_field_2(this);});

txt_input_3.addEventListener('input',function(e){err_field(this);});
txt_input_3.addEventListener('change',function(e){validate_field_3(this);});

txt_input_4.addEventListener('input',function(e){err_field(this);});
txt_input_4.addEventListener('change',function(e){validate_field_4(this);});

sbm_btn.addEventListener('click',function(e){
	switch_panels(update_vss,this);	//	hide 'sign up' button, and show 'loading' icon
	save_data(1);
});
//</editor-fold>
