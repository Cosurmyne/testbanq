function load(){
	/* containers containing javascript content are hidden by default
	* the following lines of code removes the css class hiding them
	*/
	var container = document.getElementsByClassName('js');
	for (var i = container.length - 1; i >= 0; i--) {
		container[i].classList.remove('js');
	}
	foot_year.innerHTML = new Date().getFullYear();	// footer
	if (typeof sessionStorage.username === 'string') {
		user_online.innerHTML = window.sessionStorage.username;
		uid.innerHTML = window.sessionStorage.userid;
		uname_online.innerHTML = window.sessionStorage.username;
		umail_online.innerHTML = window.sessionStorage.usermail;
		get_ajax(localStorage.user,user_details);
	}
        // initializing communication panes
	var cmc = document.getElementsByClassName('cmc');
	for (var i = 0; i < cmc.length; i++) {
		cmc[i].innerHTML = cmc[i].title;
	}
	current_res();
}
document.body.onload = load;
document.body.addEventListener('keypress',function(e){
	switch(e.key){
		case 'F1':
		console.log('mayday!');
		break;
		case 'F4':
		case 'F8':
		case 'F9':
		test(e.key);
		break;
		default:
		// console.log(e.key);
		break;
	}
});
function user_details(data){
	var detail = csv_query(data,';',1,sessionStorage.username)[0];
	sbm_btn.disabled = true;
	if(detail[3].trim().length < 1 ) {
		panel_curr_passwd.classList.add('display-none');
	}
	// initializing communication panes
	var cmc = document.getElementsByClassName('cmc');
	for (var i = 0; i < cmc.length; i++) {
		cmc[i].innerHTML = cmc[i].title;
	}
}
function err_field(field,txt){
	var cmc = document.getElementById("cmc_"+field.id.substr(field.id.length-1,1));	//	create cmc element to display error message
	//	reseting elements
	cmc.classList.remove("err");
	cmc.innerHTML = cmc.title;
	field.classList.remove('err');
	sbm_btn.disabled = false;

	if(typeof txt === "string" && txt.length > 0) {
		cmc.classList.add("err");
		field.classList.add('err');
		sbm_btn.disabled = true;
		cmc.innerHTML = txt;	// displaying error message
	}
}
//<editor-fold desc="VALIDATION" defaultstate="collapsed">
function validate_field_0(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'please enter password');
		return false;
	}
	return true;
}
function validate_field_1(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'field empty');
		return false;
	}
	return true;
}
function validate_field_2(target){
	//	confirm input text
	if(target.value.trim().length < 1) {
		err_field(target,'field empty');
		return false;
	}
	return true;
}

function passwd_strength(tf){
	const PASS_LENGTH = 8;
	let n = parseInt(tf.id.substr(tf.id.length - 1, 1));
	let count = 0;
	let valid = false;
	//------------------------------------------------------- RESET STYLING
	err_field(tf);
	for (k = 0; k < 5; k++) document.getElementById(n + "pStrength" + k).classList = '';
	//-----------------------------------------------------------UPPER CASE
	for (k = 0; k < tf.value.length; k++) if (tf.value.charCodeAt(k) >= 65 && tf.value.charCodeAt(k) <= 90) document.getElementById(n + "pStrength" + 0).classList.add('check');
	//  -------------------------------------------------------- LOWER CASE
	for (k = 0; k < tf.value.length; k++) if (tf.value.charCodeAt(k) >= 97 && tf.value.charCodeAt(k) <= 122) document.getElementById(n + "pStrength" + 1).classList.add('check');
	//  ---------------------------------------------------- NUMERIC VALUES
	for (k = 0; k < tf.value.length; k++) if (tf.value.charCodeAt(k) >= 48 && tf.value.charCodeAt(k) <= 57) document.getElementById(n + "pStrength" + 2).classList.add('check');
	//  ------------------------------------------------------------ SYMBOL
	if (is_symbol(tf.value)) document.getElementById(n + "pStrength" + 3).classList.add('check');
	//  ------------------------------------------------------- TEXT LENGTH
	if (tf.value.length >= PASS_LENGTH) document.getElementById(n + "pStrength" + 4).classList.add('check');

	//  --------------------------------------------------- VALIDATION RULE
	for (k = 0; k < 5; k++) if (document.getElementById(n + "pStrength" + k).classList.contains('check'))  count++;
	if (count === 5) {
		valid = true;
		for (k = 0; k < 5; k++) document.getElementById(n + "pStrength" + k).classList = "done";
	}
	return valid;
}
function validate_passwd(tf){
	let count = 0;
	let n = parseInt(tf.id.substr(tf.id.length - 1, 1));
	for (k = 0; k < 5; k++) if (document.getElementById(n + "pStrength" + k).classList.length > 0)  count++;
	if (tf.value.trim().length > 0 && count < 5) err_field(tf,'password strength requirements not met');
}
//</editor-fold>
//<editor-fold desc="event handling" defaultstate="collapsed">
txt_input_1.addEventListener('input',function(e){passwd_strength(this);});
txt_input_1.addEventListener('change',function(e){validate_passwd(this);});

txt_input_2.addEventListener('input',function(e){passwd_strength(this);});
txt_input_2.addEventListener('change',function(e){validate_passwd(this);});

sbm_btn.addEventListener('click',function(e){
	switch_panels(update_vss,sbm_btn);	//	hide 'sign up' button, and show 'loading' icon
	save_data(1);
});
//</editor-fold>
