function load(){
    /* containers containing javascript content are hidden by default
	* the following lines of code removes the css class hiding them
	*/
	var container = document.getElementsByClassName('js');
	for (var i = container.length - 1; i >= 0; i--) {
		container[i].classList.remove('js');
	}
        //  check ajax file
    if (url_exists(window.localStorage.profile)) {	// checke if file exists
        post_ajax(window.localStorage.profile ,'action=acc', function (data) {
            user_details(data);
        });	//	forward operation to 'step 2'
    } else {
        input_error.classList.remove('display-none');	// invoke the error description element
        input_error.innerHTML = 'file <b>\'' + window.localStorage.profile + '\'</b> missing!';
    }
    current_res();
}
document.body.onload = load;
document.body.addEventListener('keypress',function(e){
	switch(e.key){
		case 'F1':
		console.log('mayday!');
		break;
		case 'F4':
		case 'F8':
		case 'F9':
		test(e.key);
		break;
		default:
		// console.log(e.key);
		break;
	}
});
function user_details(data){
    console.log(data);
    // initializing communication panes
	var cmc = document.getElementsByClassName('cmc');
	for (var i = 0; i < cmc.length; i++) {
		cmc[i].innerHTML = cmc[i].title;
	}
}