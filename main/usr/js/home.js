function load() {
    window.localStorage.clear();
    window.localStorage.setItem('profile', PATH.value + '/main/usr/php/ajax/profile.php');
    window.localStorage.setItem('item1', PATH.value + '/main/usr/php/ajax/item1.php');
    window.localStorage.setItem('item2', PATH.value + '/main/usr/php/ajax/item2.php');
    window.localStorage.setItem('item3', PATH.value + '/main/usr/php/ajax/item3.php');
    window.localStorage.setItem('item4', PATH.value + '/main/usr/php/ajax/item4.php');
    /* containers containing javascript content are hidden by default
     * the following lines of code removes the css class hiding them
     */
    var container = document.getElementsByClassName('js');
    for (var i = container.length - 1; i >= 0; i--) {
        container[i].classList.remove('js');
    }
    //  variable to switch between the sign in and sign up panel
    acc_toggle = 1;

    if (user_online.innerHTML.length === 0) {
        accactswitch(accswitch);
    }
    //  check ajax file
    if (url_exists(window.localStorage.profile)) {	// checke if file exists
        get_ajax(window.localStorage.profile + '?action=test', function (data) {
            console.log(data);
        });	//	forward operation to 'step 2'
    } else {
        acc_err.classList.remove('display-none');	// invoke the error description element
        acc_err.innerHTML = 'file <b>\'' + window.localStorage.profile + '\'</b> missing!';
    }
    current_res();
}
document.body.onload = load;
document.body.addEventListener('keypress', function (e) {
    switch (e.key) {
        case 'F1':
            console.log('mayday!');
            break;
        case 'F4':
        case 'F8':
        case 'F9':
            test(e.key);
            break;
        default:
            // console.log(e.key);
            break;
    }
});

function accactswitch(cmp) {
    acc_toggle *= -1;
    tf_acc_sec_1.value = tf_acc_sec_2.value = null;
    err_field(tf_acc_sec_1, null);
    err_field(tf_acc_sec_2, null);

    switch (acc_toggle)
    {
        case - 1:
            cmp.innerHTML = "Create Account";
            cmp.title = "click to create fresh accout";
            btn_acc_sec.innerHTML = 'Sign In';
            acc_sec_action.value = 'sign-in';

            tf_acc_sec_1.placeholder = 'username';

            tf_acc_sec_2.type = 'password';
            tf_acc_sec_2.placeholder = tf_acc_sec_2.title = 'password';

            btn_acc_sec.onclick = function (e) {
                switch_panels(signup_vss, this);
                signin(1);
            };
            break;
        case 1:
            cmp.innerHTML = "Sign In";
            cmp.title = "click to use existing account";
            btn_acc_sec.innerHTML = 'Sign Up';
            acc_sec_action.value = 'sign-up';

            tf_acc_sec_1.placeholder = 'unique username...';

            tf_acc_sec_2.type = tf_acc_sec_2.title = 'email';
            tf_acc_sec_2.placeholder = 'email address...';

            btn_acc_sec.onclick = function (e) {
                switch_panels(signup_vss, this);
                signup(1);
            };
            break;
    }
}
// Error TextField Formatting
function err_field(field, txt) {
    var popup = document.getElementById("popupfield" + field.id.substr(field.id.length - 1, 1));	//	create popup element to display error message
    popup.innerHTML = txt;	// displaying error message

    //	reseting elements
    acc_err.innerHTML = null;
    acc_err.classList.add('display-none');
    popup.classList.remove("show");
    field.classList.remove('err');

    if (typeof txt === "string" && txt.length > 0) {
        popup.classList.add("show");
        field.classList.add('err');
    }
}
function is_alias(target) {
    //	reset input field flag (format as normal)
    err_field(target, null);
    //	confirm input text
    if (target.value.trim().length < 1) {
        err_field(target, 'field empty');
        return false;
    }
    //	discriminate input fields
    switch (target.title) {
        case 'username':
            var txt = is_name_plus(target.value, 2, 20);	// verify correct naming conversion
            if (txt !== "success") {
                err_field(tf_acc_sec_1, txt);
                return false;
            }
            if (is_symbol(target.value)) {
                err_field(tf_acc_sec_1, 'symbol detected');	// check for non-alphabet symbols
                return false;
            }
            break;
        case 'email':
            if (!is_email(target.value)) {
                err_field(tf_acc_sec_2, 'email invalid');
                return false;
            }
            break;
    }
    return true;
}
function signup(stage, uname,umail,state) {
    var valid = true;
    switch (stage) {
        case 1: 	//----------------------------------------------------------------- VALIDATION

            if (!is_alias(tf_acc_sec_1)) valid = false;
            if (!is_alias(tf_acc_sec_2)) valid = false;
            if (!valid) {
                switch_panels(btn_acc_sec, signup_vss);
                break;
            }	// exit if any input from field is not a valid

            if (url_exists(window.localStorage.profile)) {	// checke if file exists
                get_ajax(window.localStorage.profile+'?action=check&relation=user&target=USERNAME&value=' + tf_acc_sec_1.value.trim(), function (data) {signup(2, data,null,null);});	//	forward operation to 'step 2'
            } else {
                acc_err.classList.remove('display-none');	// invoke the error description element
                acc_err.innerHTML = 'file \'' + window.localStorage.profile + '\' missing!';
                switch_panels(btn_acc_sec, signup_vss);	// remove 'loading' icon and return 'sign up' button
                break;
            }
            break;
        case 2:            
            get_ajax(window.localStorage.profile+'?action=check&relation=user&target=USERMAIL&value=' + tf_acc_sec_2.value.trim(), function (data) {signup(3, uname,data,null);});
            break;
        case 3:
            if (uname === '1') {
                err_field(tf_acc_sec_1, 'unavailable');
                valid = false;
            }
            if (umail === '1') {
                err_field(tf_acc_sec_2, 'already exist');
                valid = false;
            }
            if (valid) {
                let q = 'action=signup&uname=' + tf_acc_sec_1.value + '&umail=' + tf_acc_sec_2.value;
                post_ajax(window.localStorage.profile,q,function(data){signup(4,uname,umail,data)});
            }else{
                switch_panels(btn_acc_sec, signup_vss);
            }
            break;
        case 4:
            switch (parseInt(state.split('$$')[1])) {
                case 0:
                    let maildomain = tf_acc_sec_2.value.trim().split('@')[1];
                    give_report("Congratulations!", "Your Account has been created.\nSign your emaill account @<a href='http://" + maildomain + "' title='click link for quick access'>" + maildomain + "</a> to activate account.",
                            "activate your account within 24 hours, or your preserved username and email address will be released");
                    tf_acc_sec_1.value = tf_acc_sec_2.value = null;
                    accactswitch(accswitch);
                    break;
                case -1:
                case -2:
                    acc_err.classList.remove('display-none');	// invoke the error description element
                    acc_err.innerHTML = 'couldn\'t read<b>email file</b> to activate account.';
                break;
                case -3:
                    acc_err.classList.remove('display-none');	// invoke the error description element
                    acc_err.innerHTML = 'couldn\'t create<b>account files</b>due to server related issues.';
                    break;
                case -4:
                    acc_err.classList.remove('display-none');	// invoke the error description element
                    acc_err.innerHTML = state.split('$$')[0];
                break;
                default:
                    console.log('no case');
                    break;
            }console.log(state);
            switch_panels(btn_acc_sec, signup_vss);
            break;
    }
}
function signin(stage, target) {
    switch (stage) {
        case 1:
            if (!is_alias(tf_acc_sec_1)) {
                switch_panels(btn_acc_sec, signup_vss);	// remove 'loading' icon and return 'sign up' button
                break;	// exit if username from input is not a valid name
            }

            if (url_exists(window.localStorage.profile)) {		// checke if file exists
                get_ajax(window.localStorage.profile + '?action=check&relation=user&target=USERNAME&value=' + tf_acc_sec_1.value, function (data) {
                    signin(2, data);
                });		//	forward operation to 'step 2'
            } else {
                acc_err.classList.remove('display-none');
                acc_err.innerHTML = 'file \'' + target + '\' missing!';
                switch_panels(btn_acc_sec, signup_vss);	// remove 'loading' icon and return 'sign up' button
                break;
            }
            break;
        case 2:
            if (target === '0')
            {
                err_field(tf_acc_sec_1, 'not found');
                switch_panels(btn_acc_sec, signup_vss);	// remove 'loading' icon and return 'sign up' button
                break;
            }
            post_ajax(window.localStorage.profile, 'action=signin&uname=' + tf_acc_sec_1.value + '&passwd=' + tf_acc_sec_2.value, function (data) {
                signin(3, data);
            });
            break;
        case 3:console.log(target);
            let state = target.split(';');
            if (parseInt(state[0]) > 0) {
                switch_panels(btn_acc_sec, signup_vss);
                window.sessionStorage.clear();
                window.location = 'work';
                break;
            }
            switch (parseInt(state[0])) {
                case - 1:
                    err_field(tf_acc_sec_1, 'activate account');
                    acc_err.classList.remove('display-none');
                    acc_err.innerHTML = state[1].split(':')[0] + '<b>' + state[1].split(':')[1] + '</b>';
                    switch_panels(btn_acc_sec, signup_vss);
                    break;
                case - 4:
                    err_field(tf_acc_sec_2, state[1]);
                    switch_panels(btn_acc_sec, signup_vss);
                    break;
                case - 5:
                case - 6:
                    err_field(tf_acc_sec_1, target.split(';')[1]);
                    acc_err.classList.remove('display-none');
                    acc_err.innerHTML = 'please contact <b>system admin</b> with this.';
                    switch_panels(btn_acc_sec, signup_vss);	// remove 'loading' icon and return 'sign up' button
                    break;
                default:
                    acc_err.classList.remove('display-none');
                    acc_err.innerHTML = 'sorry, you couldn\'t be logged in..';
                    switch_panels(btn_acc_sec, signup_vss);	// remove 'loading' icon and return 'sign up' buttonz
                    break;
            }
            break;
    }
}
function input_field_key(e) {
    if (e.key === 'Enter') {
        switch_panels(signup_vss, btn_acc_sec);
        switch (accswitch.innerHTML) {
            case 'Sign In':
                signup(1);
                break;
            case 'Create Account':
                signin(1);
                break;
        }
    }
}
//<editor-fold desc="event handling" defaultstate="collapsed">
accswitch.href = 'javascript:accactswitch(accswitch)';
glass_modal.addEventListener('click', function (e) {
    this.classList.add('display-none');
});
//
// field input 1
//
tf_acc_sec_1.addEventListener('input', function (e) {
    err_field(this, null);
});
tf_acc_sec_1.addEventListener('change', function (e) {
    is_alias(this);
});
tf_acc_sec_1.onkeypress = function (e) {
    input_field_key(e);
};
//
// field input 2
//
tf_acc_sec_2.addEventListener('input', function (e) {
    err_field(this, null);
});
tf_acc_sec_2.addEventListener('change', function (e) {
    is_alias(this);
});
tf_acc_sec_2.onkeypress = function (e) {
    input_field_key(e);
};
//</editor-fold>
function test(sate) {
    switch (sate) {
        case 'F4':
            err_field(tf_acc_sec_1, null);
            err_field(tf_acc_sec_2, null);
            break;
        case 'F8':
            err_field(tf_acc_sec_1, 'content error');
            break;
        case 'F9':
            err_field(tf_acc_sec_2, 'content error');
            break;
    }
}
