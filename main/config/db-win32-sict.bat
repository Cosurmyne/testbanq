@echo off

	set /p pass=" Enter MySql root Password: "
	ECHO ON

	mysql -uroot -p%pass% -hsict-mysql.nmmu.ac.za < ..\usr\sql\db-schema.sql
	mysql -uroot -p%pass% testbank -hsict-mysql.nmmu.ac.za < ..\usr\sql\db-data.sql
	mysql -uroot -p%pass% testbank -hsict-mysql.nmmu.ac.za < ..\usr\sql\db-trigger.sql
