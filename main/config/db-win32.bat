@echo off
set /p hasPass=" Do you have MySql root Password?[y/N] "
::set pass=
IF /I "%hasPass%"=="Y" (

	set /p pass=" Enter MySql root Password: "
	ECHO ON
	mysql -uroot -p%pass% < ..\usr\sql\db-admin.sql
	mysql -uroot -p%pass% < ..\usr\sql\db-schema.sql
	mysql -uroot -p%pass% testbank < ..\usr\sql\db-data.sql
	mysql -uroot -p%pass% testbank < ..\usr\sql\db-trigger.sql

) ELSE (
	ECHO ON
	mysql -uroot  < ..\usr\sql\db-admin.sql
	mysql -uroot  < ..\usr\sql\db-schema.sql
	mysql -uroot testbank < ..\usr\sql\db-data.sql
	mysql -uroot testbank < ..\usr\sql\db-trigger.sql
)
